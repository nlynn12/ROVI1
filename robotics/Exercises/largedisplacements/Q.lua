wc = rws.getRobWorkStudio():getWorkCell()
state = wc:getDefaultState()
device = wc:findDevice("UR-6-85-5-A")

function setQ(q)
	qq = rw.Q(#q,q[1],q[2],q[3],q[4],q[5],q[6])
	device:setQ(qq,state)
	rws.getRobWorkStudio():setState(state)
	rw.sleep(1)
end

setQ({0.476,-0.440,0.620,-0.182,2.047,-1.574})


q_vectors = {}
file = assert(io.open("/home/nicolai/Documents/ROVI1/Robotics/Exercises/largedisplacements/src/LUAdata.txt", "r"))
i = 0
j = 0
for line in file:lines() do
	i = i + 1
	q_vectors[i] = {}
	local string = line
	for w in (string .. ","):gmatch("([^,]*),") do
		j = j + 1
		q_vectors[i][j] = tonumber(w)
	end
	j = 0
end

for i=1,#q_vectors-1 do
	setQ(q_vectors[i])
end

print("Program finished")
