# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.0

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4"

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4-build"

# Include any dependencies generated for this target.
include CMakeFiles/exercise4_4.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/exercise4_4.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/exercise4_4.dir/flags.make

CMakeFiles/exercise4_4.dir/src/main.cpp.o: CMakeFiles/exercise4_4.dir/flags.make
CMakeFiles/exercise4_4.dir/src/main.cpp.o: /home/nicolai/Documents/RoVi1/robotics/Programming\ Exercises/exercise4_4/exercise4_4/src/main.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4-build/CMakeFiles" $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/exercise4_4.dir/src/main.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/exercise4_4.dir/src/main.cpp.o -c "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4/src/main.cpp"

CMakeFiles/exercise4_4.dir/src/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/exercise4_4.dir/src/main.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4/src/main.cpp" > CMakeFiles/exercise4_4.dir/src/main.cpp.i

CMakeFiles/exercise4_4.dir/src/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/exercise4_4.dir/src/main.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4/src/main.cpp" -o CMakeFiles/exercise4_4.dir/src/main.cpp.s

CMakeFiles/exercise4_4.dir/src/main.cpp.o.requires:
.PHONY : CMakeFiles/exercise4_4.dir/src/main.cpp.o.requires

CMakeFiles/exercise4_4.dir/src/main.cpp.o.provides: CMakeFiles/exercise4_4.dir/src/main.cpp.o.requires
	$(MAKE) -f CMakeFiles/exercise4_4.dir/build.make CMakeFiles/exercise4_4.dir/src/main.cpp.o.provides.build
.PHONY : CMakeFiles/exercise4_4.dir/src/main.cpp.o.provides

CMakeFiles/exercise4_4.dir/src/main.cpp.o.provides.build: CMakeFiles/exercise4_4.dir/src/main.cpp.o

# Object files for target exercise4_4
exercise4_4_OBJECTS = \
"CMakeFiles/exercise4_4.dir/src/main.cpp.o"

# External object files for target exercise4_4
exercise4_4_EXTERNAL_OBJECTS =

bin/exercise4_4: CMakeFiles/exercise4_4.dir/src/main.cpp.o
bin/exercise4_4: CMakeFiles/exercise4_4.dir/build.make
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_algorithms.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_pathplanners.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_pathoptimization.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_simulation.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_opengl.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_assembly.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_task.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_calibration.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_lua_s.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/liblua51.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_proximitystrategies.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/libyaobi.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/libpqp.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw.a
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libGLU.so
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libGL.so
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libX11.so
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libXext.so
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libxerces-c.so
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_assimp.a
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libboost_regex.so
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libboost_system.so
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libboost_thread.so
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libboost_program_options.so
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libpthread.so
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libboost_test_exec_monitor.a
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libboost_unit_test_framework.so
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_qhull.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_unzip.a
bin/exercise4_4: /home/nicolai/Programs/RobWork/RobWork/libs/release/librw_zlib.a
bin/exercise4_4: /usr/lib/x86_64-linux-gnu/libdl.so
bin/exercise4_4: CMakeFiles/exercise4_4.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable bin/exercise4_4"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/exercise4_4.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/exercise4_4.dir/build: bin/exercise4_4
.PHONY : CMakeFiles/exercise4_4.dir/build

CMakeFiles/exercise4_4.dir/requires: CMakeFiles/exercise4_4.dir/src/main.cpp.o.requires
.PHONY : CMakeFiles/exercise4_4.dir/requires

CMakeFiles/exercise4_4.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/exercise4_4.dir/cmake_clean.cmake
.PHONY : CMakeFiles/exercise4_4.dir/clean

CMakeFiles/exercise4_4.dir/depend:
	cd "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4-build" && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4" "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4" "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4-build" "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4-build" "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4-build/CMakeFiles/exercise4_4.dir/DependInfo.cmake" --color=$(COLOR)
.PHONY : CMakeFiles/exercise4_4.dir/depend

