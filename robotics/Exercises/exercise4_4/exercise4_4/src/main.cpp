#include <iostream>
#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/loaders/WorkCellFactory.hpp>

using namespace std;
using namespace rw::math;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::kinematics;

#define EPSILON 0.0000001	// error accepted in interpolation and inverse kinematic
#define MAX_ITE 40			// the maximum number of iterations


VelocityScrew6D<double> calculateDeltaU(Transform3D<double> &Tbasetool, Transform3D<double> Tbasetooldesired)
{
    // making delta position
    Vector3D<double> deltaPdesired = Tbasetooldesired.P() - Tbasetool.P();

    // making the rotation [R^(i-1)]^T * R^i
    Rotation3D<double> rotationT = inverse(Tbasetool.R());
    Rotation3D<double> rotationDesired = Tbasetooldesired.R();
    Rotation3D<double> proRotation = rotationDesired*rotationT;

    // make new delta W
    Vector3D<double> deltaWdesired (proRotation(2,1)-proRotation(1,2),
                                    proRotation(0,2)-proRotation(2,0),
                                    proRotation(1,0)-proRotation(0,1));
    deltaWdesired *= 0.5;

    // making deltaU
    VelocityScrew6D<double> deltaU = VelocityScrew6D<double>(deltaPdesired(0),deltaPdesired(1),deltaPdesired(2),deltaWdesired(0),deltaWdesired(1),deltaWdesired(2));

    return deltaU;
}

void algorithm1(const Device::Ptr device, State &state, const Frame* tool, Transform3D<double> &Tbasetool, Transform3D<double> Tbasetooldesired, Q &q) {
    cout << " run:" << "\t" << "deltaU" << endl;
    cout.precision(12);
    int run = 0;

    // Calculation pre delta U
    VelocityScrew6D<double> deltaU = calculateDeltaU(Tbasetool,Tbasetooldesired);
    cout << " " << run << "\t" << deltaU.norm2() << endl;

    // for each step making algorithm 1
    while( deltaU.norm2()>EPSILON && run<MAX_ITE ) {
        // calculate the jacobian matrix
        Jacobian jacobian = device->baseJframe(tool,state);

        // solve J*dq=du => dq=J^(-1)*du (inverse does not modify jacobian)
        Q deltaQ( jacobian.e().inverse()*deltaU.e() );

        // add dq to from and set the device
        q += deltaQ;
        device->setQ(q,state);

        // Calculate the new T_base^tool
        Tbasetool = device->baseTframe(tool,state);

        // Calculate the new du
        deltaU = calculateDeltaU(Tbasetool, Tbasetooldesired);

        run++;
        cout << " " << run << "\t" << deltaU.norm2() << endl;
        if( run==MAX_ITE ) RW_THROW("Algorithm 1 did not find a solution within " << MAX_ITE << " iterations!");
    }
    cout << " used " << run << " iteration(s)" << endl;
    cout << endl;
    cout.precision(6);
}

int main() {
    cout << " --- Program started --- " << endl << endl;

    cout.setf( std::ios::fixed );
    cout.precision(6);

    /***** Loading device and tool *****/
    const string wcFile = "../../../Workcells/URInterpolate/Scene.wc.xml";
    const string deviceName = "UR-6-85-5-A";
    cout << "Trying to use workcell " << wcFile << " and device " << deviceName << endl;

    WorkCell::Ptr wc = WorkCellLoader::Factory::load(wcFile);
    Device::Ptr device = wc->findDevice(deviceName);
    Frame* tool = wc->findFrame("Tool");
    if (device == NULL) RW_THROW("Device: " << deviceName << " not found!");
    if (tool == NULL) RW_THROW("Tool: " << "Tool" << " not found!");


    /***** Prepare for Algorithm 1 *****/
    // Get device to start configuration
    State state = wc->getDefaultState();
    Q q = device->getQ(state);
    cout << "q: " << q << endl;

    // Get T_base^end for start
    Transform3D<double> Tbasetool = device->baseTframe(tool,state);
    Transform3D<double> TbasetoolStart = Tbasetool; // save for later comparison
    cout << "Tbasetool:\t" << endl << " " << Tbasetool.P() << endl << " " << Tbasetool.R() << endl;

    // Make the desired configuration
    const double delta = 0.0001;
    Vector3D<double> deltaP = Vector3D<double>(delta,delta,delta);
    Vector3D<double> deltaPdesired = Tbasetool.P() + deltaP;
    Rotation3D<double> rotationDesired = Tbasetool.R();
    Transform3D<double> Tbasetooldesired(deltaPdesired,rotationDesired);
    cout << "Tbasetooldesired:\t" << endl << " " << Tbasetooldesired.P() << endl << " " << Tbasetooldesired.R() << endl;
    cout << "The positional displacement is: " << (Tbasetool.P()-Tbasetooldesired.P()).norm2() << endl << endl;


    /***** Using Algorithm 1 *****/
    cout << "Using Algorithm 1:" << endl;
    algorithm1(device, state, tool, Tbasetool, Tbasetooldesired, q);
    cout << "q: " << q << endl;
    cout << "Tbasetool:\t" << endl << " " << Tbasetool.P() << endl << " " << Tbasetool.R() << endl;
    cout << "Tbasetooldesired:\t" << endl << " " << Tbasetooldesired.P() << endl << " " << Tbasetooldesired.R() << endl;
    cout << "The positional displacement was: " << (TbasetoolStart.P()-Tbasetool.P()).norm2() << endl;

    cout << endl << " --- Program ended ---" << endl;
    return 0;
}
