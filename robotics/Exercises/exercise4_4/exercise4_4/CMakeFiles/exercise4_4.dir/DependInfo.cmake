# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4/src/main.cpp" "/home/nicolai/Documents/RoVi1/robotics/Programming Exercises/exercise4_4/exercise4_4/CMakeFiles/exercise4_4.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "BOOST_DISABLE_ASSERTS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/boostbindings"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/eigen3"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../src"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/rwyaobi"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/rwpqp"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/lua/src"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/qhull/src"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/zlib"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/unzip"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/assimp/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
