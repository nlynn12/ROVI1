#include <iostream>
#include <rw/math.hpp> // Pi, Deg2Rad
#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/RPY.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/math/EAA.hpp>

#define RW_FIXED_FRAME  0
#define RW_RPY          1

using namespace std;
using namespace rw::math;

Rotation3D<double> calc_rotation_matrix(double roll, double pitch, double yaw, int rotation_mode){
    Rotation3D<double> r_m;

    double x,y,z;
    z = roll;
    y = pitch;
    x = yaw;

    switch (rotation_mode) {
    case RW_FIXED_FRAME:
        r_m(0,0) = cos(y)*cos(z);                       r_m(0,1) = -cos(y)*sin(z);                       r_m(0,2) = sin(y);
        r_m(1,0) = cos(x)*sin(z)+cos(z)*sin(x)*sin(y);  r_m(1,1) = cos(x)*cos(z)-sin(x)*sin(y)*sin(z);   r_m(1,2) = -cos(y)*sin(x);
        r_m(2,0) = sin(x)*sin(z)-cos(x)*cos(z)*sin(y);  r_m(2,1) = cos(z)*sin(x)+cos(x)*sin(y)*sin(z);   r_m(2,2) = cos(x)*cos(y);
        break;
    case RW_RPY:
        r_m(0,0) = cos(y)*cos(z);    r_m(0,1) = sin(x)*sin(y)*cos(z)-cos(x)*sin(z);   r_m(0,2) = cos(x)*sin(y)*cos(z)+sin(x)*sin(z);
        r_m(1,0) = cos(y)*sin(z);    r_m(1,1) = sin(x)*sin(y)*sin(z)+cos(x)*cos(z);   r_m(1,2) = cos(x)*sin(y)*sin(z)-sin(x)*cos(z);
        r_m(2,0) = -sin(y);          r_m(2,1) = sin(x)*cos(y);                        r_m(2,2) = cos(x)*cos(y);
    default:
        break;
    }

    return r_m;
}

int main(int argc, char** argv) {
    cout << " --- Program started --- " << endl << endl;

    Rotation3D<double> r_m_FIXED;
    r_m_FIXED = calc_rotation_matrix(1.5708,0.3491,1.0472,RW_FIXED_FRAME);
    cout << "FIXED: \t" << r_m_FIXED << endl;

    Rotation3D<double> r_m_RPY;
    r_m_RPY = calc_rotation_matrix(1.5708,0.3491,1.0472,RW_RPY);
    cout << "RPY: \t" << r_m_RPY << endl;

    // Exercise 3.2
    double theta_x, theta_y, theta_z;
    theta_y = asin(-r_m_RPY(2,0));
    cout << theta_y << endl;
    if(theta_y != 1){
        theta_x = atan2(cos(theta_y)*r_m_RPY(2,1),cos(theta_y)*r_m_RPY(2,2));
        theta_z = atan2(cos(theta_y)*r_m_RPY(1,0),cos(theta_y)*r_m_RPY(0,0));
    }
    cout << theta_x << "\t" << theta_y << "\t" << theta_z << endl;

    Rotation3D<double> lol;
    cout << lol << endl;
    lol.multiply(r_m_RPY,r_m_FIXED, lol);
    cout << lol << endl;


    cout << endl << " --- Program ended --- " << endl;
    return 0;
}
