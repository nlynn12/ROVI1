#include <iostream>
#include <vector>
#include <rw/math.hpp> // Pi, Deg2Rad
#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/RPY.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/math/EAA.hpp>

using namespace std;
using namespace rw::math;

#define R 0
#define T 1

/*class robot{
   public:
    vector<Transform3D> Tref;
    vector<Transform3D> Tz;

    robot(vector<double>);
    void calc_Tref(vector<float>);

  private:
    vector<double> q;

    Rotation3D<double> calc_rotation_matrix(double, double, double);

};

robot::robot(vector<double> input_q){
    q = input_q;
}

void robot::calc_Tref(vector<float> joint){
    Transform3D tmp;
    float x

}*/

Rotation3D<double> calc_rotation_matrix(double roll, double pitch, double yaw){
    Rotation3D<double> r_m;

    double x,y,z;
    z = roll;
    y = pitch;
    x = yaw;

    r_m(0,0) = cos(y)*cos(z);    r_m(0,1) = sin(x)*sin(y)*cos(z)-cos(x)*sin(z);   r_m(0,2) = cos(x)*sin(y)*cos(z)+sin(x)*sin(z);
    r_m(1,0) = cos(y)*sin(z);    r_m(1,1) = sin(x)*sin(y)*sin(z)+cos(x)*cos(z);   r_m(1,2) = cos(x)*sin(y)*sin(z)-sin(x)*cos(z);
    r_m(2,0) = -sin(y);          r_m(2,1) = sin(x)*cos(y);                        r_m(2,2) = cos(x)*cos(y);

    for(int i=0; i<3;i++){
        for(int j=0;j<3;j++){
            if(abs(r_m(i,j)) < 1E-10){
                r_m(i,j) = 0;
            }
        }
    }

    return r_m;
}

Transform3D<double> calc_transform(vector<double> tmp){
    Vector3D<double> translation(tmp[0],tmp[1],tmp[2]);

    Rotation3D<double> r_m_RPY;
    r_m_RPY = calc_rotation_matrix(tmp[3],tmp[4],tmp[5]);

    Transform3D<double> test(translation,r_m_RPY);
    return test;
}

vector<Transform3D<double> > calc_joint_refs(vector<vector<double> > joints){
    vector<Transform3D<double> > tmp;
    for(int i = 0; i< joints.size(); i++){
        tmp.push_back(calc_transform(joints[i]));
    }
    return tmp;
}

vector<Transform3D<double> > calc_Tz(vector<double> q, vector<int> q_type){
    vector<Transform3D<double> > tmp;
    vector<double> tmp2 {0,0,0,0,0,0};
    for(int i = 0; i< q.size(); i++){
        if(q_type[i] == R){
            tmp2 = {0,0,0,q[i],0,0};
        }else{
            tmp2 = {0,0,q[i],0,0,0};
        }
        tmp.push_back(calc_transform(tmp2));
    }
    return tmp;
}

int main()
{
    // Calculate Trefs:
    cout << endl << "----------------------------------------------------" << endl;
    cout << "                 Tref:"<< endl;
    cout << "----------------------------------------------------" << endl;

    vector<double> T1_base { 1.2,2.3,-3.5,M_PI/2,0.0,0.0 };
    vector<double> T2_1 {0.0,0.0,4.0,0.0,0.0,-(M_PI/2)};
    vector<double> TTCP_2 {0.0,0.0,2.0,0.0,0.0,0.0};

    vector<vector<double> > joint_values;

    joint_values.push_back(T1_base);
    joint_values.push_back(T2_1);
    joint_values.push_back(TTCP_2);

    vector<Transform3D<double> > Tref;
    Tref = calc_joint_refs(joint_values);

    for(int i = 0; i < Tref.size(); i++){
        cout << Tref[i] << endl;
    }


    // Calculate Tz:
    cout << endl << "----------------------------------------------------" << endl;
    cout << "                 Tz:"<< endl;
    cout << "----------------------------------------------------" << endl;
    vector<double> q {M_PI/3,2.8};
    vector<int> q_type {R,T};
    vector<Transform3D<double> > Tz;
    Tz = calc_Tz(q,q_type);

    for(int i = 0; i < Tz.size(); i++){
        cout << Tz[i] << endl;
    }

    // Calc T^TCP_Base
    cout << endl << "----------------------------------------------------" << endl;
    cout << "                 T^TCP_Base:"<< endl;
    cout << "----------------------------------------------------" << endl;
    //Transform3D<double> result_tmp;
    Transform3D<double> result;
    cout << result << endl << endl;


    for(int i=0; i<Tref.size()-1;i++){
        result = result * Tref[i];
        result = result * Tz[i];
        //cout << i << " " << result << endl;
    }

    result = result * Tref[Tref.size()-1];

    cout<< endl << result << endl;

    return 0;
}

