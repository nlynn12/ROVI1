Programming Exercises
====================
The [programming exercises](Exercises/) can be found in the [Robotic Notes](Robotic Notes.pdf). For compiling the programs simply use cmake

```
$ cmake CMakeLists.txt
$ make
```
Executables are defined to be placed in the bin folder.

**Note** that the [RobWork framework](http://www.robwork.dk/jrobwork/), developed at the University of Southern Denmark, needs to be installed in order to compile the programming exercises.

Workcells
=========
The [workcells](Workcells/) used in the projects and programming exercises are used with the [RobWork framework](http://www.robwork.dk/jrobwork/).

Installing the RobWork Framework on Ubuntu 15.04
================================================
**Please proceed at own risk** - the authors cannot be held responsible for any damage done by following the guide below.

- Insert in .bashrc
```
# ROBWORK #
export RW_ROOT=~/Programs/RobWork/RobWork/
export RWS_ROOT=~/Programs/RobWork/RobWorkStudio/
export RWHW_ROOT=~/Programs/RobWork/RobWorkHardware/
export RWSIM_ROOT=~/Programs/RobWork/RobWorkSim/
```

- Restart or log out and log in

- Install dependencies
```
sudo apt-get install gcc g++ cmake libatlas-base-dev libxerces-c3.1 libxerces-c-dev libboost-dev libboost-date-time-dev libboostfilesystem-dev libboost-program-options-dev libboost-regex-dev libboost-serialization-dev libboost-system-dev libboost-test-dev libboost-thread-dev swig qt4-dev-tools libqt4-dev qt4-designer libatlas-base-dev liblapack-dev libode-dev subversion
```

- Obtain RobWork
```
svn -r5443 co https://svnsrv.sdu.dk/svn/RobWork/trunk ~/Programs/RobWork
```

- Make RobWork
```
cd ~/Programs/RobWork
mkdir release
cd release
cmake -DCMAKE_BUILD_TYPE=release -DWITH_RWSIM=false ..
make
```