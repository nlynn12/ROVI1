\documentclass[11pt,twoside,a4paper]{article}


% ------- Enable UTF8 characters ------- %
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}


% --------------- Math ----------------- %
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mdframed}
\newcommand{\dif}{\mbox{$\mathrm{d}$}}
\newcommand{\ohm}{\mbox{$\Omega$}}

% ------------- Figures & Tables -------------- %
\usepackage{graphicx}
\usepackage{caption}
\usepackage{float}
\usepackage{subcaption}
\captionsetup[figure]{singlelinecheck=false, margin=0pt,labelsep=space,labelfont=bf,textfont=it}
\captionsetup[table]{singlelinecheck=false, margin=0pt,labelsep=space,labelfont=bf,textfont=it}
\usepackage{epstopdf} %MATLAB eps (vector) figures

\usepackage{array}
\newcolumntype{x}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}p{#1}}


% --------------- Color ---------------- %
\usepackage{color}
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}


% --------------- Code ----------------- %
\usepackage{verbatim}
\usepackage{listings}
\lstset{
	frame				= lines,
  	language				= C++,
  	aboveskip			= 3mm,
  	belowskip			= 3mm,
  	captionpos			= t,
  	showstringspaces		= false,
  	columns				= flexible,
  	basicstyle			= {\small\ttfamily},
  	numbers				= left,
  	numberstyle			= \tiny\color{gray},
  	keywordstyle			= \color{blue},
  	commentstyle			= \color{dkgreen},
  	stringstyle			= \color{dkgreen},
  	breaklines			= true,
  	breakatwhitespace	= true,
  	tabsize				= 3,
  	xleftmargin			= 17pt,
  	framexleftmargin		= 17pt,
  	framexrightmargin	= 17pt,
  	framexbottommargin	= 5pt,
	framextopmargin		= 5pt,
	moredelim			= **[is][\color{mauve}]{@}{@}
}
\lstset{
	literate				= {~} {\raise.17ex\hbox{$\scriptstyle\sim$}}{1}
}


\lstdefinelanguage{pseudo}{
  sensitive=false,
  morecomment=[l]//
}
\lstdefinestyle{pseudo}{
  language     = pseudo,
  commentstyle = \color{dkgreen}
}



\DeclareCaptionFormat{listing}{\rule{\dimexpr\textwidth+17pt\relax}{0.4pt}\par\vskip1pt#1#2#3}
\captionsetup[lstlisting]{format=listing,singlelinecheck=false, margin=0pt,labelsep=space,labelfont=bf}

\renewcommand\lstlistingname{Listing}



% ----------- Page Layout ------------- %
\usepackage{fullpage}
\headsep = 24pt % spacing between header and text
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{paralist}
\fancyhf{}
\fancyhead[LE]{\slshape \rightmark} % section
\fancyhead[RE]{\thepage}
\fancyhead[RO]{\slshape \leftmark} % chapter
\fancyhead[LO]{\thepage}
\pagestyle{fancy}
\setlength{\headheight}{15pt}



\newcommand{\HRule}{\rule{\linewidth}{0.3mm}}
\usepackage{blindtext} %Lorem Ipsum replica

% -------- Table of Contents --------- %
\usepackage[titles]{tocloft}
\usepackage[toc,page]{appendix}
\setlength{\cftbeforesecskip}{7pt}
%\setlength{\cftbeforechapskip}{7pt}
\setlength{\cftbeforetoctitleskip}{-20pt}



% -------- TikZ --------- %
\usepackage{tikz}
\usepackage{circuitikz}
\usepackage{tikz-timing}
%\input{timing}
\usepackage{pgf}
\usetikzlibrary{arrows,automata}
\usetikzlibrary{patterns}
\usetikzlibrary{shapes.geometric}

\usetikzlibrary{fit}
\usetikzlibrary{calc}
\usetikzlibrary{intersections}
\usetikzlibrary{backgrounds}

\tikzset{desc/.style={outer sep=0pt,inner sep=0pt,text centered,font=\scriptsize,fill=black!10}}

\newcommand{\texta}{Helpful\\ \tiny (to achieve the objective)}
\newcommand{\textb}{Harmful\\ \tiny (to achieve the objective)}
\newcommand{\textc}{\rotatebox[origin=r]{90}{\shortstack{Internal origin\\ \tiny (product\slash company attributes)}}}
\newcommand{\textd}{\rotatebox[origin=r]{90}{\shortstack{External origin\\ \tiny (environment\slash market attributes)}}}


% -------- To Do notes --------- %
\usepackage{todonotes}


%---------- URL ----------%
\usepackage{hyperref} % clickable references
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=blue,
    pdfborder = { 0 0 0 }
}
\def\UrlFont{}



\usepackage[hang,flushmargin]{footmisc}


\begin{document}

% --------- Title Page, Front Page --------- %
\setcounter{page}{1}
\pagenumbering{Alph}

\input{frontpage}



\begin{comment}

% --------- Front Matter --------- %
\setcounter{page}{1} % Start counter at 1
\pagenumbering{roman}


\renewcommand\contentsname{Table of Contents}
\tableofcontents
\thispagestyle{empty}
\addtocontents{toc}{\vskip20pt}
\newpage
\end{comment}



% --------- Main Matter --------- %
\setcounter{page}{1} % Start counter at 1
\pagenumbering{arabic}


\section{Purpose}
The purpose of this project is to get experience using the Rapidly-exploring Random Trees path planning method. In this method two trees are expanded from the points $q_{\text{start}}$ and $q_{\text{goal}}$ until the two trees are connected. The trees each explore space around them and also advance towards each other through the
use of a simple greedy heuristic. When the method is constructing the trees it is done iterative; for each of the trees a random point in $C_{\text{free}}$ is selected at a fixed distance $\epsilon$ from the current configuration until the two trees are connected by a collision free path (i.e. a path in $C_{\text{free}}$). An important note is that as the method picks the new points randomly it is needed to perform some statistics regarding the path length and computation time, to create a statement using confidence intervals. Also it is needed to select an optimal $\epsilon$ for the specific environment.


\section{Procedure}
\subsection{Selecting an Optimal Stepsize $\epsilon$}
For selecting an optimal stepsize for the environment several has been tested in the range from 0.001 through $2\pi$ in steps of $0.001 + j \cdot 2\pi/100$ with $j=0,1,\hdots,100$. Each of the stepsizes have been sampled 32 times and averaged to be sure that no ``outliers'' should represent the true values. The data extracted for each value of the stepsize is the;
\begin{itemize}\itemsep-3pt
 \item number of $q$ state vectors
 \item computation time
 \item the total path length that the tool travels (in eucledian space [metrics]).
\end{itemize}
This is plotted in a scatter plot to visualize the tendency/behavior, see figure \ref{fig:scatter}.

\begin{figure}[H]
 \centering
 \hspace{-1.5cm}
 \includegraphics[scale=0.8]{mand2_scatter}
 \caption{The scatter diagram illustrates two plots; (1) the computation time (blue) and (2) the total path length (green) plotted against the stepsize $\epsilon$.}
 \label{fig:scatter}
\end{figure}

From figure \ref{fig:scatter} it can be seen that the computation time is smallest in the stepsize region from approximately 1 to 2.5. It can also be seen that the total pathlength in this region is smallest at a stepsize of approximately 2.5. Therefore the stepsize $\epsilon = 2.52327$ is chosen for this environment.


\subsection{Testing the Path Planning with the Selected Stepsize}
With the chosen stepsize some statistics is needed to be performed to create a statement about the future behavior of the path planning. Therefore 10.000 samples was collected and analysed. Firstly the distribution of computation time and the total path length was needed to be examined. These are illustrated in figure \ref{fig:hist} and \ref{fig:pdf}.

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
	\hspace{-1cm}
        \includegraphics[width=1.2\textwidth]{hist_time}
	\caption{Histogram time}
	\label{fig:pdf_time}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=1.2\textwidth]{hist_length}
	\caption{Histogram length}
	\label{fig:pdf_length}
    \end{subfigure}
    \caption{Histogram for the computation time and the total path length of the path planning}
    \label{fig:hist}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
	\hspace{-1cm}
        \includegraphics[width=1.2\textwidth]{pdf_time}
	\caption{PDF time}
	\label{fig:pdf_time}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=1.2\textwidth]{pdf_length}
	\caption{PDF length}
	\label{fig:pdf_length}
    \end{subfigure}
    \caption{Probability Density Functions for the computation time and the total path length of the path planning}
    \label{fig:pdf}
\end{figure}

The distributions are right skewed normal distributions and the sudden cut off at the left of curve is due to natural causes, i.e. the computation time cannot be quicker and the path length cannot be smaller than the most direct way between the two points. Therefore the distributions are assumed to be normal distributions from which the following distribution parameters are estimated and from these the confidence intervals are conducted for the estimated variance and mean.\\

\noindent\textbf{Computation time:}
\vspace{-8pt}
\begin{eqnarray*}
    \hat{\mu} &= 0.0759086 \hspace{1.5cm}   95\%\text{ CI}[0.0750944, 0.0767228]\\
    \hat{\sigma}^2 &= 0.0415386 \hspace{1.5cm}  95\%\text{ CI}[0.0409708, 0.0421224]
\end{eqnarray*}
\textbf{Path Length:}
\vspace{-8pt}
\begin{eqnarray*}
    \hat{\mu} &  4.82713  \hspace{1.5cm}   95\%\text{ CI}[4.80972, 4.84454]\\
    \hat{\sigma}^2 & 0.888087  \hspace{1.5cm}   95\%\text{ CI}[0.875948, 0.90057]
\end{eqnarray*}

\section{Implementation}
The C++ program RRTFinal outputs a .txt file with all the Q vectors in the path. The Lua script imports this .txt file and uses the Q vectors to animate the states of the scene.


\end{document}