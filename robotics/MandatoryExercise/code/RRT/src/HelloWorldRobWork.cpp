#include <iostream>
#include <rw/rw.hpp>
#include <rwlibs/pathplanners/rrt/RRTPlanner.hpp>
#include <rwlibs/pathplanners/rrt/RRTQToQPlanner.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>
#include <rw/math/MetricFactory.hpp>
#include <iomanip>      // std::setw

using namespace std;
using namespace rw::common;
using namespace rw::kinematics;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::pathplanning;
using namespace rw::proximity;
using namespace rw::trajectory;
using namespace rwlibs::pathplanners;
using namespace rwlibs::proximitystrategies;

#define MAXTIME 10.

bool checkCollisions(Device::Ptr device, const State &state, const CollisionDetector &detector, const Q &q) {
	State testState;
	CollisionDetector::QueryResult data;
	bool colFrom;

	testState = state;
	device->setQ(q,testState);
	colFrom = detector.inCollision(testState,&data);
	if (colFrom) {
		cerr << "Configuration in collision: " << q << endl;
		cerr << "Colliding frames: " << endl;
		FramePairSet fps = data.collidingFrames;
		for (FramePairSet::iterator it = fps.begin(); it != fps.end(); it++) {
			cerr << (*it).first->getName() << " " << (*it).second->getName() << endl;
		}
		return false;
	}
	return true;
}

int main(int argc, char** argv) {
    const string wcFile = "/home/jjb/Dropbox/7.semester/ROVI/Kr16WallWorkCell/Scene.wc.xml";
	const string deviceName = "KukaKr16";
    const string itemName = "Bottle";
    const string toolName = "Tool";
	cout << "Trying to use workcell " << wcFile << " and device " << deviceName << endl;

	WorkCell::Ptr wc = WorkCellLoader::Factory::load(wcFile);
    Device::Ptr device = wc->findDevice(deviceName);
	if (device == NULL) {
		cerr << "Device: " << deviceName << " not found!" << endl;
		return 0;
	}

    // Init a statevector so that KUKA is at the bottle
    Q init(6,-3.142,-0.827,-3.002,-3.143,0.099,-1.573);

    // get the bottle frame
    Frame* bottle = wc->findFrame(itemName);
    // get the gripper frame
    Frame* gripper = device->getEnd();
    // get the default state of the scene
    State state = wc->getDefaultState();
    // set the Q vector to be at the bottle
    device->setQ(init,state);
    // grip the bottle
    Kinematics::gripFrame(bottle,gripper,state);


	CollisionDetector detector(wc, ProximityStrategyFactory::makeDefaultCollisionStrategy());
    PlannerConstraint constraint = PlannerConstraint::make(&detector,device,state);

	/** Most easy way: uses default parameters based on given device
		sampler: QSampler::makeUniform(device)
		metric: PlannerUtil::normalizingInfinityMetric(device->getBounds())
		extend: 0.05 */
	//QToQPlanner::Ptr planner = RRTPlanner::makeQToQPlanner(constraint, device, RRTPlanner::RRTConnect);



    for(int j = 0; j <= 100; j++){
        device->setQ(init,state);
        /** More complex way: allows more detailed definition of parameters and methods */
        QSampler::Ptr sampler = QSampler::makeConstrained(QSampler::makeUniform(device),constraint.getQConstraintPtr());
        QMetric::Ptr metric = MetricFactory::makeEuclidean<Q>();
        double extend = 0.001 +j * 2*M_PI/100;
        QToQPlanner::Ptr planner = RRTPlanner::makeQToQPlanner(constraint, sampler, metric, extend, RRTPlanner::RRTConnect);

        Q from(6,-3.142,-0.827,-3.002,-3.143,0.099,-1.573);
        Q to(6,1.571,0.006,0.030,0.153,0.762,4.490);

        if (!checkCollisions(device, state, detector, from))
            return 0;
        if (!checkCollisions(device, state, detector, to))
            return 0;

        QPath path;
        Timer t;

        t.resetAndResume();

        planner->query(from,to,path,MAXTIME);

        t.pause();

        double toolDist = 0;
        double toolRad = 0;

        for(int i = 0; i < path.size()-1; i++){

            device->setQ(path.at(i),state);
            Transform3D<> bTe1 = device->baseTend(state);
            device->setQ(path.at(i+1),state);
            Transform3D<> bTe2 = device->baseTend(state);
            toolDist+= (bTe1.P()-bTe2.P()).norm2();
            toolRad += (path.at(i) - path.at(i+1)).norm2();

         //cout << setw(10) << (path.at(i) - path.at(i+1)).norm2()<< endl;
        }

        cout <<"Eps: "<< setw(10)<< extend << "\tPs: "<< setw(10) << path.size() << "\tT: " << setw(10) << t.getTime() << "\tPL "<< setw(10) << toolDist << "\tPR "<< setw(10) << toolRad << endl;
        if (t.getTime() >= MAXTIME) {
            cout << "Notice: max time of " << MAXTIME << " seconds reached." << endl;
        }

    }

	cout << "Program done." << endl;
	return 0;
}
