clear;clc;

% Load dataset
data = load('/home/nicolai/Dropbox/Skole/7. Semester/ROVI/Projects/Robotics Mandatory 2/data_epsilon.txt');

x = data(:,1);
y = data(:,2);

x1 = data(:,1);
y1 = data(:,3);

scatter(x,y,'*')
hold on
scatter(x1,y1)
grid on
xlabel('Step size (\epsilon)')
ylabel('Computation Time [s] and Total Path Length [metric]')
legend('Computation Time','Total Path Length')
hold off

%%
clear;clc;

% Load dataset
%load('/home/nicolai/Dropbox/Skole/7. Semester/ROVI/Projects/Robotics Mandatory 2/data.txt');
load('/home/jjb/Dropbox/Projects/Robotics Mandatory 2/data.txt');

pathlength = data(:,4);
figure(1)
%hist(pathlength,100)
bar(hist(pathlength,100) ./ sum(hist(pathlength,100)));
hold on
xlabel('Path Length')
ylabel('Probability')
title('Histogram of path length')
hold off

computationtime = data(:,3);
figure(2)


%[f,x] = hist(computationtime,100)
%figure(2)
%bar(x,f/trapz(x,f));
bar(hist(computationtime,100) ./ sum(hist(computationtime,100)));
hold on
xlabel('Computation Time')
ylabel('Probability')
title('Histogram of computation time')
hold off

%%
mean_time = mean(computationtime);
mean_length = mean(pathlength);
sigma_length = var(pathlength);

pd = fitdist(computationtime,'Normal')
ci = paramci(pd)
y= pdf(pd,sort(computationtime));
figure(3)
%bar(x,f/trapz(x,f));
%hold on
plot(sort(computationtime),y,'LineWidth',2)
xlabel('Computation Time')
ylabel('Probability')
title('Probability Density Function of the Computation Time')
hold off


pd1 = fitdist(pathlength,'Normal')
ci1 = paramci(pd1)
y1= pdf(pd,sort(pathlength));
figure(4)
plot(sort(pathlength),y,'LineWidth',2)
xlabel('Path length [metric]')
ylabel('Probability')
title('Probability Density Function of the Path Length')
hold off

ci_bound = mean_length - (sigma_length/sqrt(10000))*norminv(1-(0.95/2))
