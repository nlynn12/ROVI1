wc = rws.getRobWorkStudio():getWorkCell()
state = wc:getDefaultState()
device = wc:findDevice("KukaKr16")

function setQ(q)
	qq = rw.Q(#q,q[1],q[2],q[3],q[4],q[5],q[6])
	device:setQ(qq,state)
	rws.getRobWorkStudio():setState(state)
	rw.sleep(1)
end

setQ({-3.142, -0.827, -3.002, -3.143, 0.099, -1.573})

bottle = wc:findFrame("Bottle")
rw.gripFrame(bottle, device:getEnd(), state)


q_vectors = {}
file = assert(io.open("/home/nicolai/Dropbox/Skole/7. Semester/ROVI/Projects/Robotics Mandatory 2/LUAdata.txt", "r"))
i = 0
j = 0
for line in file:lines() do
	i = i + 1
	q_vectors[i] = {}
	local string = line
	for w in (string .. ","):gmatch("([^,]*),") do
		j = j + 1
		q_vectors[i][j] = tonumber(w)
	end
	j = 0
end

for i=1,#q_vectors-1 do
	--local tmp = q_vectors[i]
	--q1=rw.Q(#tmp,tmp[1],tmp[2],tmp[3],tmp[4],tmp[5],tmp[6])
	--tmp = q_vectors[i+1]
	--q2=rw.Q(#tmp,tmp[1],tmp[2],tmp[3],tmp[4],tmp[5],tmp[6])
	--rw.LinearInterpolator(q1[4],q2[1],0.01)
	setQ(q_vectors[i])
end

print("Program finished")