\documentclass[10pt,conference,a4paper]{IEEEtran}

% ------- Enable UTF8 characters ------- %
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}


% --------------- Math ----------------- %
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mdframed}
\newcommand{\dif}{\mbox{$\mathrm{d}$}}
\newcommand{\ohm}{\mbox{$\Omega$}}
\usepackage{bm}

% ------------- Figures & Tables -------------- %
\usepackage{graphicx}
\usepackage{caption}
\usepackage{float}
\usepackage{subcaption}
\captionsetup[figure]{singlelinecheck=false, margin=0pt,labelsep=space,labelfont=bf,textfont=it}
\captionsetup[table]{singlelinecheck=false, margin=0pt,labelsep=space,labelfont=bf,textfont=it}
\usepackage{epstopdf} %MATLAB eps (vector) figures

\usepackage{array}
\newcolumntype{x}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}p{#1}}

\usepackage{blindtext}

\usepackage{fancyhdr}

\rhead{
\fontsize{9}{12}\selectfont 
\today}
\renewcommand{\headrulewidth}{0pt}

%---------- URL ----------%
\usepackage{hyperref} % clickable references
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black,
    pdfborder = { 0 0 0 }
}
\def\UrlFont{}

\usepackage{todonotes}

% --------------- Code ----------------- %
\usepackage{verbatim}
\usepackage{listings}
\lstset{
	frame				= lines,
  	language				= C++,
  	aboveskip			= 3mm,
  	belowskip			= 3mm,
  	captionpos			= t,
  	showstringspaces		= false,
  	columns				= flexible,
  	basicstyle			= {\small\ttfamily},
  	numbers				= left,
  	numberstyle			= \tiny\color{gray},
  	keywordstyle			= \color{blue},
  	commentstyle			= \color{dkgreen},
  	stringstyle			= \color{dkgreen},
  	breaklines			= true,
  	breakatwhitespace	= true,
  	tabsize				= 3,
  	xleftmargin			= 17pt,
  	framexleftmargin		= 17pt,
  	framexrightmargin	= 17pt,
  	framexbottommargin	= 5pt,
	framextopmargin		= 5pt,
	moredelim			= **[is][\color{mauve}]{@}{@}
}
\lstset{
	literate				= {~} {\raise.17ex\hbox{$\scriptstyle\sim$}}{1}
}


\lstdefinelanguage{pseudo}{
  sensitive=false,
  morecomment=[l]//
}
\lstdefinestyle{pseudo}{
  language     = pseudo,
  commentstyle = \color{dkgreen}
}



\DeclareCaptionFormat{listing}{\rule{\dimexpr\textwidth+17pt\relax}{0.4pt}\par\vskip1pt#1#2#3}
\captionsetup[lstlisting]{format=listing,singlelinecheck=false, margin=0pt,labelsep=space,labelfont=bf}

\renewcommand\lstlistingname{Listing}

%---------- Bibliography ----------%
\usepackage[
  backend=bibtex,
  %style=alpabethical,
  citestyle=numeric-comp,
  sorting=none
  %citestyle=verbose%-tcomp
  ]{biblatex}
\bibliography{bibliography} 

\usepackage[hang,flushmargin]{footmisc}


\begin{document}


 \title{Using the UR-6-85-5-A as a Welding Device\\[0.2cm]\large{3\textsuperscript{rd} Mandatory Exercise (RoVi1)}\\[0.2cm]\large{\today}} 
 \author{
 \IEEEauthorblockN{Nicolai~Anton~Lynnerup\\ \small{\url{nlynn12@student.sdu.dk}}\\M.Sc.Stud Robot Systems Engineering} \and
 \IEEEauthorblockA{University of Southern Denmark\\Faculty of Engineering\\Maersk Mc-Kinney Moller Institute} \and
 \IEEEauthorblockN{Jens-Jakob~Bentsen\\ \small{\url{jeben12@student.sdu.dk}}\\M.Sc.Stud Robot Systems Engineering}
 }
 \maketitle
 
 %\begin{abstract}
 %\blindtext
 %\end{abstract}
 
 \begin{IEEEkeywords}
  Robotics, Path Planning, Cubic Spline, Interpolation
 \end{IEEEkeywords}
 
 \pagestyle{fancy}
 %\thispagestyle{fancy}
 
\section{Introduction}
This exercise addresses the generation of a tool trajectory so that the UR-6-85-5-A used, can weld on a pallet. The frames that the tool is to pass through given in the task description where the rotation for all frames are; $RPY\left(0,\frac{\pi}{4},\pi\right)$ and the positional parts are listed below:
\begin{itemize}
 \item T1 with position Vec(-0.975,-0.450,-0.032)
 \item T2 with position Vec(-0.975,0.440,-0.032)
 \item T3 with position Vec(-0.974,0.449,-0.032)
 \item T4 with position Vec(-0.965,0.450,-0.032)
 \item T5 with position Vec(-0.475,0.450,-0.032)
\end{itemize}
It is noted that the trajectory has zero rotational velocity throughout. Furthermore there's only a change in $y$ between T1 and T2, where between T4 and T5 only the $x$ is changed. This results in an $L$--shaped tool trajectory.


\section{Methodology}

\subsection{Implementing and Employing Cubic Spline Interpolated}
In order to move the robot through the frames it is needed to tesselate between them due to the large displacements. It is furthermore required that it is the tool that travels through the given frames in order for it to complete its task of welding. To tesselate between them, cubic spline interpolation can be applied.

The general formulae for cubic spline interpolation, which can be applied to both joint and tool space is found to be (\cite{rob123}, p.65):
$$X_{i-1,i}(t)=a_i t^3+b_i t^2 + c_i t +d_i$$
where the parameters needs to be determined. This formulae describes a curve between $X_{i-1}$ and $X_{i}$ (In joint space $X_{i}\equiv \bm{q}^i$). As $N$ curves needs to be determined, this results in $4N$ parameters. This can be written as $2N$ linear equations in the unknown parameters whereas the remaining $2N$ linear equations can be given by the velocities through each of the passage points. Thus a set of 4 linear equations in 4 unknowns is obtained for each subpath. This can be written as (equation (5.22) (\cite{rob123} p.65)):

\begin{align}
 \nonumber C(t,&t_s,t_f,P_s,P_f,V_s,V_f) \\
			      \nonumber &= \left[ -2(P_f-P_s)+(t_f-t_s)(V_s+V_f)\right]\left[\frac{t-t_s}{t_f-t_s}\right]^3\\
			      \nonumber &+\left[3(P_f-P_s)-(t_f-t_s)(2V_s+V_f)\right]\left[\frac{t-t_s}{t_f-t_s}\right]^2\\
			      &+ V_s(t-t_s)+P_s
\end{align}
which returns a new point (a vector consisting of $x,y,z$) between $X_{i-1}$ and $X_i$, determined by the interpolation.

The cubic spline interpolation is implemented cf. equation (5.22) (\cite{rob123} p.65).


\subsection{Implementing and Employing Algorithm 3}
%All the following computations is done on the Intel$^{\small\textregistered}$ Core$^\text{TM}$ i5-2430M CPU @ 2.40GHz $\times$ 4 \\
To determine the corresponding joint curve from an interpolated tool curve $\bm{T}_{base}^{tool}(t)$, algorithm 3 (\cite{rob123} p.60) is implemented. The implementation is based on the teaching assistants implementation of algorithm 2\cite{TA}. Additionally RobWorks has its own implementation called \lstinline|JacobianIKSolver|\cite{IKSolver}, which has not been used.

Algorithm 3 ``Jacobian based inverse kinematics for interpolated tool curves'' requires submethods which has been implemented in previous exercises:
\begin{align}
 \Delta u &= W(T_{base}^{tool}(q),[T_{base}^{tool}]_{desired}(t_i))\label{eq:deltau}\\
 J(q)\Delta q &= \Delta u\label{eq:deltaq}
\end{align}
where equation (\ref{eq:deltau}) is computed by equation 4.13 in \cite{rob123} p. 43, and the solution to equation (\ref{eq:deltaq}) can be obained by taking the inverse and performing LU decomposition (only for quadratic matrices, e.g.: 6 DoF robot) or by taking the Moore--Penrose inverse by SVD (applies to all robots).

It should be noted that equation 4.13, $W_{rot}(R)$ (\cite{rob123} p. 43) must be computed based on the rule of thumb for machines with double precision, which states:
$$x=\frac{1}{2}||(R_{32}-R_{23},R_{13}-R_{31},R_{21}-R_{12})||$$
If:
\begin{table}[H]
 \begin{tabular}{p{5cm}p{4cm}}
  $x\geq 10^{-6}$ & Eq. 4.18 (\cite{rob123} p.45)\\
  $x\leq 10^{-6}$ and $R_{11}+R_{22}+R_{33} > 0 $ & Eq. 4.19 (\cite{rob123} p.45)\\
  $x\geq 10^{-6}$ and $R_{11}+R_{22}+R_{33} < 0 $ & Eq. 4.20 (\cite{rob123} p.46)\\
 \end{tabular}

\end{table}


\subsection{Materials}
For employing cubic spline interpolation and algorithm 3, the RobWorks framework developed at University of Southern Denmark is used. This framework is a collection of C++ libraries in which simulation and controlling multiple axis robots exists. The control part of the libraries includes e.g. kinematic modeling of several types of robot manipulators with serial, tree and parallel structures and path planning methods. The simulation tool used for this framework is the RobWork Studio which is illustrated in figure \ref{fig:init}.

\begin{figure}[H]
 \centering
 \includegraphics[width=0.9\linewidth]{img/init}
 \caption{The figure is a screenshot of the RobWork Studio in which the environment used for employing cubic spline interpolation and algorithm 3 (\cite{rob123} p.60). The robot is placed in its initial configuration of the welding process.}\label{fig:init}
\end{figure}


Furthermore as seen in figure \ref{fig:init} an environment is chosen in which a UR-6-85-5-A serial industrial manipulator is placed beside a pallet. The robot's task is to weld on two edges of the pallet where the cubic spline interpolation is to be applied in order to tesselate between the frames, see figure \ref{fig:corner}.

\begin{figure}[H]
 \centering
 \includegraphics[width=0.9\linewidth]{img/corner}
 \caption{The figure is a screenshot of the RobWork Studio in which the environment used for employing cubic spline interpolation algorithm 3 (\cite{rob123} p.60). The robot is placed in its corner configuration of the welding process.}\label{fig:corner}
\end{figure}




\section{Results}
\subsection{Ensuring $T1=Q$ (Q.3)}
\noindent Transform Q:\\
Transform3D(Vector3D(-0.975408, -0.449697, -0.0304272), Rotation3D(0.705849, -0.000208552, -0.708362, 0.00147271, -0.999997, 0.0017619, -0.708361, -0.00228685, -0.705847))\\
Transform T1: \\
Transform3D(Vector3D(-0.975, -0.45, -0.032), Rotation3D(0.707107, 8.65956e-17, -0.707107, 0, -1, -1.22465e-16, -0.707107, 8.65956e-17, -0.707107))
\subsection{Positions from Cubic Spline Interpolation (Q.4)}
\noindent t=0.5s Vector3D(-0.975, -0.0675, -0.032)\\
t=1.05s Vector3D(-0.975031, 0.455281, -0.032)\\
t=1.32s Vector3D(-0.982184, 0.450032, -0.032)\\
t=1.7s Vector3D(-0.78566, 0.45, -0.032)
\subsection{Joint Configurations from Algorithm 3 (Q.5)}
\noindent i=50 Q[6]\{-0.0848625, -0.817944, 1.35372, -0.53578, 1.48593, -1.5708\}\\
i=105 Q[6]\{-0.762995, -0.423898, 0.590323, -0.166426, 0.807802, -1.5708\}\\
i=132 Q[6]\{-0.750865, -0.415345, 0.5732, -0.157855, 0.819931, -1.5708\}\\
i=170 Q[6]\{-0.971858, -0.835992, 1.38724, -0.551243, 0.598939, -1.5708\}
\subsection{Modified Times after Time Scaling (Q.7)}
\noindent i=50 \ 0.0184307s\\
i=105 0.01s\\
i=132 0.01s\\
i=170 0.0165541s\\
Total time: 3.10918s\\

\begin{figure}[H]
 \centerline{\includegraphics[width=\linewidth]{img/timescale}}
\end{figure}


\printbibliography

\end{document}

\begin{comment}
 Question 3
Transform Q: 
Transform3D(Vector3D(-0.975408, -0.449697, -0.0304272), Rotation3D(0.705849, -0.000208552, -0.708362, 0.00147271, -0.999997, 0.0017619, -0.708361, -0.00228685, -0.705847))
Transform T1: 
Transform3D(Vector3D(-0.975, -0.45, -0.032), Rotation3D(0.707107, 8.65956e-17, -0.707107, 0, -1, -1.22465e-16, -0.707107, 8.65956e-17, -0.707107))

Question 4
t=0.5s Vector3D(-0.975, -0.0675, -0.032)
t=1.05s Vector3D(-0.975031, 0.455281, -0.032)
t=1.32s Vector3D(-0.982184, 0.450032, -0.032)
t=1.7s Vector3D(-0.78566, 0.45, -0.032)

Question 5
i=50 Q[6]{-0.0848625, -0.817944, 1.35372, -0.53578, 1.48593, -1.5708}
i=105 Q[6]{-0.762995, -0.423898, 0.590323, -0.166426, 0.807802, -1.5708}
i=132 Q[6]{-0.750865, -0.415345, 0.5732, -0.157855, 0.819931, -1.5708}
i=170 Q[6]{-0.971858, -0.835992, 1.38724, -0.551243, 0.598939, -1.5708}

Question 7
i=50 0.0184307
i=105 0.01
i=132 0.01
i=170 0.0165541
Total time: 3.10918
\end{comment}

