#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <cmath>

#include <rw/math/Q.hpp>
#include <rw/math/EAA.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/MetricFactory.hpp>
#include <rw/math/EigenDecomposition.hpp>
#include <rw/loaders/WorkCellFactory.hpp>
#include <rw/invkin.hpp>


using namespace std;
using namespace rw::math;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::kinematics;

#define EPSILON 1e-6	// error accepted in interpolation and inverse kinematic
#define MAX_ITE 100			// the maximum number of iterations


Vector3D<double> C(double t, double ts, double tf, Vector3D<double> Ps, Vector3D<double> Pf, Vector3D<double> Vs, Vector3D<double> Vf){
    return (-2*(Pf-Ps) + (tf-ts)*(Vs+Vf))*pow((t-ts)/(tf-ts),3)+(3*(Pf-Ps)-(tf-ts)*(2*Vs+Vf))*pow((t-ts)/(tf-ts),2)+Vs*(t-ts)+Ps;
}

VelocityScrew6D<double> calculateDeltaU(Transform3D<double> &Tbasetool, Transform3D<double> Tbasetooldesired){
    // making delta position
    Vector3D<double> deltaPdesired = Tbasetooldesired.P() - Tbasetool.P();

    // Compute the rotation [R^(i-1)]^T * R^i
    Rotation3D<double> rotationT = Tbasetool.R().inverse();//inverse(Tbasetool.R());
    Rotation3D<double> rotationDesired = Tbasetooldesired.R();
    Rotation3D<double> R = rotationDesired*rotationT;

    // Decide which W_rot(R) formulae to use (only valid for machines w/ double precision):
    Vector3D<double> deltaWdesired (R(2,1)-R(1,2),
                                    R(0,2)-R(2,0),
                                    R(1,0)-R(0,1));


    double x = 0.5 * deltaWdesired.norm2();

    if(x >= 10e-6){
        // Eq 4.18 p.45
        deltaWdesired *= (acos((R(0,0)+R(1,1)+R(2,2)-1)/(2)))/(deltaWdesired.norm2());
        //cout << setw(15) << x << setw(22) <<"Eq 4.18" << endl;
    }
    if(x <= 10e-6 && (R(0,0)+R(1,1)+R(2,2) - 1) > 0){
        // Eq 4.19 p.45
        deltaWdesired *= 0.5;
        //cout << setw(15) << x << setw(15) << proRotation(0,0)+proRotation(1,1)+proRotation(2,2) - 1 << setw(7) <<"Eq 4.19" << endl;
    }
    if(x <= 10e-6 && (R(0,0)+R(1,1)+R(2,2) - 1) < 0){
        //cout << setw(15) << x << setw(15) << proRotation(0,0)+proRotation(1,1)+proRotation(2,2) - 1 << setw(7) <<"Eq 4.20" << endl;
        // Initials to Eq 4.20 p.46
        int k = 0;
        int k_max_test = 0;
        for(int i = 0; i < 3; i++){
            if(sqrt(1+R(i,i))>k_max_test){
                k_max_test = sqrt(1+R(i,i));
                k = i;
            }
        }
        vector<double> alpha(3);
        alpha[k] = 1;
        //for i != k:
        //  alpha_i = sign(R_ik + R_ki)

        for(int i = 0; i < 3; i++){
            if(i != k){
                if(R(i,k) + R(k,i)<0) alpha[i] = -1;
                else alpha[i] = 1;
            }
        }

        // Eq 4.20 p.46
        /// W_ROT FUNCTION HERE!
        double alpha1 = alpha[0]*sqrt(1+R(0,0));
        double alpha2 = alpha[1]*sqrt(1+R(1,1));
        double alpha3 = alpha[2]*sqrt(1+R(2,2));
        Vector3D<double> altdWd(alpha1,alpha2,alpha3);
        deltaWdesired = (2*M_PI-deltaWdesired.norm2())/(2*sqrt(3+R(0,0)+R(1,1)+R(2,2)))*altdWd;

    }

    // W_rot eq.4.19 p.45
    // make new delta W
    //deltaWdesired *= 0.5;


    // making deltaU
    VelocityScrew6D<double> deltaU = VelocityScrew6D<double>(deltaPdesired(0),
                                                             deltaPdesired(1),
                                                             deltaPdesired(2),
                                                             deltaWdesired(0),
                                                             deltaWdesired(1),
                                                             deltaWdesired(2));


    return deltaU;
}

vector<Q> algorithm3_tesselation(const Device::Ptr device,
                                 State &state,
                                 const Frame* tool,
                                 Transform3D<double> &Tbasetool,
                                 Transform3D<double> Tbasetooldesired,
                                 Q &q,
                                 int M = 100) {
    vector<Q> list_Q;

    Transform3D<double> TbasetoolStart;
    TbasetoolStart = Tbasetool;

    //Compute M
    cout << "M: " << M <<endl;

    //Compute ThetaVDesired
    Rotation3D<double> rotationT = Tbasetool.R();//inverse(Tbasetool.R());
    Rotation3D<double> rotationDesired = Tbasetooldesired.R();
    Rotation3D<double> tmpRot = rotationDesired*rotationT;
    EAA<double>  thetaVd(tmpRot.inverse());

    //cout << thetaVd << endl;
    //cout << thetaVd.axis() << endl;

    Vector3D<double> PBTi;
    Rotation3D<double> RBTi;
    VelocityScrew6D<double> deltaU;

    //ofstream file;
    //file.open("LUAdata.txt");

    for(int i=1; i<=M; i++){
        //i/M is the current interpolation step
        float factor ((float)i/(float)M);



        //The Point Base -> Tool at i'th interpolation step
        PBTi = TbasetoolStart.P()+  (factor * (Tbasetooldesired.P()-TbasetoolStart.P())); //Wpos
        //PBTi = Tbasetool.P()+  (factor * (Tbasetooldesired.P()-Tbasetool.P())); //Wpos

        //The Rotation Base -> Tool at i'th interpolation step
        EAA<double> thetaVdtmp(thetaVd.axis(),(factor*thetaVd.angle()));
        //cout << thetaVdtmp << endl;
        //cout << thetaVdtmp.axis() << endl;

        RBTi = (thetaVdtmp.toRotation3D())*TbasetoolStart.R();

        //cout << thetaVdtmp.angle() << endl;

        //PBTi and RBTi
        Transform3D<double> TBTi(PBTi, RBTi);

        //cout<<"TBTI: "<< TBTi<< endl;
        deltaU = calculateDeltaU(Tbasetool,TBTi);

        // for each step making algorithm 1
        int run = 0;
        while( deltaU.norm2()>EPSILON && run<MAX_ITE ) {
            //cout << deltaU.norm2() << endl;
            // calculate the jacobian matrix
            Jacobian jacobian = device->baseJframe(tool,state);

            // solve J*dq=du => dq=J^(-1)*du (inverse does not modify jacobian)
            Q deltaQ( jacobian.e().inverse()*deltaU.e() );

            // add dq to from and set the device
            q += deltaQ;
            device->setQ(q,state);
            //cout << "q: " << q << endl;

            // Calculate the new T_base^tool

            Tbasetool = device->baseTframe(tool,state);

            // Calculate the new du
            deltaU = calculateDeltaU(Tbasetool,TBTi);

            run++;
            //cout << " " << run << "\t" << deltaU.norm2() << endl;
            if( run==MAX_ITE ) RW_THROW("Algorithm 3 did not find a solution within " << MAX_ITE << " iterations!");
            //if(run == MAX_ITE) break;
        }

        //cout<< "i: " << i << "q: "<< q << endl;
        //cout<< q << endl;
        //file << q[0] << ","<<q[1]<< ","<<q[2]<< ","<<q[3]<< ","<<q[4]<< ","<<q[5]<< endl;
        list_Q.push_back(q);

    }
    //file.close();
    return list_Q;
}

Q algorithm3(Transform3D<double> TBT, Transform3D<double> TBTdesired, Frame* tool, State state, Device::Ptr device){

    VelocityScrew6D<double> deltaU;
    deltaU = calculateDeltaU(TBT,TBTdesired);

    Q q = device->getQ(state);

    int run = 0;
    while( deltaU.norm2()>EPSILON && run<MAX_ITE ) {
        // calculate the jacobian matrix
        Jacobian jacobian = device->baseJframe(tool,state);

        // solve J*dq=du => dq=J^(-1)*du (inverse does not modify jacobian)
        Q deltaQ( jacobian.e().inverse()*deltaU.e() );

        // add dq to from and set the device
        q += deltaQ;
        device->setQ(q,state);
        //cout << "q: " << q << endl;

        // Calculate the new T_base^tool
        TBT = device->baseTframe(tool,state);

        // Calculate the new du
        deltaU = calculateDeltaU(TBT,TBTdesired);
        //cout << run << "\t" << deltaU << endl;

        run++;
        if( run==MAX_ITE ) RW_THROW("Algorithm 3 did not find a solution within " << MAX_ITE << " iterations!");
    }
    return q;
}


int main() {
    const string wcFile = "/home/nicolai/Documents/ROVI1/robotics/Workcells/URInterpolate/Scene.wc.xml";
    const string deviceName = "UR-6-85-5-A";
    const string toolName = "Tool";
    cout << "Trying to use workcell " << wcFile << " and device " << deviceName << endl;

    WorkCell::Ptr wc = WorkCellLoader::Factory::load(wcFile);
    Device::Ptr device = wc->findDevice(deviceName);
    Frame* tool = wc->findFrame(toolName);
    if (device == NULL) RW_THROW("Device: " << deviceName << " not found!");
    if (tool == NULL) RW_THROW("Tool: " << toolName << " not found!");


    vector<Q> allQ;


    // Q2
    Q init(6, 0.476,-0.440,0.620,-0.182,2.047,-1.574);
    State state = wc->getDefaultState();
    device->setQ(init,state);
    Transform3D<double> Qinit = device->baseTframe(tool,state);


    // ------ T1 ---------
    RPY<double> T1RPY(0,M_PI/4,M_PI);
    Vector3D<double> T1v(-0.975,-0.450,-0.032);
    Transform3D<double> T1(T1v,T1RPY.toRotation3D());

    // ------ T2 ---------
    RPY<double> T2RPY(0,M_PI/4,M_PI);
    Vector3D<double> T2v(-0.975,0.440,-0.032);
    Transform3D<double> T2(T2v,T2RPY.toRotation3D());
    // ------ T3 ---------
    RPY<double> T3RPY(0,M_PI/4,M_PI);
    Vector3D<double> T3v(-0.974,0.449,-0.032);
    Transform3D<double> T3(T3v,T3RPY.toRotation3D());
    // ------ T4 ---------
    RPY<double> T4RPY(0,M_PI/4,M_PI);
    Vector3D<double> T4v(-0.965,0.450,-0.032);
    Transform3D<double> T4(T4v,T4RPY.toRotation3D());
    // ------ T5 ---------
    RPY<double> T5RPY(0,M_PI/4,M_PI);
    Vector3D<double> T5v(-0.475,0.450,-0.032);
    Transform3D<double> T5(T5v,T5RPY.toRotation3D());

    // Q3:
    cout << "Question 3" << endl;
    cout << "Transform Q: " << endl << Qinit << endl;
    cout << "Transform T1: " << endl << T1 << endl;

    // Q4:
    cout << endl << "Question 4" << endl;
    Vector3D<double> cubicspline;
    cubicspline = C(0.5,0,1,T1.P(),T2.P(),     Vector3D<double>(0,0,0),        Vector3D<double>(0,0.5,0));
    cout << "t=0.5s " << cubicspline << endl;
    cubicspline = C(1.05,1,1.2,T2.P(),T3.P(),  Vector3D<double>(0,0.5,0),      Vector3D<double>(0.02,0.02,0));
    cout << "t=1.05s " << cubicspline << endl;
    cubicspline = C(1.32,1.2,1.4,T3.P(),T4.P(),Vector3D<double>(0.02,0.02,0),  Vector3D<double>(0.5,0,0));
    cout << "t=1.32s " << cubicspline << endl;
    cubicspline = C(1.7,1.4,2.4,T4.P(),T5.P(), Vector3D<double>(0.5,0,0),      Vector3D<double>(0,0,0));
    cout << "t=1.7s " << cubicspline << endl;

    // Q5:
    for(double t = 0.01; t<2.4; t+=0.01){
        Vector3D<double> cubicP;
        if(t > 0    && t <= 1)      cubicP = C(t,0,1,T1.P(),T2.P(),     Vector3D<double>(0,0,0),        Vector3D<double>(0,0.5,0));
        if(t > 1    && t <= 1.2)    cubicP = C(t,1,1.2,T2.P(),T3.P(),   Vector3D<double>(0,0.5,0),      Vector3D<double>(0.02,0.02,0));
        if(t > 1.2  && t <= 1.4)    cubicP = C(t,1.2,1.4,T3.P(),T4.P(), Vector3D<double>(0.02,0.02,0),  Vector3D<double>(0.5,0,0));
        if(t > 1.4  && t <= 2.4)    cubicP = C(t,1.4,2.4,T4.P(),T5.P(), Vector3D<double>(0.5,0,0),      Vector3D<double>(0,0,0));

        Transform3D<double> TBT = device->baseTframe(tool,state);
        Transform3D<double> TBTdesired(cubicP, T1.R());
        //TBT = TBTdesired;
        //TBTdesired = Transform3D<double>(cubicP,T1.R());

        Q Qspline = algorithm3(TBT,TBTdesired,tool,state,device);

        device->setQ(Qspline,state);

        allQ.push_back(Qspline);
    }
    cout << endl << "Question 5" << endl;
    cout << "i=50 " << allQ[50] << endl;
    cout << "i=105 " << allQ[105] << endl;
    cout << "i=132 " << allQ[132] << endl;
    cout << "i=170 " << allQ[170] << endl;



    // Q6:
    ofstream file;
    file.open("LUAdataCubic.txt");
    for(unsigned int i = 0; i<allQ.size();i++){
        file << allQ[i][0] << ","<<allQ[i][1]<< ","<<allQ[i][2]<< ","<<allQ[i][3]<< ","<<allQ[i][4]<< ","<<allQ[i][5]<< endl;
    }
    file.close();



    // Q7:
    double deltaT = 0.01;
    vector<double> tau;

    for(unsigned int i = 0; i < allQ.size()-1; i++){
        deltaT = 0.01;
        Q deltaQ = allQ[i+1] - allQ[i];

        Q VelLim = device->getVelocityLimits();

        int maxViolatorJoint;
        bool hasViolated = false;
        for(int i = 0; i < 6; i++){
            double maxViolaterValue = 0;
            if(abs(deltaQ[i]/deltaT) > VelLim[i] && abs(deltaQ[i]/deltaT)/VelLim[i] > maxViolaterValue ){
                maxViolatorJoint = i;
                hasViolated = true;
                maxViolaterValue = abs(deltaQ[i]/deltaT)/VelLim[i];
            }
        }

        if(hasViolated){
            double scalefactor = (abs(deltaQ[maxViolatorJoint])/deltaT) / VelLim[maxViolatorJoint];
            deltaT *= scalefactor;
        }
        tau.push_back(deltaT);
    }
    cout << endl << "Question 7" << endl;
    cout << "i=50 " << tau[50] << endl;
    cout << "i=105 " << tau[105] << endl;
    cout << "i=132 " << tau[132] << endl;
    cout << "i=170 " << tau[170] << endl;

    double totaltime = 0;
    for(unsigned int i = 0; i < tau.size(); i++){
        totaltime += tau[i];
    }
    cout << "Total time: " << totaltime << endl;

    ofstream taufile;
    taufile.open("tau.txt");
    for(unsigned int i = 0; i<tau.size();i++){
        taufile << tau[i] << endl;
    }
    taufile.close();

    cout << "Program done." << endl;
    return 0;
}
