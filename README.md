RoVi1 - Introduction to Robotics and Computer Vision
====
University of Southern Denmark

Robotics
========
Knowledge
---------
* The principles of the geometry and kinematics in robot and vision systems.
* Homogeneous transformations
* Camera-scene calibration
* Trajectory generation and representation
* Basic point to point robot planning algorithms

Skills
---------
* Develop and use kinematic models for robots
* Use homogeneous transformations to compute position and rotation in 3D.
* Setup and solve camera scene calibration issues
* Represent robot tasks as trajectories
* Implement and use robot planning algorithms
* Use software tools for visualizing robots

Competences
---------
* Solve tasks involving forward and inverse kinematics, point to point planning, basic robot calibration and trajectory generation



Computer vision
========
Knowledge
---------
* Basic image characteristics (Image structure, Colour)
* Pinhole camera model and simple multi camera geometry
* Basic image operations
* Intensity transformations
* Filtering operations
* Features and extraction processes
* Representations and descriptors for object recognition

Skills
------
* Apply existing image processing toolboxes (e.g. OpenCV)
* Ability to judge image degradation, select appropriate filters and apply these
* Select appropriate features for recognition and related tasks
* Construct object recognition/pose estimation algorithms for 2D problems
* Simple 3D reconstruction for points

Competences
-----------
* Ability to solve simple 2D computer vision and image restoration problems