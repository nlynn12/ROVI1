clear;
clc;
close all;
legendfontsize  = 10;

filename = '0_dq.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_0 = importdata(filename,delimiterIn,headerlinesIn);

filename = '1_dq.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_1 = importdata(filename,delimiterIn,headerlinesIn);

filename = '2_dq.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_2 = importdata(filename,delimiterIn,headerlinesIn);

clear filename; clear headerlinesIn; clear delimiterIn;
 
C = {'k','b','r','g','c',[.5 .6 .7],[.8 .2 .6]}; % Cell array of colros.
 figure ('Name','q_0','NumberTitle','off')
 for k = 2: size(q_0.data,2)
     plot(q_0.data(:, 1), q_0.data(:, k),'color',C{k-1})
     hold on;
 end
 h_legend = legend({'q0';'q1';'q2';'q3';'q4';'q5';'q6'});
 set(h_legend,'FontSize',legendfontsize);
 title('\fontsize{16}\Delta q: Speed Fast');
 xlabel('Time [s]');
 ylabel('\Delta Joint Configuration [rad]');
 
 print('dq_0_fast','-depsc');


 figure ('Name','q_1','NumberTitle','off')
 for k = 2: size(q_1.data,2)
 plot(q_1.data(:, 1), q_1.data(:, k),'color',C{k-1})
 hold on;
 end
 h_legend = legend({'q0';'q1';'q2';'q3';'q4';'q5';'q6'});
 set(h_legend,'FontSize',legendfontsize);
  title('\fontsize{16}\Delta q: Speed Medium');
  xlabel('Time [s]')
 ylabel('\Delta Joint Configuration [rad]')
 
  print('dq_1_medium','-depsc');
 
  figure ('Name','q_2','NumberTitle','off')
 for k = 2: size(q_2.data,2)
 plot(q_2.data(:, 1), q_2.data(:, k),'color',C{k-1})
 hold on;
 end
 h_legend = legend({'q0';'q1';'q2';'q3';'q4';'q5';'q6'});
 set(h_legend,'FontSize',legendfontsize);
  title('\fontsize{16}\Delta q: Speed Slow'); 
 xlabel('Time [s]')
 ylabel('\Delta Joint Configuration [rad]')
 
  print('dq_2_slow','-depsc');
  close all;