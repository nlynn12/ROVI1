 clear;
clc;
close all;
filename = '0_te.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_0 = importdata(filename,delimiterIn,headerlinesIn);

filename = '1_te.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_1 = importdata(filename,delimiterIn,headerlinesIn);

filename = '2_te.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_2 = importdata(filename,delimiterIn,headerlinesIn);
%clear filename;
clear headerlinesIn; clear delimiterIn;
legendfontsize = 10;


 %%
C = {'k','b','r','g','c',[.5 .6 .7],[.8 .2 .6]}; % Cell array of colros.
 figure ('Name','q_0','NumberTitle','off')
 for k = 2: size(q_0.data,2)
     plot(q_0.data(:, 1), q_0.data(:, k),'color',C{4})
     hold on;
     plot(q_1.data(:, 1), q_1.data(:, k),'color',C{2})
     plot(q_2.data(:, 1), q_2.data(:, k),'color',C{3})
     
 end
 h_legend = legend({'Fast';'Medium';'Slow';});
 set(h_legend,'FontSize',legendfontsize);
 title('\fontsize{16}Tracking Error');
 xlabel('\Delta t [s]');
 ylabel('Tracking Error [Pixels]');
 
 print('te_all','-depsc');

 
 close all;