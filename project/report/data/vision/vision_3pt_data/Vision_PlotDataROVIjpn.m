clear;
clc;
close all;
legendfontsize = 10;

filename = 'vision_jpn_Fast_3pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_0 = importdata(filename,delimiterIn,headerlinesIn);

filename = 'vision_jpn_Medium_3pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_1 = importdata(filename,delimiterIn,headerlinesIn);

filename = 'vision_jpn_Slow_3pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_2 = importdata(filename,delimiterIn,headerlinesIn);

clear filename; clear headerlinesIn; clear delimiterIn;

%%
C = {'k','b','r','g','c',[.5 .6 .7],[.8 .2 .6]}; % Cell array of colros.
 figure ('Name','q_0','NumberTitle','off')
 for k = 2: size(q_0.data,2)
     plot(q_0.data(:, 1), q_0.data(:, k),'color',C{k-1})
     hold on;
 end
 h_legend = legend({'q0';'q1';'q2';'q3';'q4';'q5';'q6'});
 set(h_legend,'FontSize',legendfontsize);
 title('\fontsize{16}Joint Positions Normed: Fast');
 xlabel('Time [s]');
 ylabel('Joint Configuration [rad]');
 
 print('jpn_fast','-depsc');


 figure ('Name','q_1','NumberTitle','off')
 for k = 2: size(q_1.data,2)
 plot(q_1.data(:, 1), q_1.data(:, k),'color',C{k-1})
 hold on;
 end
 h_legend = legend({'q0';'q1';'q2';'q3';'q4';'q5';'q6'});
 set(h_legend,'FontSize',legendfontsize);
  title('\fontsize{16}Joint Positions Normed: Medium');
  xlabel('Time [s]')
 ylabel('Joint Configuration [rad]')
 
  print('jpn_medium','-depsc');
 
  figure ('Name','q_2','NumberTitle','off')
 for k = 2: size(q_2.data,2)
 plot(q_2.data(:, 1), q_2.data(:, k),'color',C{k-1})
 hold on;
 end
 h_legend = legend({'q0';'q1';'q2';'q3';'q4';'q5';'q6'});
 set(h_legend,'FontSize',legendfontsize);
  title('\fontsize{16}Joint Positions Normed: Slow');
 xlabel('Time [s]')
 ylabel('Joint Configuration [rad]')
 print('jpn_slow','-depsc');
 
 close all;

% xmin = -100;
% xmax = 100;
% n = 20;
% bla = xmin+rand(1,n)*(xmax-xmin);
% 
% minVal = min(bla);
% maxVal = max(bla);
% norm_data = (bla - minVal) / ( maxVal - minVal );