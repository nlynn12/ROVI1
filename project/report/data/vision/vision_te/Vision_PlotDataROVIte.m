clear;
clc;
close all;
filename = 'vision_te_Fast_1pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
f1 = importdata(filename,delimiterIn,headerlinesIn);

filename = 'vision_te_Medium_1pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
m1 = importdata(filename,delimiterIn,headerlinesIn);

filename = 'vision_te_Slow_1pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
s1 = importdata(filename,delimiterIn,headerlinesIn);

%%
filename = 'vision_te_Fast_2pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
f2 = importdata(filename,delimiterIn,headerlinesIn);

filename = 'vision_te_Medium_2pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
m2 = importdata(filename,delimiterIn,headerlinesIn);

filename = 'vision_te_Slow_2pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
s2 = importdata(filename,delimiterIn,headerlinesIn);

%% 
filename = 'vision_te_Fast_3pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
f3 = importdata(filename,delimiterIn,headerlinesIn);

filename = 'vision_te_Medium_3pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
m3 = importdata(filename,delimiterIn,headerlinesIn);

filename = 'vision_te_Slow_3pt.txt';
delimiterIn = ',';
headerlinesIn = 1;
s3 = importdata(filename,delimiterIn,headerlinesIn);


%clear filename;
clear headerlinesIn; clear delimiterIn;
legendfontsize = 10;

 %%
C = {'k','b','r','g','c',[.5 .6 .7],[.8 .2 .6]}; % Cell array of colors.
 figure ('Name','Fast','NumberTitle','off')
 for k = 2: size(f1.data,2)
     plot(f1.data(:, 1), f1.data(:, k),'color',C{4})
     hold on;
     plot(f2.data(:, 1), f2.data(:, k),'color',C{2})
     plot(f3.data(:, 1), f3.data(:, k),'color',C{3})
 end
 h_legend = legend({'1pt';'2pt';'3pt';});
 set(h_legend,'FontSize',legendfontsize);
 title('\fontsize{16}Tracking Error: Fast');
 xlabel('Time [s]');
 ylabel('Tracking Error [Pixels]');
 print('vision_te_fast','-depsc');
 
  figure ('Name','Medium','NumberTitle','off')
 for k = 2: size(m1.data,2)
     plot(m1.data(:, 1), m1.data(:, k),'color',C{4})
     hold on;
     plot(m2.data(:, 1), m2.data(:, k),'color',C{2})
     plot(m3.data(:, 1), m3.data(:, k),'color',C{3})
 end
 
 h_legend = legend({'1pt';'2pt';'3pt';});
 set(h_legend,'FontSize',legendfontsize);
 title('\fontsize{16}Tracking Error: Medium');
 xlabel('Time [s]');
 ylabel('Tracking Error [Pixels]');
 print('vision_te_medium','-depsc');
 
 
   figure ('Name','Slow','NumberTitle','off')
 for k = 2: size(s1.data,2)
     plot(s1.data(:, 1), s1.data(:, k),'color',C{4})
     hold on;
     plot(s2.data(:, 1), s2.data(:, k),'color',C{2})
     plot(s3.data(:, 1), s3.data(:, k),'color',C{3})
 end
 
 h_legend = legend({'1pt';'2pt';'3pt';});
 set(h_legend,'FontSize',legendfontsize);
 title('\fontsize{16}Tracking Error: Slow');
 xlabel('Time [s]');
 ylabel('Tracking Error [Pixels]');
 print('vision_te_slow','-depsc');

close all;