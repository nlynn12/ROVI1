clear;
clc;
close all;

legendfontsize = 8;

filename = '0_tpose.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_0 = importdata(filename,delimiterIn,headerlinesIn);

filename = '1_tpose.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_1 = importdata(filename,delimiterIn,headerlinesIn);

filename = '2_tpose.txt';
delimiterIn = ',';
headerlinesIn = 1;
q_2 = importdata(filename,delimiterIn,headerlinesIn);

 clear filename; clear headerlinesIn; clear delimiterIn;
 
 C = {'k','b','r','g','c',[.5 .6 .7],[.8 .2 .6]}; % Cell array of colros.
 figure ('Name','q_0','NumberTitle','off')
 [hAx,hLine1,hLine2] = plotyy(...
 q_0.data(:, 1),[q_0.data(:, 2)'; q_0.data(:, 3)'; q_0.data(:, 4)'],...
 q_0.data(:, 1),[q_0.data(:, 5)'; q_0.data(:, 6)'; q_0.data(:, 7)']);
 set(hLine1,'LineStyle','-')
 set(hLine2,'LineStyle','--')
 set(hAx(1),'YTick',(-4:0.25:4))
 set(hAx(2),'YTick',(-4:0.5:4))

 h_legend = legend({'x[m]';'y[m]';'z[m]';'r[rad]';'p[rad]';'y[rad]'});
 set(h_legend,'FontSize',legendfontsize);
 
 title('\fontsize{16}Tool Pose: Speed Fast');
 xlabel('Time [s]');
 ylabel(hAx(1),'Tool Pose Position [m]');
 ylabel(hAx(2),'Tool Pose Orientation [rad]');
 print('tp_0_fast','-depsc');

 q_0 = q_1;
 figure ('Name','q_1','NumberTitle','off')
 [hAx,hLine1,hLine2] = plotyy(q_0.data(:, 1),[ q_0.data(:, 2)'; q_0.data(:, 3)'; q_0.data(:, 4)'], ...
 q_0.data(:, 1)', [q_0.data(:, 5)'; q_0.data(:, 6)'; q_0.data(:, 7)']);
 set(hLine1,'LineStyle','-')
 set(hLine2,'LineStyle','--')
 set(hAx(1),'YTick',(-4:0.25:4))
 set(hAx(2),'YTick',(-4:0.5:4))
 
 h_legend = legend({'x[m]';'y[m]';'z[m]';'r[rad]';'p[rad]';'y[rad]'});
 set(h_legend,'FontSize',legendfontsize);
 
 title('\fontsize{16}Tool Pose: Speed Medium');
 xlabel('Time [s]');
 ylabel(hAx(1),'Tool Pose Position [m]');
 ylabel(hAx(2),'Tool Pose Orientation [rad]');
 print('tp_1_medium','-depsc'); 
 
 
 q_0 = q_2;
 figure ('Name','q_2','NumberTitle','off')
 [hAx,hLine1,hLine2] = plotyy(q_0.data(:, 1),[ q_0.data(:, 2)'; q_0.data(:, 3)'; q_0.data(:, 4)'],...
 q_0.data(:, 1)', [q_0.data(:, 5)'; q_0.data(:, 6)'; q_0.data(:, 7)']);
 set(hAx(1),'YTick',(-4:0.25:4))
 set(hAx(2),'YTick',(-4:0.5:4))
 set(hLine1,'LineStyle','-')
 set(hLine2,'LineStyle','--')

 h_legend = legend({'x[m]';'y[m]';'z[m]';'r[rad]';'p[rad]';'y[rad]'});
 set(h_legend,'FontSize',legendfontsize);
 
 title('\fontsize{16}Tool Pose: Speed Slow');
 xlabel('Time [s]');
 ylabel(hAx(1),'Tool Pose Position [m]');
 ylabel(hAx(2),'Tool Pose Orientation [rad]');
 print('tp_2_slow','-depsc');
 close all;