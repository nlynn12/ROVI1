#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <numeric>
#include <chrono>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/legacy/legacy.hpp>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"


using namespace std;
using namespace cv;

/*int nFeatures = 50;
int nOctaveLayers = 3;
int contrastThreshold = 1;
int edgeThreshold = 10;
int sigma = 16;*/


Mat color_object_roi;
Mat object_gray;
vector<KeyPoint> keypoints_object;
SurfFeatureDetector surf(2.50e2,4,2);
SurfDescriptorExtractor extractor;
cv::Mat descriptors_object;

int img_num = 1;
int path_ctrl = 0;
stringstream ss;
string file_path_easy = "../../images/marker_corny/marker_corny_";
string file_path_hard = "../../images/marker_corny_hard/marker_corny_hard_";
string path;

void on_trackbar(int, void*){
    switch (path_ctrl) {
    case 0:
        path = file_path_easy;
        break;
    case 1:
        path = file_path_hard;
    default:
        break;
    }

    if(img_num < 1) img_num = 1;
    if(path == file_path_easy && img_num > 30) img_num = 30;
    ss << setw(2) << setfill('0') << img_num;
    string number = ss.str();
    string t_path = path + number + ".png";
    ss.str(string());

    Mat scene_color = imread(t_path, CV_LOAD_IMAGE_COLOR);
    if (scene_color.empty()){
        cout << t_path + ": No such file or directory" << endl;
        exit(-1);
    }

    // Transform to grayscale
    Mat scene_gray;
    cvtColor(scene_color, scene_gray, CV_BGR2GRAY);

    // SURF
    vector<KeyPoint> keypoints_scene;
    surf(scene_gray, Mat(), keypoints_scene);

    // Extract descriptors
    cv::Mat descriptors_scene;
    extractor.compute(scene_gray,keypoints_scene,descriptors_scene);


    /// Object detection:
    // Compare the object image with the scene
    FlannBasedMatcher matcher;
    std::vector<DMatch> matches;
    matcher.match(descriptors_object, descriptors_scene, matches);

    double max_dist = 0; double min_dist = 100;

    // Compute min and max distance keypoints
    for( int i = 0; i < descriptors_object.rows; i++ ){
        double dist = matches[i].distance;
        if(dist < min_dist) min_dist = dist;
        if(dist > max_dist) max_dist = dist;
    }


    // Remove bad matches based on former distance calculation
    std::vector< DMatch > good_matches;
    for( int i = 0; i < descriptors_object.rows; i++ ){
        if(matches[i].distance < 3*min_dist) good_matches.push_back( matches[i]);
    }

    // Draw matches
    Mat img_matches;
    drawMatches(color_object_roi,keypoints_object,
                scene_color,keypoints_scene,
                good_matches,img_matches,Scalar::all(-1), Scalar::all(-1),
                vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);


    // Localize the object in the scene
    std::vector<Point2f> object;
    std::vector<Point2f> scene;

    // Get the keypoints from the good matches
    for( int i = 0; i < good_matches.size(); i++ ){
        object.push_back( keypoints_object[ good_matches[i].queryIdx ].pt );
        scene.push_back( keypoints_scene[ good_matches[i].trainIdx ].pt );
    }

    Mat H = findHomography(object, scene, CV_RANSAC);

    // Get the corners from the color_object_roi
    std::vector<Point2f> object_corners(4);
    object_corners[0] = cvPoint(0,0);
    object_corners[1] = cvPoint( color_object_roi.cols, 0 );
    object_corners[2] = cvPoint( color_object_roi.cols, color_object_roi.rows );
    object_corners[3] = cvPoint( 0, color_object_roi.rows );
    std::vector<Point2f> scene_corners(4);

    perspectiveTransform(object_corners, scene_corners, H);

    for(auto corner : scene_corners)
        cout << "\t(" << corner.x << "," << corner.y << ")" << endl;

    //-- Draw lines between the corners (the mapped object in the scene - image_2 )
    line( img_matches, scene_corners[0] + Point2f( color_object_roi.cols, 0), scene_corners[1] + Point2f( color_object_roi.cols, 0), Scalar(0, 255, 0), 4 );
    line( img_matches, scene_corners[1] + Point2f( color_object_roi.cols, 0), scene_corners[2] + Point2f( color_object_roi.cols, 0), Scalar( 0, 255, 0), 4 );
    line( img_matches, scene_corners[2] + Point2f( color_object_roi.cols, 0), scene_corners[3] + Point2f( color_object_roi.cols, 0), Scalar( 0, 255, 0), 4 );
    line( img_matches, scene_corners[3] + Point2f( color_object_roi.cols, 0), scene_corners[0] + Point2f( color_object_roi.cols, 0), Scalar( 0, 255, 0), 4 );

    imshow("Control",img_matches);
}


int main(){
    // Load training image
    Mat color_object = imread("../../images/marker_corny/marker_corny_01.png", CV_LOAD_IMAGE_COLOR);
    if (color_object.empty()){
        cout << KRED << "Error: " << KNRM << "No such file or directory" << endl;
        return -1;
    }

    // Load training image ROI
    Rect roi(582,84, 975-582,467-84);
    color_object_roi = color_object(roi);

    // Transform to grayscale
    cvtColor(color_object_roi, object_gray, CV_BGR2GRAY);

    // SURF
    surf(object_gray, Mat(), keypoints_object);

    // Extract descriptors from grayscale image
    extractor.compute(object_gray,keypoints_object,descriptors_object);


    namedWindow("Control",CV_WINDOW_NORMAL);
    createTrackbar("Path", "Control", &path_ctrl, 1, on_trackbar);
    createTrackbar("Image", "Control", &img_num, 52, on_trackbar);
    /*createTrackbar("Features", "Control", &nFeatures, 100, on_trackbar);
    createTrackbar("Octave Layers", "Control", &nOctaveLayers, 5, on_trackbar);
    createTrackbar("Contrast Threshold", "Control", &contrastThreshold, 100, on_trackbar);
    createTrackbar("Edge Threshold", "Control", &edgeThreshold, 100, on_trackbar);
    createTrackbar("Sigma", "Control", &sigma, 100, on_trackbar);*/

    waitKey(0);
    destroyAllWindows();
    return 0;
}

