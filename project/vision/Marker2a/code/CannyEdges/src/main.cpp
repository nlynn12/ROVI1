#include <iostream>
#include <vector>
#include <string>
#include <numeric>
#include <sstream>
#include <iomanip>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

Mat src, src_blur,src_edges;

int thres_slider_max = 255;
int low_threshold = 0;
int high_threshold = 100;



void CannyEdges(){
    blur( src, src_blur, cv::Size(5,5) );
    //Canny(src_blur, src_edges, 100, low_threshold, 3);
    Canny(src_blur, src_edges,low_threshold,high_threshold);
    namedWindow("Canny Edges",cv::WINDOW_NORMAL);
    imshow("Canny Edges", src_edges);
    waitKey(0);
}

void on_trackbar(int, void*){
    CannyEdges();
}

int main(){
    // Load image
    src = imread("../../images/marker_thinline/marker_thinline_01.png", CV_LOAD_IMAGE_COLOR);
    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }

    string sourceWindow = "source";
    namedWindow(sourceWindow,cv::WINDOW_NORMAL);
    imshow(sourceWindow,src);
    createTrackbar( "Low", sourceWindow, &low_threshold, thres_slider_max, on_trackbar );
    createTrackbar( "High", sourceWindow, &high_threshold, thres_slider_max, on_trackbar );

    //imshow("Src",src);
    waitKey(0);

    //CannyE();

    waitKey(0);
    destroyAllWindows();

    return 0;
}

