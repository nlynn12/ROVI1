#include <iostream>
#include <vector>
#include <string>
#include <numeric>
#include <sstream>
#include <iomanip>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

Mat src, src_blur,src_edges;
vector<Vec2f> lines;

int thres_slider_max = 255;
int _threshold = 100;



void CannyEdges(){
    blur( src, src_blur, cv::Size(5,5) );
    //Canny(src_blur, src_edges, 100, low_threshold, 3);
    Canny(src_blur, src_edges,128,168);
    namedWindow("Canny Edges",cv::WINDOW_NORMAL);
    imshow("Canny Edges", src_edges);
    waitKey(0);
}

void HoughTransform(){
    if(_threshold<=1){
        _threshold = 1;
    }
    HoughLines(src_edges,lines,1,M_PI/180,_threshold);

    // Draw lines
    Mat tmp = src.clone();
    Point p1,p2;
    for(auto l : lines){
        p1.x = l[0] * cos(l[1]);
        p1.y = l[0] * sin(l[1]);
        p2.x = p1.x + 1000*sin(l[1]);
        p2.y = p1.y + 1000*-cos(l[1]);
        line(tmp,p1,p2,Scalar(0,0,255),1,8,0);
        circle(tmp,p1,2,Scalar(0,255,0));
    }
    imshow("Hough Transform",tmp);
}

void on_trackbar(int, void*){
    HoughTransform();
}

int main(){
    // Load image
    src = imread("../../images/marker_thinline/marker_thinline_01.png", CV_LOAD_IMAGE_COLOR);
    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }

    CannyEdges();



    string sourceWindow = "Hough Transform";
    namedWindow(sourceWindow,cv::WINDOW_NORMAL);
    imshow(sourceWindow,src);
    createTrackbar( "HT", sourceWindow, &_threshold, thres_slider_max, on_trackbar );

    waitKey(0);
    destroyAllWindows();

    return 0;
}

