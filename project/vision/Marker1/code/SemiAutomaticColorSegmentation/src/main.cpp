#include <iostream>
#include <vector>
#include <string>
#include <numeric>
#include <sstream>
#include <iomanip>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;



int main(){
    // Load image
    Mat src = imread("../MarkerColor/marker_color_01.png", CV_LOAD_IMAGE_COLOR);
    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }
    // Create ROI and compute mean of it
    // Red
    //cv::Rect roi(658,158,50,50);
    // Blue
    cv::Rect roi(658,342,50,50);

    cv::Mat roi_src = src(roi);
    cv::Scalar mean_s, stddev_s;
    cv::meanStdDev(roi_src,mean_s,stddev_s);

    cv::Mat_<float> mean(3,1);
    for(int i = 0; i < 3; ++i) {
        mean(i,0) = mean_s[i];
    }

    const float euclideanDistTol = 0.15*255;
    stringstream ss;
    string path = "../MarkerColor/marker_color_";

    //Mat_<Vec3f> threshold_src;
    for(int i = 1; i<=30; i++){
        ss << setw(2) << setfill('0') << i;
        string number = ss.str();
        string t_path = path + number + ".png";
        ss.str(string());

        Mat src = imread(t_path, CV_LOAD_IMAGE_COLOR);
        if (src.empty()){
            cout << t_path + ": No such file or directory" << endl;
            return -1;
        }
        Mat_<Vec3f> threshold_src = src.clone();

        for(int y = 0; y < src.rows; y++){
            for(int x = 0; x < src.cols; x++){
                Vec3f &bgr = threshold_src(y,x);
                Mat_<float> bgrm(bgr,false);
                if(cv::norm(bgrm-mean) <= euclideanDistTol)
                    bgr = Vec3f(255,255,255);
                else
                    bgr = Vec3f(0,0,0);

            }
        }
        Mat kernel = Mat::ones(9,9,CV_8U);
        erode(threshold_src,threshold_src,kernel);
        dilate(threshold_src,threshold_src,kernel);
        imwrite("../ResultsB/res"+number+".png",threshold_src);
    }

    /*float euclideanDistTol = 0.2*255;
    Mat_<Vec3f> threshold_src = src.clone();

    for(int y = 0; y < src.rows; y++){
        for(int x = 0; x < src.cols; x++){
            Vec3f &bgr = threshold_src(y,x);
            Mat_<float> bgrm(bgr,false);
            if(cv::norm(bgrm-mean) <= euclideanDistTol)
                bgr = Vec3f(1,1,1);
            else
                bgr = Vec3f(0,0,0);

        }
    }
    imshow("Thresholded Src",threshold_src);
    waitKey(0);

    Mat kernel = Mat::ones(9,9,CV_8U);
    erode(threshold_src,threshold_src,kernel);
    dilate(threshold_src,threshold_src,kernel);


    imshow("Thresholded image", threshold_src);
    waitKey(0);
*/
    waitKey(0);
    destroyAllWindows();

    return 0;
}

