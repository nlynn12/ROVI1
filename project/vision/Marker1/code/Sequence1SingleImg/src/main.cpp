#include <iostream>
#include <vector>
#include <string>
#include <numeric>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

using namespace std;
using namespace cv;

enum ColorSegMode{
    NONE,
    ERODE,
    DILATE,
    ERODE_DILATE,
    DILATE_ERODE
};

float l2_norm(vector<float> const& u) {
    float accum = 0.;
    for (auto x : u) {
        accum += x * x;
    }
    return sqrt(accum);
}

void meanColorSegmentation(cv::Mat &src, cv::Mat_<Vec3f> &dst, cv::Rect &roi, float etol, ColorSegMode mode = NONE, int kernelSize = 9){
    cv::Mat roi_src = src(roi);

    cv::Scalar mean_s, stddev_s;
    cv::meanStdDev(roi_src,mean_s,stddev_s);

    /*cv::Mat_<float> mean(3,1);
    for(int i = 0; i < 3; ++i) {
        mean(i,0) = mean_s[i];
    }*/

    vector<float> mean_test(3);
    for(int i = 0; i < 3; i++){
        mean_test[i] = mean_s[i];
    }
    vector<float> dist(3);

    float euclideanDistTol = etol*255;

    dst = src.clone();


    for(int y = 0; y < src.rows; y++){
        Vec3f* data_ptr = dst.ptr<Vec3f>(y);
        for(int x = 0; x < src.cols; x++){
            Vec3f &bgr = data_ptr[x];

            vector<float> bgrm {bgr[0],bgr[1],bgr[2]};

            for(int i = 0; i<3; i++){
                dist[i] = bgrm[i]-mean_test[i];
            }

            if(l2_norm(dist) <= euclideanDistTol)
                bgr = Vec3f(1,1,1);
            else
                bgr = Vec3f(0,0,0);
        }
    }

    Mat kernel = Mat::ones(kernelSize,kernelSize,CV_8U);

    switch (mode) {
    case NONE:
        break;
    case ERODE:
        erode(dst,dst,kernel);
        break;
    case DILATE:
        dilate(dst,dst,kernel);
        break;
    case ERODE_DILATE:
        erode(dst,dst,kernel);
        dilate(dst,dst,kernel);
        break;
    case DILATE_ERODE:
        dilate(dst,dst,kernel);
        erode(dst,dst,kernel);
        break;
    default:
        break;
    }
}


cv::Mat createBorder(cv::Mat src, int padding, int border_intensity = 0){
    cv::Mat result;
    cv::copyMakeBorder(src,result,padding,padding,padding,padding, cv::BORDER_CONSTANT, border_intensity);
    return result;
}

void findCircles(vector<vector<Point>> contours, vector<vector<Point>> &circles, float ptol = 0.7, float atol = 2000){
    int i = 0;
    // Loop through each contour in list of contours
    for(auto component : contours){
        // Compute area, perimeter and how close the component resembles a circle
        float area = contourArea(component);
        float perimeter = arcLength(component,true);
        float percentage_circle = (4*M_PI*area)/(perimeter*perimeter);
        // Check if component is a circle
        if(percentage_circle > ptol && area > atol){
            circles.resize(i+1);
            circles[i] = component;
            i++;
        }
    }
}

void getCenterOfMass(cv::Mat src,vector<vector<Point>> &contours,vector<Point> &COM){
    Point min,max;

    // Init
    min.x = src.cols;
    min.y = src.rows;
    max.x = 0;
    max.y = 0;

    // Loop through pixels in contours in list of contours
    for(auto contour : contours){
        for(auto p : contour){
            if(p.x < min.x) min.x = p.x;
            if(p.x > max.x) max.x = p.x;
            if(p.y < min.y) min.y = p.y;
            if(p.y > max.y) max.y = p.y;

        }
        //Compute Center of Mass and push to vector
        COM.push_back(Point(((max.x-min.x)/2)+min.x,((max.y-min.y)/2)+min.y));
        //Reset
        min.x = src.cols;
        min.y = src.rows;
        max.x = 0;
        max.y = 0;
    }
}

RNG rng(12345);

void drawConnectedComponents(cv::Mat src, vector<vector<Point>> contours){
    Mat drawing = Mat::zeros(src.size(), CV_8UC3);
    for(int i = 0; i < contours.size(); i++){
        Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        drawContours(drawing,contours,i,color,1,8);
    }
    namedWindow( "Connected Components", CV_WINDOW_NORMAL );
    imshow( "Connected Components", drawing );
    waitKey(0);
}

int main(){
    // Load image
    //cv::Mat src = imread("../../images/MarkerColor/marker_color_01.png", CV_LOAD_IMAGE_COLOR);
    cv::Mat src = imread("../../images/MarkerColorHard/marker_color_hard_49.png", CV_LOAD_IMAGE_COLOR);
    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }
    cout << src.type() << endl;
    namedWindow( "src", CV_WINDOW_NORMAL );
    imshow("src",src);
    waitKey(0);

    for(int i = 0; i < 200; i++){

    static auto endTimePoint = std::chrono::system_clock::now();
    static auto startTimePoint = std::chrono::system_clock::now();
    static auto diffTimePoint = endTimePoint - startTimePoint;
    double timeDifference;

    startTimePoint = std::chrono::system_clock::now();

/*
    // Red
    //cv::Rect roi(658,158,50,50);
    cv::Rect roi(420,290,20,20); // hard_49.png
    cv::Mat_<Vec3f> r_res;
    meanColorSegmentation(src,r_res,roi,0.1,DILATE_ERODE,9);

    // Convert to 1 channel and Convert data type from float to 8bit unsigned
    cv::Mat r_res_gray;
    cv::cvtColor(r_res,r_res_gray,COLOR_BGR2GRAY);
    r_res_gray.convertTo(r_res_gray,CV_8UC1);


    // FindContours
    vector<vector<Point>> r_contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(r_res_gray,r_contours,hierarchy,CV_RETR_LIST,CHAIN_APPROX_NONE);


    //drawConnectedComponents(r_res_gray, r_contours);


    //FindCircles
    vector<vector<Point>> r_circles;
    findCircles(r_contours,r_circles);

    //drawConnectedComponents(r_res_gray,r_circles);

    //ComputeCOM
    vector<Point> COM;
    getCenterOfMass(r_res_gray,r_circles,COM);

    //for(auto centroid : COM)
    //    cout << "(" << centroid.x << "," << centroid.y << ")" << endl;
*/

    // Blue
    //cv::Rect b_roi(658,342,50,50);
    cv::Rect b_roi(400,170,20,20); // hard_49.png
    cv::Mat_<Vec3f> b_res;
    meanColorSegmentation(src,b_res,b_roi,0.1,DILATE_ERODE,9);

    // Convert to 1 channel and Convert data type from float to 8bit unsigned
    cv::Mat b_res_gray;
    cv::cvtColor(b_res,b_res_gray,COLOR_BGR2GRAY);
    b_res_gray.convertTo(b_res_gray,CV_8UC1);

    // FindContours
    vector<vector<Point>> b_contours;
    std::vector<cv::Vec4i> b_hierarchy;
    cv::findContours(b_res_gray,b_contours,b_hierarchy,CV_RETR_LIST,CHAIN_APPROX_NONE);

    //drawConnectedComponents(b_res_gray, b_contours);

    //FindCircles
    vector<vector<Point>> b_circles;
    findCircles(b_contours,b_circles);

    //drawConnectedComponents(b_res_gray,b_circles);

    //ComputeCOM
    vector<Point> b_COM;
    getCenterOfMass(b_res_gray,b_circles,b_COM);

    //for(auto centroid : b_COM)
    //    cout << "(" << centroid.x << "," << centroid.y << ")" << endl;


    endTimePoint = std::chrono::system_clock::now();
    diffTimePoint = endTimePoint - startTimePoint;
    timeDifference = std::chrono::duration<double,std::milli>(diffTimePoint).count();
    cout << timeDifference << endl;
}
    waitKey(0);
    destroyAllWindows();
    return 0;
}

