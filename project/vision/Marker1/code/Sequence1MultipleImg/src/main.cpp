#include <iostream>
#include <vector>
#include <string>
#include <numeric>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

using namespace std;
using namespace cv;

vector<float> mean_g;
vector<Point> displacements;
int d_count = 1;
Point prev_pos;

int img_num = 1;
int path_ctrl = 0;
stringstream ss;
string file_path_easy = "../../images/MarkerColor/marker_color_";
string file_path_hard = "../../images/MarkerColorHard/marker_color_hard_";
string file_path_rws = "../../images/Marker1Background2/Marker1Background2_";
string path;



enum ColorSegMode{
    NONE,
    ERODE,
    DILATE,
    ERODE_DILATE,
    DILATE_ERODE
};

float l2_norm(vector<float> const& u) {
    float accum = 0.;
    for (auto x : u) {
        accum += x * x;
    }
    return sqrt(accum);
}

void computeMean(cv::Mat src,cv::Rect roi, vector<float> &training_mean){
    cv::Mat roi_src = src(roi);
    //imshow("src_roi",roi_src);
    //waitKey(0);

    cv::Scalar mean_s, stddev_s;
    cv::meanStdDev(roi_src,mean_s,stddev_s);
    cout << mean_s << endl;

    training_mean.resize(3);
    for(int i = 0; i < 3; i++){
        training_mean[i] = mean_s[i];
    }
}

void meanColorSegmentation(cv::Mat src, cv::Mat_<Vec3f> &dst, vector<float> training_mean, float etol, ColorSegMode mode = NONE, int kernelSize = 9){
    vector<float> dist(3);

    float euclideanDistTol = etol*255;

    dst = src.clone();


    for(int y = 0; y < src.rows; y++){
        Vec3f* data_ptr = dst.ptr<Vec3f>(y);
        for(int x = 0; x < src.cols; x++){
            Vec3f &bgr = data_ptr[x];

            vector<float> bgrm {bgr[0],bgr[1],bgr[2]};

            for(int i = 0; i<3; i++){
                dist[i] = bgrm[i]-training_mean[i];
            }

            if(l2_norm(dist) <= euclideanDistTol)
                bgr = Vec3f(1,1,1);
            else
                bgr = Vec3f(0,0,0);
        }
    }

    Mat kernel = Mat::ones(kernelSize,kernelSize,CV_8U);

    switch (mode) {
    case NONE:
        break;
    case ERODE:
        erode(dst,dst,kernel);
        break;
    case DILATE:
        dilate(dst,dst,kernel);
        break;
    case ERODE_DILATE:
        erode(dst,dst,kernel);
        dilate(dst,dst,kernel);
        break;
    case DILATE_ERODE:
        dilate(dst,dst,kernel);
        erode(dst,dst,kernel);
        break;
    default:
        break;
    }
}

void findCircles(vector<vector<Point>> contours, vector<vector<Point>> &circles, float ptol = 0.7, float atol = 2000){
    int i = 0;
    // Loop through each contour in list of contours
    for(auto component : contours){
        // Compute area, perimeter and how close the component resembles a circle
        float area = contourArea(component);
        float perimeter = arcLength(component,true);
        float percentage_circle = (4*M_PI*area)/(perimeter*perimeter);
        // Check if component is a circle
        if(percentage_circle > ptol && area > atol){
            circles.resize(i+1);
            circles[i] = component;
            i++;
        }
    }
}

void getCenterOfMass(cv::Mat src,vector<vector<Point>> contours,vector<Point> &COM){
    Point min,max;

    // Init
    min.x = src.cols;
    min.y = src.rows;
    max.x = 0;
    max.y = 0;

    // Loop through pixels in contours in list of contours
    for(auto contour : contours){
        for(auto p : contour){
            if(p.x < min.x) min.x = p.x;
            if(p.x > max.x) max.x = p.x;
            if(p.y < min.y) min.y = p.y;
            if(p.y > max.y) max.y = p.y;

        }
        //Compute Center of Mass and push to vector
        COM.push_back(Point(((max.x-min.x)/2)+min.x,((max.y-min.y)/2)+min.y));
        //Reset
        min.x = src.cols;
        min.y = src.rows;
        max.x = 0;
        max.y = 0;
    }
}

RNG rng(12345);

void drawConnectedComponents(cv::Mat src, vector<vector<Point>> contours){
    Mat drawing = Mat::zeros(src.size(), CV_8UC3);
    for(int i = 0; i < contours.size(); i++){
        Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        drawContours(drawing,contours,i,color,1,8);
    }
    namedWindow( "Connected Components", CV_WINDOW_NORMAL );
    imshow( "Connected Components", drawing );
    waitKey(0);
}


void on_trackbar(int, void*){
    switch (path_ctrl) {
    case 0:
        path = file_path_easy;
        break;
    case 1:
        path = file_path_hard;
        break;
    case 2:
        path = file_path_rws;
        break;
    default:
        break;
    }

    if(img_num < 1) img_num = 1;
    if(path == file_path_easy && img_num > 30) img_num = 30;
    if(path == file_path_rws && img_num > 48) img_num = 48;

    ss << setw(2) << setfill('0') << img_num;
    string number = ss.str();
    string t_path = path + number + ".png";
    ss.str(string());

    Mat src = imread(t_path, CV_LOAD_IMAGE_COLOR);
    if (src.empty()){
        cout << t_path + ": No such file or directory" << endl;
        exit(-1);
    }

    /// IMAGE PROCESSING

    // Blue
    cv::Mat_<Vec3f> b_res;
    meanColorSegmentation(src,b_res,mean_g,0.15,DILATE_ERODE,9);

    //imshow("Marker1_mean_color_segmented",b_res);
    //waitKey(0);

    // Convert to 1 channel and Convert data type from float to 8bit unsigned
    cv::Mat b_res_gray;
    cv::cvtColor(b_res,b_res_gray,COLOR_BGR2GRAY);
    b_res_gray.convertTo(b_res_gray,CV_8UC1);

    // FindContours
    vector<vector<Point>> b_contours;
    std::vector<cv::Vec4i> b_hierarchy;
    cv::findContours(b_res_gray,b_contours,b_hierarchy,CV_RETR_LIST,CHAIN_APPROX_NONE);
    //cout << b_contours.size() << endl;
    //drawConnectedComponents(b_res_gray, b_contours);

    //FindCircles
    vector<vector<Point>> b_circles;
    findCircles(b_contours,b_circles,0.81);


    Mat drawing = Mat::zeros(b_res_gray.size(), CV_8UC3);
    for(int i = 0; i < b_circles.size(); i++){
        Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        drawContours(drawing,b_circles,i,color,1,8);
    }

    //drawConnectedComponents(b_res_gray,b_circles);

    //ComputeCOM
    vector<Point> b_COM;
    getCenterOfMass(b_res_gray,b_circles,b_COM);


    //cout << endl;
    //for(auto centroid : b_COM)
    //    cout << "(" << centroid.x << "," << centroid.y << ")" << endl;

    // Compute largest euclidean dist
    int maxdist = 0;
    Point p1,p2;
    for(int i = 0; i<b_COM.size();i++){
        for(int j = 0; j<b_COM.size(); j++){
            int dist = sqrt(pow(b_COM[j].x - b_COM[i].x,2)+pow(b_COM[j].y - b_COM[i].y,2));
            if(dist>maxdist){
                maxdist = dist;
                p1 = b_COM[j];
                p2 = b_COM[i];
            }
        }
    }

    // Compute centroid of marker
    Point COM_marker = (p1+p2)*0.5;

    // Draw line
    if(b_COM.size() == 3){
        line(drawing,p1,p2,Scalar(0,255,0));

        // Coordinate System
        //line(drawing,Point(0,src.rows/2),Point(src.cols,src.rows/2),Scalar(0,255,0));
        //line(drawing,Point(src.cols/2,0),Point(src.cols/2,src.rows),Scalar(0,255,0));

        circle(drawing,COM_marker,5,Scalar(0,0,255),1);
        //cout << "(" << COM_marker.x << "," << COM_marker.y << ")" << endl;
        Point COM_corr(COM_marker.x-(src.cols/2),-1*(COM_marker.y-src.rows/2));
        //cout << "(" << COM_corr.x << "," << COM_corr.y << ")" << endl;


        displacements.resize(d_count);
        displacements[d_count] = Point(COM_corr.x-prev_pos.x,COM_corr.y-prev_pos.y);
        cout << "(" << displacements[d_count].x << "," << displacements[d_count].y << ")" << endl;
        prev_pos = COM_corr;
        d_count++;

    }

    // Show two images in one window:
    Size sz1 = src.size();
    Size sz2 = drawing.size();
    Mat test(sz1.height,sz1.width+sz2.width,CV_8UC3);
    Mat left(test, Rect(0,0,sz1.width,sz1.height));
    src.copyTo(left);
    Mat right(test, Rect(sz1.width,0,sz2.width,sz2.height));
    drawing.copyTo(right);

    imshow("Control",test);

    //imshow("Control",drawing);
}


int main(){
    // Load training image
    cv::Mat src = imread("../../images/MarkerColor/marker_color_01.png", CV_LOAD_IMAGE_COLOR);
    //cv::Mat src = imread("../../images/Marker1Background2/Marker1Background2_01.png", CV_LOAD_IMAGE_COLOR);
    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }

    imshow("src",src);
    waitKey(0);

    // Calculate mean
    cv::Rect b_roi(658,342,50,50);
    //cv::Rect b_roi(500,525,50,50);

    // Draw ROI
    /*Mat roi_draw = src.clone();
    rectangle(roi_draw,b_roi,Scalar(0,255,0));
    imshow("draw_roi",roi_draw);
    waitKey(0);*/

    computeMean(src,b_roi, mean_g);

    prev_pos = Point(0,0);

    namedWindow("Control",CV_WINDOW_NORMAL);
    createTrackbar("Path", "Control", &path_ctrl, 2, on_trackbar);
    createTrackbar("Image", "Control", &img_num, 52, on_trackbar);

    waitKey(0);
    destroyAllWindows();
    return 0;
}

