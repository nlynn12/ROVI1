Guide for compiling and running the RobWork Studio Plugin
=========================================================
**Note** RobWork needs to be installed. If you do not have it installed please refer to [this guide](https://gitlab.com/nlynn12/ROVI1/blob/master/Robotics/README.md#installing-the-robwork-framework-on-ubuntu-1504)


- Change directory to the build folder
```
cd your_path/SamplePluginPA10/build/
```

- Run cmake from inside the build folder and make the plugin
```
cmake ../src/
make
```

- The library file will be placed in SamplePluginPA10/libs/release/
- Open RobWorkStudio and load the plugin
- The initialize function in the plugin does **not** load any workcell before the path for the project folder is specified. This is done in the Settings tab of the UI part of the plugin where a full path to the project folder is needed. For example:
```
/home/nlynn12/Documents/FinalProject/
```
- When pressing enter in the text field the plugin loads:
    - The [PA10 workcell](https://gitlab.com/nlynn12/ROVI1/tree/master/FinalProject/PA10WorkCell),
    - the first marker,
    - the first background,
    - the fast marker speed and
    - the time step

Guide for using the plugin
==========================