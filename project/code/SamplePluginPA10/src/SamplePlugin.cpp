#include "SamplePlugin.hpp"
#include "VisionMarker1.hpp"

#include <rws/RobWorkStudio.hpp>
#include <rw/kinematics.hpp>
#include <rw/loaders/ImageLoader.hpp>
#include <rw/loaders/WorkCellFactory.hpp>

#include <QPushButton>


using namespace std;
using namespace rw::math;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::kinematics;
using namespace rw::common;
using namespace rw::graphics;
using namespace rw::sensor;
using namespace rwlibs::opengl;
using namespace rwlibs::simulation;
using namespace rws;
using namespace cv;

SamplePlugin::SamplePlugin() : RobWorkStudioPlugin("SamplePluginUI", QIcon(":/pa_icon.png")){
    setupUi(this);
    
    _timer = new QTimer(this);
    connect(_timer, SIGNAL(timeout()), this, SLOT(timer()));
    
    // now connect stuff from the ui component
    connect(_btn1,              SIGNAL(pressed()), this,        SLOT(btnPressed())                );
    connect(_btn2,              SIGNAL(pressed()), this,        SLOT(btnPressed())                );
    //connect(_slider_marker_pos, SIGNAL(valueChanged(int)),this, SLOT(rws_update_marker_position()));
    
    // Settings tab
    connect(_pro_path,          SIGNAL(returnPressed()),this,   SLOT(rws_update_project_path())   );
    connect(_load_scene,        SIGNAL(pressed()),this,         SLOT(rws_load_workcell())         );
    connect(_marker,            SIGNAL(activated(int)),this,    SLOT(rws_load_marker())           );
    connect(_marker_movement,   SIGNAL(activated(int)),this,    SLOT(rws_load_marker_movement())  );
    connect(_background,        SIGNAL(activated(int)),this,    SLOT(rws_load_background())       );
    connect(_tracking_points,   SIGNAL(activated(int)),this,    SLOT(rws_update_tracking_points()));
    connect(_data_checkbox,     SIGNAL(stateChanged(int)),this, SLOT(rws_data_checkbox())         );
    connect(_data_extractor,    SIGNAL(pressed()),this,         SLOT(sim_data_extration())        );
    
    
    Image textureImage(300,300,Image::GRAY,Image::Depth8U);
    _textureRender = new RenderImage(textureImage);
    Image bgImage(0,0,Image::GRAY,Image::Depth8U);
    _bgRender = new RenderImage(bgImage,2.5/1000.0);
    _framegrabber = NULL;
}

SamplePlugin::~SamplePlugin(){
    delete _textureRender;
    delete _bgRender;
}

void SamplePlugin::initialize(){
    getRobWorkStudio()->stateChangedEvent().add(boost::bind(&SamplePlugin::stateChangedListener, this, _1), this);
}

void SamplePlugin::open(WorkCell* workcell){
    _wc = workcell;
    _state = _wc->getDefaultState();
    
    //log().info() << workcell->getFilename() << "\n";
    
    if (_wc != NULL) {
        markerposcounter = 0;
        
        // Add the texture render to this workcell if there is a frame for texture
        Frame* textureFrame = _wc->findFrame("MarkerTexture");
        if (textureFrame != NULL) {
            getRobWorkStudio()->getWorkCellScene()->addRender("TextureImage",_textureRender,textureFrame);
        }
        
        // Add the background render to this workcell if there is a frame for texture
        Frame* bgFrame = _wc->findFrame("Background");
        if (bgFrame != NULL) {
            getRobWorkStudio()->getWorkCellScene()->addRender("BackgroundImage",_bgRender,bgFrame);
        }
        
        // Create a GLFrameGrabber if there is a camera frame with a Camera property set
        Frame* cameraFrame = _wc->findFrame("CameraSim");
        if (cameraFrame != NULL) {
            if (cameraFrame->getPropertyMap().has("Camera")) {
                // Read the dimensions and field of view
                double fovy;
                int width,height;
                std::string camParam = cameraFrame->getPropertyMap().get<std::string>("Camera");
                std::istringstream iss (camParam, std::istringstream::in);
                iss >> fovy >> width >> height;
                // Create a frame grabber
                _framegrabber = new GLFrameGrabber(width,height,fovy);
                SceneViewer::Ptr gldrawer = getRobWorkStudio()->getView()->getSceneViewer();
                _framegrabber->init(gldrawer);
            }
        }
        
    }
}

void SamplePlugin::close() {
    // Stop the timer
    _timer->stop();
    // Remove the texture render
    Frame* textureFrame = _wc->findFrame("MarkerTexture");
    if (textureFrame != NULL) {
        getRobWorkStudio()->getWorkCellScene()->removeDrawable("TextureImage",textureFrame);
    }
    // Remove the background render
    Frame* bgFrame = _wc->findFrame("Background");
    if (bgFrame != NULL) {
        getRobWorkStudio()->getWorkCellScene()->removeDrawable("BackgroundImage",bgFrame);
    }
    // Delete the old framegrabber
    if (_framegrabber != NULL) {
        delete _framegrabber;
    }
    _framegrabber = NULL;
    _wc = NULL;
}

Mat SamplePlugin::toOpenCVImage(const Image& img) {
    Mat res(img.getHeight(),img.getWidth(), CV_8UC3);
    res.data = (uchar*)img.getImageData();
    return res;
}

Mat SamplePlugin::capture_image(QLabel* _img){
    Mat imflip;
    if (_framegrabber != NULL) {
        // Get the image as a RW image
        Frame* cameraFrame = _wc->findFrame("CameraSim");
        _framegrabber->grab(cameraFrame, _state);
        const Image& image = _framegrabber->getImage();
        
        // Convert to OpenCV image
        Mat im = toOpenCVImage(image);
        cv::flip(im, imflip, 0);
        
        // Show in QLabel
        if(_img != nullptr){
            QImage img(imflip.data, imflip.cols, imflip.rows, imflip.step, QImage::Format_RGB888);
            QPixmap p = QPixmap::fromImage(img);
            _img->setPixmap(p.scaled(250,300,Qt::KeepAspectRatio));
        }
    }
    return imflip;
}

void SamplePlugin::btnPressed() {
    QObject *obj = sender();
    if(obj==_btn1){
        // Toggle the timer on and off
        if (!_timer->isActive()){
            rw::common::Log::log().error() << "Start\n";
            double deltaT = _deltat->value();
            _timer->start(1000*deltaT);
            markerposcounter = 0;
            _state = _wc->getDefaultState();
            
            if(performDataExtraction){
                std::string marker_speed = _marker_movement->currentText().toStdString();
                std::string n_track_p = _tracking_points->currentText().toStdString();
                feature_te_file.open(project_path + "Data/vision/vision_te_" + marker_speed + "_" + n_track_p + ".txt");
                feature_te_file << "Time,TrackingError\n";
                feature_jpn_file.open(project_path + "Data/vision/vision_jpn_" + marker_speed + "_" + n_track_p + ".txt");
                feature_jpn_file << "Time,JointPositionNormalized\n";
                feature_jvn_file.open(project_path + "Data/vision/vision_jvn_" + marker_speed + "_" + n_track_p + ".txt");
                feature_jvn_file << "Time,JointVelocitiesNormalized\n";
            }
        }else{
            rw::common::Log::log().error() << "Stop\n";
            if(feature_te_file.is_open()) feature_te_file.close();
            if(feature_jpn_file.is_open()) feature_jpn_file.close();
            if(feature_jvn_file.is_open()) feature_jvn_file.close();
            _timer->stop();
        }
    }else if(obj== _btn2){
        rw::common::Log::log().error() << "Reset\n";
        resetPressed = true;
        if(feature_te_file.is_open()) feature_te_file.close();
        if(feature_jpn_file.is_open()) feature_jpn_file.close();
        if(feature_jvn_file.is_open()) feature_jvn_file.close();
        if(_timer->isActive()) _timer->stop();
        markerposcounter = 0;
        UpdateMarkerPos(markerPath[markerposcounter]);
        
        // Reset workcell
        _state = _wc->getDefaultState();
        Device::Ptr device = _wc->findDevice("PA10");
        Q q = device->getQ(_state);
        device->setQ(q,_state);
        getRobWorkStudio()->setState(_state);
        
        // Reset UI Plugin images
        capture_image(_label);
        capture_image(_image_res);
    }
}


void SamplePlugin::rws_update_project_path(){
    project_path = _pro_path->text().toStdString();
    log().info() << "Updated project path: " << project_path << "\n";
    
    // Initialize workcell
    rws_load_workcell();
    rws_update_tracking_points();
    rws_load_marker();
    rws_load_marker_movement();
    rws_load_background();
    rws_data_checkbox();
    
    // Capture initial images
    capture_image(_label);
    capture_image(_image_res);
}

void SamplePlugin::rws_load_workcell(){
    WorkCell::Ptr wc = WorkCellLoader::Factory::load(project_path + "PA10WorkCell/ScenePA10RoVi1.wc.xml");
    getRobWorkStudio()->setWorkCell(wc);
}

void SamplePlugin::rws_load_marker(){
    std::string marker_name = _marker->currentText().toStdString();
    
    Image::Ptr marker_img;
    if      (marker_name == "Marker1")      marker_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/markers/Marker1.ppm");
    else if (marker_name == "Marker2a")     marker_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/markers/Marker2a.ppm");
    else if (marker_name == "Marker2b")     marker_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/markers/Marker2b.ppm");
    else if (marker_name == "Marker3")      marker_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/markers/Marker3.ppm");
    else                                    marker_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/markers/Marker1.ppm");
    
    _textureRender->setImage(*marker_img);
    getRobWorkStudio()->updateAndRepaint();
    log().info() << "Marker changed to " << marker_name << "\n";
}

void SamplePlugin::rws_load_marker_movement(){
    std::string marker_movement = _marker_movement->currentText().toStdString();
    std::string marker_path_location;
    
    if      (marker_movement == "Fast")   marker_path_location = project_path + "SamplePluginPA10/motions/MarkerMotionFast.txt";
    else if (marker_movement == "Medium") marker_path_location = project_path + "SamplePluginPA10/motions/MarkerMotionMedium.txt";
    else if (marker_movement == "Slow")   marker_path_location = project_path + "SamplePluginPA10/motions/MarkerMotionSlow.txt";
    else                                  marker_path_location = project_path + "SamplePluginPA10/motions/MarkerMotionFast.txt";
    
    
    ifstream file(marker_path_location.c_str());
    std::string line;
    double x,y,z,R,P,Y;
    
    if( file.is_open() ){
        markerPath.clear();
        while (getline(file, line))
        {
            if (!line.empty())
            {
                stringstream ss;
                ss << line;
                ss >> x >> y >> z >> R >> P >> Y ;
            }
            Vector3D<double> Ptmp(x,y,z);
            RPY<double> RPYtmp(R,P,Y);
            Transform3D<double> Ttmp(Ptmp,RPYtmp.toRotation3D());
            markerPath.push_back(Ttmp);
        }
        file.close();
    }
    log().info() << "Marker Movement changed to " << marker_movement << " with " << markerPath.size() << " steps\n";
}


void SamplePlugin::rws_load_background(){
    std::string bg_name = _background->currentText().toStdString();
    
    Image::Ptr bg_img;
    
    bool set_image = true;
    
    if     (bg_name == "color1")   bg_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/backgrounds/color1.ppm");
    else if(bg_name == "color2")   bg_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/backgrounds/color2.ppm");
    else if(bg_name == "color3")   bg_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/backgrounds/color3.ppm");
    else if(bg_name == "lines1")   bg_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/backgrounds/lines1.ppm");
    else if(bg_name == "texture1") bg_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/backgrounds/texture1.ppm");
    else if(bg_name == "texture2") bg_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/backgrounds/texture2.ppm");
    else if(bg_name == "texture3") bg_img = ImageLoader::Factory::load(project_path + "SamplePluginPA10/backgrounds/texture3.ppm");
    else{
        if(_bgRender != nullptr){
            delete _bgRender;
            _bgRender = nullptr;
        }
        Image bg(0,0,Image::GRAY,Image::Depth8U);
        _bgRender = new RenderImage(bg,2.5/1000);
        set_image = false;
    }
    
    if(set_image) _bgRender->setImage(*bg_img);
    
    getRobWorkStudio()->updateAndRepaint();
    log().info() << "Background changed to " << bg_name << "\n";
}

void SamplePlugin::rws_update_tracking_points(){
    nTrackingPoints = _tracking_points->currentIndex() + 1;
    //rw::common::Log::log().info() << "tp: " << nTrackingPoints << "\n";
}

void SamplePlugin::rws_data_checkbox(){
    if(_data_checkbox->isChecked()) performDataExtraction = true;
    else performDataExtraction = false;
}

void SamplePlugin::timer() {
    if(markerposcounter == markerPath.size()-1){
        _timer->stop();
        if(feature_te_file.is_open()) feature_te_file.close();
        if(feature_jpn_file.is_open()) feature_jpn_file.close();
        if(feature_jvn_file.is_open()) feature_jvn_file.close();
        log().info() << "Marker Path Done!\n";
        //RW_THROW("Marker Path Done");
        return;
    }
    
    UpdateMarkerPos(markerPath[markerposcounter]);
    Mat imflip = capture_image(_label);
    
    if(_marker->currentText().toStdString() == "Simulation"){
        visual_servoing();
    }else if(_marker->currentText().toStdString() == "Marker1"){
        feature_extraction(imflip);
    }
    
    markerposcounter++;
    
    capture_image(_image_res);
    //rw::common::Log::log().error() << "Marker Pos: " << markerposcounter << "\n";
}

void SamplePlugin::feature_extraction(Mat imflip){
    /// FEATURE EXTRACTION
    
    static auto endTimePoint = std::chrono::system_clock::now();
    static auto startTimePoint = std::chrono::system_clock::now();
    static auto diffTimePoint = endTimePoint - startTimePoint;
    double tau;
    
    startTimePoint = std::chrono::system_clock::now();
    
    cv::cvtColor(imflip,imflip,CV_BGR2RGB);
    if(markerposcounter == 0){
        Mat test = imread(project_path + "SamplePluginPA10/markers/Marker1.ppm");
        featureextraction::initialize_marker1(test);
    }
    
    vector<Point2d> featurePoints = featureextraction::featureextraction_marker1(imflip,nTrackingPoints);
    
    endTimePoint = std::chrono::system_clock::now();
    diffTimePoint = endTimePoint - startTimePoint;
    tau = std::chrono::duration<double,std::milli>(diffTimePoint).count();
    //rw::common::Log::log().info() << "tau:\t" << tau << "\n";
    
    boost::numeric::ublas::matrix<double> uv(6,1,0);
    for(unsigned int i = 0; i < featurePoints.size()*2; i+=2){
        // u,v
        uv(i,0) = featurePoints[i/2].x;
        uv(i+1,0) = featurePoints[i/2].y;
    }
    vector<double> featureOffsets(6);
    switch(nTrackingPoints){
    case 1:
        featureOffsets = {0,0,0,0,0,0};
        break;
    case 2:
        featureOffsets = {0,0,0,0,88,88};
        break;
    case 3:
        featureOffsets = {0,0,-88,88,88,-88};
        break;
    default:
        break;
    }
    //vector<double> featureOffsets {88,88,-88,88,88,-88};
    boost::numeric::ublas::matrix<double> dudv = sim_compute_du_dv(uv,featureOffsets);
    //rw::common::Log::log().info() << dudv << "\n";
    Q deltaQ = sim_compute_deltaQ(uv,dudv);
    
    // Check Velocity limits
    deltaQ = velocity_scale(deltaQ,_deltat->value(),tau);
    
    // Set the new Q vector in the state and update RobWorks Studio
    Device::Ptr device = _wc->findDevice("PA10");
    Q q = device->getQ(_state);
    device->setQ(q+deltaQ,_state);
    getRobWorkStudio()->setState(_state);
    
    // Extract data if checkbox is checked
    if(performDataExtraction){
        // Normalized joint VELOCITIES
        Q Qvelocity = deltaQ / _deltat->value();
        Q VelocityLimits = device->getVelocityLimits();
        Q Qvn(7,0,0,0,0,0,0,0);
        for(int i = 0; i < 7; i++){
            Qvn[i] = (Qvelocity[i] + VelocityLimits[i])/(VelocityLimits[i] * 2);
        }
        
        feature_jvn_file << markerposcounter * _deltat->value() << "," <<
                            Qvn[0] << "," <<
                            Qvn[1] << "," <<
                            Qvn[2] << "," <<
                            Qvn[3] << "," <<
                            Qvn[4] << "," <<
                            Qvn[5] << "," <<
                            Qvn[6] << "\n";
        
        
        // Normalized joint POSITIONS
        feature_jpn_file << markerposcounter * _deltat->value() << "," <<
                            (q[0] + 3.142) / (3.142*2) << "," <<
                            (q[1] + 1.641) / (1.641*2) << "," <<
                            (q[2] + 3.142) / (3.142*2) << "," <<
                            (q[3] + 2.496) / (2.496*2) << "," <<
                            (q[4] + 4.712) / (4.712*2) << "," <<
                            (q[5] + 2.094) / (2.094*2) << "," <<
                            (q[6] + 6.283) / (6.283*2) << "\n";
        
        
        // Tracking Error
        Mat data_e_img = capture_image(nullptr);
        cv::cvtColor(data_e_img,data_e_img,CV_BGR2RGB);
        featurePoints = featureextraction::featureextraction_marker1(data_e_img,nTrackingPoints);
        for(unsigned int i = 0; i < featurePoints.size()*2; i+=2){
            // u,v
            uv(i,0) = featurePoints[i/2].x;
            uv(i+1,0) = featurePoints[i/2].y;
        }
        dudv = sim_compute_du_dv(uv,featureOffsets);
        //rw::common::Log::log().info() << dudv << "\n";
        
        double edist = sqrt(pow((double)dudv(0,0),2)+pow((double)dudv(1,0),2));
        feature_te_file << markerposcounter * _deltat->value() << "," << edist << "\n";
    }
}

void SamplePlugin::visual_servoing(){
    //rw::common::Log::log().info() << _tracking_points->currentIndex() << "\n";
    vector<Transform3D<double>> trackPoints (3);
    vector<double> trackingPointsOffset(6);
    
    switch(nTrackingPoints){
    case 1:
        trackPoints[0] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
        trackPoints[1] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
        trackPoints[2] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
        trackingPointsOffset = {0,0,0,0,0,0};
        break;
    case 2:
        trackPoints[0] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
        trackPoints[1] = Transform3D<double>(Vector3D<double>(-0.1,0,0), Rotation3D<double>::identity());
        trackPoints[2] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
        trackingPointsOffset = {0,0,164.6,0,0,0};
        break;
    case 3:
        trackPoints[0] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
        trackPoints[1] = Transform3D<double>(Vector3D<double>(-0.1,0,0), Rotation3D<double>::identity());
        trackPoints[2] = Transform3D<double>(Vector3D<double>(0,0.1,0), Rotation3D<double>::identity());
        trackingPointsOffset = {0,0,164.6,0,0,164.6};
        break;
    default:
        rw::common::Log::log().info() << "The selected number of tracking points are not supported!\n";
        return;
    }
    
    boost::numeric::ublas::matrix<double> uv = sim_compute_uv(trackPoints);
    boost::numeric::ublas::matrix<double> dudv = sim_compute_du_dv(uv,trackingPointsOffset);
    
    // Calculate deltaQ
    Q deltaQ = sim_compute_deltaQ(uv,dudv);
    
    // Check Velocity limits
    deltaQ = velocity_scale(deltaQ,_deltat->value());
    
    // Set the new Q vector in the state and update RobWorks Studio
    Device::Ptr device = _wc->findDevice("PA10");
    Q q = device->getQ(_state);
    device->setQ(q+deltaQ,_state);
    getRobWorkStudio()->setState(_state);
}

void SamplePlugin::UpdateMarkerPos(Transform3D<double> newtransform, bool UpdateRobworkStudio/*=true*/){
    rw::kinematics::MovableFrame *marker = (rw::kinematics::MovableFrame*)(_wc->findFrame("Marker"));
    marker->setTransform(newtransform, _state);
    if(UpdateRobworkStudio) getRobWorkStudio()->setState(_state);
}

Jacobian SamplePlugin::computeImageJacobian(double u, double v, double z, double f){
    rw::math::Jacobian image_jacobian(2,6);
    image_jacobian(0,0) = -f/z;
    image_jacobian(0,1) = 0;
    image_jacobian(0,2) = u/z;
    image_jacobian(0,3) = (u*v)/f;
    image_jacobian(0,4) = -(pow(f,2)+pow(u,2))/f;
    image_jacobian(0,5) = v;
    
    image_jacobian(1,0) = 0;
    image_jacobian(1,1) = -f/z;
    image_jacobian(1,2) = v/z;
    image_jacobian(1,3) = (pow(f,2)+pow(v,2))/f;
    image_jacobian(1,4) = -(u*v)/f;
    image_jacobian(1,5) = -u;
    
    return image_jacobian;
}

boost::numeric::ublas::matrix<double> SamplePlugin::sim_compute_uv(vector<Transform3D<double>> trackPoints){
    Frame* camsim = _wc->findFrame("CameraSim");
    Frame* marker = _wc->findFrame("Marker");
    
    boost::numeric::ublas::matrix<double> uv(trackPoints.size()*2,1,0);
    
    for(unsigned int i = 0; i < trackPoints.size()*2; i+=2){
        Transform3D<double> worldTcam = camsim->wTf(_state);
        Transform3D<double> worldTmarker = marker->wTf(_state) * trackPoints[i/2];
        worldTcam.invMult(worldTcam,worldTmarker);
        
        uv(i,0) = -worldTcam.P()[0] * 823/worldTcam.P()[2];
        uv(i+1,0) = worldTcam.P()[1] * 823/worldTcam.P()[2];
    }
    return uv;
}

boost::numeric::ublas::matrix<double> SamplePlugin::sim_compute_du_dv(boost::numeric::ublas::matrix<double> uv, vector<double> featureOffset){
    boost::numeric::ublas::matrix<double> error(6,1,0);
    for(unsigned int i = 0; i<featureOffset.size(); i++){
        error(i,0) = featureOffset[i] - uv(i,0);
    }
    return error;
}

Q SamplePlugin::sim_compute_deltaQ(boost::numeric::ublas::matrix<double> uv, boost::numeric::ublas::matrix<double> dudv){
    Device::Ptr device = _wc->findDevice("PA10");
    Frame* cam = _wc->findFrame("Camera");
    
    Transform3D<double> Tbasetool = device->baseTframe(cam,_state);
    
    double z = 0.5, f = 823;
    
    /// Solve J_image*S(q)*J(q)*dq = [du,dv]
    
    //Calculate the Image Jacobian
    Jacobian Jimage1 = computeImageJacobian(uv(0,0),uv(1,0),z,f);
    Jacobian Jimage2 = computeImageJacobian(uv(2,0),uv(3,0),z,f);
    Jacobian Jimage3 = computeImageJacobian(uv(4,0),uv(5,0),z,f);
    Jacobian Jimage(6,6);
    
    Jimage(0,0) =     Jimage1(0,0);
    Jimage(0,1) =     Jimage1(0,1);
    Jimage(0,2) =     Jimage1(0,2);
    Jimage(0,3) =     Jimage1(0,3);
    Jimage(0,4) =     Jimage1(0,4);
    Jimage(0,5) =     Jimage1(0,5);
    Jimage(1,0) =     Jimage1(1,0);
    Jimage(1,1) =     Jimage1(1,1);
    Jimage(1,2) =     Jimage1(1,2);
    Jimage(1,3) =     Jimage1(1,3);
    Jimage(1,4) =     Jimage1(1,4);
    Jimage(1,5) =     Jimage1(1,5);
    
    Jimage(2,0) =     Jimage2(0,0);
    Jimage(2,1) =     Jimage2(0,1);
    Jimage(2,2) =     Jimage2(0,2);
    Jimage(2,3) =     Jimage2(0,3);
    Jimage(2,4) =     Jimage2(0,4);
    Jimage(2,5) =     Jimage2(0,5);
    Jimage(3,0) =     Jimage2(1,0);
    Jimage(3,1) =     Jimage2(1,1);
    Jimage(3,2) =     Jimage2(1,2);
    Jimage(3,3) =     Jimage2(1,3);
    Jimage(3,4) =     Jimage2(1,4);
    Jimage(3,5) =     Jimage2(1,5);
    
    Jimage(4,0) =     Jimage3(0,0);
    Jimage(4,1) =     Jimage3(0,1);
    Jimage(4,2) =     Jimage3(0,2);
    Jimage(4,3) =     Jimage3(0,3);
    Jimage(4,4) =     Jimage3(0,4);
    Jimage(4,5) =     Jimage3(0,5);
    Jimage(5,0) =     Jimage3(1,0);
    Jimage(5,1) =     Jimage3(1,1);
    Jimage(5,2) =     Jimage3(1,2);
    Jimage(5,3) =     Jimage3(1,3);
    Jimage(5,4) =     Jimage3(1,4);
    Jimage(5,5) =     Jimage3(1,5);
    
    // The Matrix Sq from the book. Convert du to base frame
    boost::numeric::ublas::matrix<double> sq(6,6,0);
    boost::numeric::ublas::matrix<double> RbtqT = Tbasetool.R().inverse().m();
    sq(0,0) = RbtqT(0,0) ; sq(0,1) = RbtqT(0,1); sq(0,2) = RbtqT(0,2);
    sq(1,0) = RbtqT(1,0) ; sq(1,1) = RbtqT(1,1); sq(1,2) = RbtqT(1,2);
    sq(2,0) = RbtqT(2,0) ; sq(2,1) = RbtqT(2,1); sq(2,2) = RbtqT(2,2);
    
    sq(3,3) = RbtqT(0,0) ; sq(3,4) = RbtqT(0,1); sq(3,5) = RbtqT(0,2);
    sq(4,3) = RbtqT(1,0) ; sq(4,4) = RbtqT(1,1); sq(4,5) = RbtqT(1,2);
    sq(5,3) = RbtqT(2,0) ; sq(5,4) = RbtqT(2,1); sq(5,5) = RbtqT(2,2);
    
    //Calculate the Robot Jacobian
    Jacobian jacobian = device->baseJframe(cam,_state);
    
    //Multiply all the Matrices
    boost::numeric::ublas::matrix<double> Zimagetmp     = boost::numeric::ublas::prod(Jimage.m(),sq);
    boost::numeric::ublas::matrix<double> Zimage        = boost::numeric::ublas::prod(Zimagetmp,jacobian.m());
    boost::numeric::ublas::matrix<double> ZimageT       = boost::numeric::ublas::trans(Zimage);
    boost::numeric::ublas::matrix<double> ZimageZimageT = boost::numeric::ublas::prod(Zimage,ZimageT);
    boost::numeric::ublas::matrix<double> invZimage     = rw::math::LinearAlgebra::pseudoInverse(ZimageZimageT);
    
    //Last Step for Solving for deltaQ
    
    boost::numeric::ublas::matrix<double> y = boost::numeric::ublas::prod(invZimage,dudv);
    boost::numeric::ublas::matrix<double> deltaQ = boost::numeric::ublas::prod(ZimageT,y);
    
    // Convert to datatype "Q" in robworks. TODO: make this generic. Will only work with 7 joint robobts
    return Q(7,deltaQ(0,0),deltaQ(1,0),deltaQ(2,0),deltaQ(3,0),deltaQ(4,0),deltaQ(5,0),deltaQ(6,0));
}

Q SamplePlugin::velocity_scale(Q deltaQ, double deltaT, double tau/* = 0*/){
    Device::Ptr device = _wc->findDevice("PA10");
    Q VelLim = device->getVelocityLimits();
    
    int maxViolatorJoint;
    bool hasViolated = false;
    deltaT -= (tau/1000);
    if(deltaT<0) return Q(7,0,0,0,0,0,0,0);
    
    //rw::common::Log::log().info() << "delta t:\t" << deltaT << "\n";
    
    for(int i = 0; i < 7; i++){
        double maxViolaterValue = 0;
        if(abs(deltaQ[i]/deltaT) > VelLim[i] && abs(deltaQ[i]/deltaT) > maxViolaterValue ){
            maxViolatorJoint = i;
            hasViolated = true;
        }
    }
    
    if(hasViolated){
        double scalefactor = VelLim[maxViolatorJoint] / (abs(deltaQ[maxViolatorJoint])/deltaT) ;
        for(int i = 0; i < 7; i++){
            deltaQ[i]  = deltaQ[i]*scalefactor;
            //rw::common::Log::log().info() << "Joint: " << i << "\t Scale " << scalefactor << "\n";
        }
    }
    return deltaQ;
}


void SamplePlugin::sim_data_extration(){
    double dt = 1;
    
    ofstream q_file,dq_file,tpose_file,te_file;
    stringstream ss;
    
    /// Data for Q, dQ, Tool Pose,
    
    // Loop for each marker speed
    for(int mk_i = 0; mk_i < 3; mk_i++){
        // Update marker movement
        _marker_movement->setCurrentIndex(mk_i);
        rws_load_marker_movement();
        markerposcounter = 0;
        
        // Elapsed time
        double time = 0;
        
        // Open ze files
        ss << mk_i;
        string number = ss.str();
        
        std::string n_track_p = _tracking_points->currentText().toStdString();
        q_file.open(project_path + "Data/" + n_track_p + "_data/" + number + "_q.txt");
        dq_file.open(project_path + "Data/" + n_track_p + "_data/" + number + "_dq.txt");
        tpose_file.open(project_path + "Data/" + n_track_p + "_data/" + number + "_tpose.txt");
        q_file << "Time,q0,q1,q2,q3,q4,q5,q6\n";
        dq_file << "Time,dq0,dq1,dq2,dq3,dq4,dq5,dq6\n";
        tpose_file << "Time,x,y,z,R,P,Y\n";
        
        ss.str(string());
        
        // Reset Robot in RobWorkStudio
        _state = _wc->getDefaultState();
        Device::Ptr device = _wc->findDevice("PA10");
        Q q = device->getQ(_state);
        device->setQ(q,_state);
        //getRobWorkStudio()->setState(_state);
        
        while(markerposcounter != markerPath.size()-1){
            UpdateMarkerPos(markerPath[markerposcounter],false);
            
            vector<Transform3D<double>> trackPoints (3);
            vector<double> trackingPointsOffset(6);
            switch(nTrackingPoints){
            case 1:
                trackPoints[0] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                trackPoints[1] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                trackPoints[2] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                trackingPointsOffset = {0,0,0,0,0,0};
                break;
            case 2:
                trackPoints[0] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                trackPoints[1] = Transform3D<double>(Vector3D<double>(-0.1,0,0), Rotation3D<double>::identity());
                trackPoints[2] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                trackingPointsOffset = {0,0,164.6,0,0,0};
                break;
            case 3:
                trackPoints[0] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                trackPoints[1] = Transform3D<double>(Vector3D<double>(-0.1,0,0), Rotation3D<double>::identity());
                trackPoints[2] = Transform3D<double>(Vector3D<double>(0,0.1,0), Rotation3D<double>::identity());
                trackingPointsOffset = {0,0,164.6,0,0,164.6};
                break;
            default:
                rw::common::Log::log().info() << "The selected number of tracking points are not supported!\n";
                return;
            }
            
            boost::numeric::ublas::matrix<double> uv = sim_compute_uv(trackPoints);
            boost::numeric::ublas::matrix<double> dudv = sim_compute_du_dv(uv,trackingPointsOffset);
            
            // Calculate deltaQ
            Q deltaQ = sim_compute_deltaQ(uv,dudv);
            
            // Get device
            Device::Ptr device = _wc->findDevice("PA10");
            
            // Check Velocity limits
            Q VelLim = device->getVelocityLimits();
            
            int maxViolatorJoint;
            bool hasViolated = false;
            
            for(int i = 0; i < 7; i++){
                double maxViolaterValue = 0;
                if(abs(deltaQ[i]/dt) > VelLim[i] && abs(deltaQ[i]/dt) > maxViolaterValue ){
                    maxViolatorJoint = i;
                    hasViolated = true;
                }
            }
            
            if(hasViolated){
                double scalefactor = VelLim[maxViolatorJoint] / (abs(deltaQ[maxViolatorJoint])/dt) ;
                for(int i = 0; i < 7; i++){
                    deltaQ[i]  = deltaQ[i]*scalefactor;
                }
            }
            
            
            // Set the new Q vector in the state and update RobWorks Studio
            Q q = device->getQ(_state);
            device->setQ(q+deltaQ,_state);
            //getRobWorkStudio()->setState(_state);
            
            //Output Data For Plotting "Tracking Error". Recalculate the du,dv before updating Marker Pos
            Frame* tool = _wc->findFrame("Tool");
            Transform3D<double> worldTtool = tool->wTf(_state);
            uv = sim_compute_uv(trackPoints);
            dudv = sim_compute_du_dv(uv,trackingPointsOffset);
            
            
            // Write to files:
            q_file << time << "," << q[0] << "," << q[1] << "," << q[2] << "," << q[3] << "," << q[4] << "," << q[5] << "," << q[6] << "\n" ;
            dq_file << time << ","
                    << deltaQ[0] << ","
                    << deltaQ[1] << ","
                    << deltaQ[2] << ","
                    << deltaQ[3] << ","
                    << deltaQ[4] << ","
                    << deltaQ[5] << ","
                    << deltaQ[6] << "\n" ;
            
            RPY<double> wTt_rpy(worldTtool.R());
            tpose_file << time << ","
                       << worldTtool.P()[0] << ","
                       << worldTtool.P()[1] << ","
                       << worldTtool.P()[2] << ","
                       << wTt_rpy[0] << ","
                       << wTt_rpy[1] << ","
                       << wTt_rpy[2] << "\n";
            
            markerposcounter++;
            time += dt;
        }
        
        q_file.close();
        dq_file.close();
        tpose_file.close();
    }
    
    /// deltaT
    for(int mk_i = 0; mk_i < 3; mk_i++){
        // Update marker movement
        _marker_movement->setCurrentIndex(mk_i);
        rws_load_marker_movement();
        
        // Open ze files
        ss << mk_i;
        string number = ss.str();
        std::string n_track_p = _tracking_points->currentText().toStdString();
        te_file.open(project_path + "Data/" + n_track_p + "_data/" + number + "_te.txt");
        te_file << "Time,TrackingError\n";
        ss.str(string());
        
        for(double deltaT = 1; deltaT > 0; deltaT -= 0.05){
            
            markerposcounter = 0;
            
            // Reset Robot in RobWorkStudio
            _state = _wc->getDefaultState();
            Device::Ptr device = _wc->findDevice("PA10");
            Q q = device->getQ(_state);
            device->setQ(q,_state);
            //getRobWorkStudio()->setState(_state);
            
            // Reset largest error
            double max_error_dt = 0;
            
            while(markerposcounter != markerPath.size()-1){
                UpdateMarkerPos(markerPath[markerposcounter],false);
                
                vector<Transform3D<double>> trackPoints (3);
                vector<double> trackingPointsOffset(6);
                switch(nTrackingPoints){
                case 1:
                    trackPoints[0] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                    trackPoints[1] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                    trackPoints[2] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                    trackingPointsOffset = {0,0,0,0,0,0};
                    break;
                case 2:
                    trackPoints[0] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                    trackPoints[1] = Transform3D<double>(Vector3D<double>(-0.1,0,0), Rotation3D<double>::identity());
                    trackPoints[2] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                    trackingPointsOffset = {0,0,164.6,0,0,0};
                    break;
                case 3:
                    trackPoints[0] = Transform3D<double>(Vector3D<double>(0,0,0), Rotation3D<double>::identity());
                    trackPoints[1] = Transform3D<double>(Vector3D<double>(-0.1,0,0), Rotation3D<double>::identity());
                    trackPoints[2] = Transform3D<double>(Vector3D<double>(0,0.1,0), Rotation3D<double>::identity());
                    trackingPointsOffset = {0,0,164.6,0,0,164.6};
                    break;
                default:
                    rw::common::Log::log().info() << "The selected number of tracking points are not supported!\n";
                    return;
                }
                
                boost::numeric::ublas::matrix<double> uv = sim_compute_uv(trackPoints);
                boost::numeric::ublas::matrix<double> dudv = sim_compute_du_dv(uv,trackingPointsOffset);
                
                // Calculate deltaQ
                Q deltaQ = sim_compute_deltaQ(uv,dudv);
                
                // Get device
                Device::Ptr device = _wc->findDevice("PA10");
                
                // Check Velocity limits
                Q VelLim = device->getVelocityLimits();
                
                int maxViolatorJoint;
                bool hasViolated = false;
                
                for(int i = 0; i < 7; i++){
                    double maxViolaterValue = 0;
                    if(abs(deltaQ[i]/deltaT) > VelLim[i] && abs(deltaQ[i]/deltaT) > maxViolaterValue ){
                        maxViolatorJoint = i;
                        hasViolated = true;
                    }
                }
                
                if(hasViolated){
                    double scalefactor = VelLim[maxViolatorJoint] / (abs(deltaQ[maxViolatorJoint])/deltaT) ;
                    for(int i = 0; i < 7; i++){
                        deltaQ[i]  = deltaQ[i]*scalefactor;
                    }
                }
                
                
                // Set the new Q vector in the state and update RobWorks Studio
                Q q = device->getQ(_state);
                device->setQ(q+deltaQ,_state);
                //getRobWorkStudio()->setState(_state);
                
                //Output Data For Plotting "Tracking Error". Recalculate the du,dv before updating Marker Pos
                uv = sim_compute_uv(trackPoints);
                dudv = sim_compute_du_dv(uv,trackingPointsOffset);
                
                // Calculate euclidean distance from center of image to du,dv
                double edist = sqrt(pow(dudv(0,0),2)+pow(dudv(1,0),2));
                // Is dudv the largest error for this time step?
                if(edist > max_error_dt)    max_error_dt = edist;
                
                markerposcounter++;
            }
            
            te_file << deltaT << "," << max_error_dt << "\n";
            
        }
        te_file.close();
    }
}

void SamplePlugin::stateChangedListener(const State& state) {
    _state = state;
}

Q_EXPORT_PLUGIN(SamplePlugin)
