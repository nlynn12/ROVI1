#ifndef SAMPLEPLUGIN_HPP
#define SAMPLEPLUGIN_HPP

#include "../build/ui_SamplePlugin.h"

#include <opencv2/opencv.hpp>

#include <vector>
#include <string>
#include <fstream>
#include <chrono>

#include <rws/RobWorkStudioPlugin.hpp>
#include <rw/kinematics/State.hpp>
#include <rwlibs/opengl/RenderImage.hpp>
#include <rwlibs/simulation/GLFrameGrabber.hpp>

#include <rw/math.hpp>
#include <rw/math/Q.hpp>
#include <rw/math/EAA.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/loaders/WorkCellFactory.hpp>
#include <rw/invkin.hpp>
#include <rw/math/EigenDecomposition.hpp>

#include <QSlider>
#include <QString>
#include <QFileDialog>
#include <QCheckBox>

using namespace std;
using namespace rw::math;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::kinematics;


class SamplePlugin: public rws::RobWorkStudioPlugin, private Ui::SamplePlugin
{
Q_OBJECT
Q_INTERFACES( rws::RobWorkStudioPlugin )
public:
	SamplePlugin();
	virtual ~SamplePlugin();
	virtual void open(rw::models::WorkCell* workcell);
	virtual void close();
	virtual void initialize();

    string MarkerPathLocation;

private slots:
    // UI Main tab
	void btnPressed();
    //void rws_update_marker_position();

    // UI Settings tab
    void rws_update_project_path();
    void rws_load_workcell();
    void rws_load_marker();
    void rws_load_background();
    void rws_load_marker_movement();
    void rws_update_tracking_points();
    void rws_data_checkbox();
    void sim_data_extration();

    void timer();
	void stateChangedListener(const rw::kinematics::State& state);

private:
	static cv::Mat toOpenCVImage(const rw::sensor::Image& img);

	QTimer* _timer;

	rw::models::WorkCell::Ptr _wc;
	rw::kinematics::State _state;
	rwlibs::opengl::RenderImage *_textureRender, *_bgRender;
	rwlibs::simulation::GLFrameGrabber* _framegrabber;
    std::vector<Transform3D<double> > markerPath;
    unsigned int markerposcounter = 0;

    std::string project_path;
    bool trainingImage = false;
    bool resetPressed = false;
    int nTrackingPoints = 0;

    void UpdateMarkerPos(Transform3D<double> newtransform, bool UpdateRobworkStudio = true);

    /// Feature Extraction
    cv::Mat capture_image(QLabel *);
    void feature_extraction(cv::Mat imflip);

    /// Visual Servoing
    void visual_servoing();
    Jacobian computeImageJacobian(double u, double v, double z, double f);
    boost::numeric::ublas::matrix<double> sim_compute_uv(vector<Transform3D<double>> trackPoints);
    boost::numeric::ublas::matrix<double> sim_compute_du_dv(boost::numeric::ublas::matrix<double> uv, vector<double> featureOffset);
    Q sim_compute_deltaQ(boost::numeric::ublas::matrix<double> uv, boost::numeric::ublas::matrix<double> dudv);

    Q velocity_scale(Q deltaQ, double deltaT, double tau = 0);

    // Output stream objects for data extraction
    bool performDataExtraction = false;
    ofstream feature_te_file,feature_jpn_file,feature_jvn_file;

};

#endif /*SAMPLEPLUGIN_HPP*/
