#include "VisionMarker1.hpp"

using namespace std;
using namespace cv;

vector<float> mean_g;

float featureextraction::l2_norm(vector<float> const& u) {
    float accum = 0.;
    for (auto x : u) {
        accum += x * x;
    }
    return sqrt(accum);
}

void featureextraction::computeMean(cv::Mat src,cv::Rect roi, vector<float> &training_mean){
    cv::Mat roi_src = src(roi);

    cv::Scalar mean_s, stddev_s;
    cv::meanStdDev(roi_src,mean_s,stddev_s);
    cout << mean_s << endl;

    training_mean.resize(3);
    for(int i = 0; i < 3; i++){
        training_mean[i] = mean_s[i];
    }
}

void featureextraction::meanColorSegmentation(cv::Mat src,
                                              cv::Mat_<Vec3f> &dst,
                                              vector<float> training_mean,
                                              float etol,
                                              ColorSegMode mode /*= NONE*/,
                                              int kernelSize/* = 9*/){
    vector<float> dist(3);

    float euclideanDistTol = etol*255;

    dst = src.clone();


    for(int y = 0; y < src.rows; y++){
        Vec3f* data_ptr = dst.ptr<Vec3f>(y);
        for(int x = 0; x < src.cols; x++){
            Vec3f &bgr = data_ptr[x];

            vector<float> bgrm {bgr[0],bgr[1],bgr[2]};

            for(int i = 0; i<3; i++){
                dist[i] = bgrm[i]-training_mean[i];
            }

            if(l2_norm(dist) <= euclideanDistTol)
                bgr = Vec3f(1,1,1);
            else
                bgr = Vec3f(0,0,0);
        }
    }

    Mat kernel = Mat::ones(kernelSize,kernelSize,CV_8U);

    switch (mode) {
    case NONE:
        break;
    case ERODE:
        erode(dst,dst,kernel);
        break;
    case DILATE:
        dilate(dst,dst,kernel);
        break;
    case ERODE_DILATE:
        erode(dst,dst,kernel);
        dilate(dst,dst,kernel);
        break;
    case DILATE_ERODE:
        dilate(dst,dst,kernel);
        erode(dst,dst,kernel);
        break;
    default:
        break;
    }
}

void featureextraction::findCircles(vector<vector<Point>> contours, vector<vector<Point>> &circles, float ptol /*= 0.7*/, float atol /*= 2000*/){
    int i = 0;
    // Loop through each contour in list of contours
    for(auto component : contours){
        // Compute area, perimeter and how close the component resembles a circle
        float area = contourArea(component);
        float perimeter = arcLength(component,true);
        float percentage_circle = (4*M_PI*area)/(perimeter*perimeter);
        // Check if component is a circle
        if(percentage_circle > ptol && area > atol){
            circles.resize(i+1);
            circles[i] = component;
            i++;
        }
    }
}

void featureextraction::getCenterOfMass(cv::Mat src,vector<vector<Point>> contours,vector<Point2d> &COM){
    Point2d min,max;

    // Init
    min.x = src.cols;
    min.y = src.rows;
    max.x = 0;
    max.y = 0;

    // Loop through pixels in contours in list of contours
    for(auto contour : contours){
        for(auto p : contour){
            if(p.x < min.x) min.x = (double)p.x;
            if(p.x > max.x) max.x = (double)p.x;
            if(p.y < min.y) min.y = (double)p.y;
            if(p.y > max.y) max.y = (double)p.y;

        }
        //Compute Center of Mass and push to vector
        COM.push_back(Point2d(((max.x-min.x)/2)+min.x,((max.y-min.y)/2)+min.y));
        //Reset
        min.x = src.cols;
        min.y = src.rows;
        max.x = 0;
        max.y = 0;
    }
}

vector<Point2d> featureextraction::featureextraction_marker1(Mat src, int nTrackingPoints){
    /// IMAGE PROCESSING

    // Blue
    cv::Mat_<Vec3f> b_res;
    meanColorSegmentation(src,b_res,mean_g,0.15,DILATE_ERODE,9);

    //imshow("Marker1_mean_color_segmented",b_res);
    //waitKey(0);

    // Convert to 1 channel and Convert data type from float to 8bit unsigned
    cv::Mat b_res_gray;
    cv::cvtColor(b_res,b_res_gray,COLOR_BGR2GRAY);
    b_res_gray.convertTo(b_res_gray,CV_8UC1);



    // FindContours
    vector<vector<Point>> b_contours;
    std::vector<cv::Vec4i> b_hierarchy;
    cv::findContours(b_res_gray,b_contours,b_hierarchy,CV_RETR_LIST,CHAIN_APPROX_NONE);

    //FindCircles
    vector<vector<Point>> b_circles;
    findCircles(b_contours,b_circles,0.81);

    //imshow("imflip",b_res_gray);
    //waitKey(0);

    //ComputeCOM
    vector<Point2d> b_COM;
    getCenterOfMass(b_res_gray,b_circles,b_COM);

    // Compute largest euclidean dist
    int maxdist = 0;
    Point2d p1,p2;
    for(unsigned int i = 0; i<b_COM.size();i++){
        for(unsigned int j = 0; j<b_COM.size(); j++){
            int dist = sqrt(pow(b_COM[j].x - b_COM[i].x,2)+pow(b_COM[j].y - b_COM[i].y,2));
            if(dist>maxdist){
                maxdist = dist;
                p1 = b_COM[j];
                p2 = b_COM[i];
            }
        }
    }

    // Compute centroid of marker
    Point2d COM_marker = (p1+p2)*0.5;

    // Draw line
    if(b_COM.size() == 3){

        Point2d COM_corr(COM_marker.x-(src.cols/2),COM_marker.y-(src.rows/2));

        int indexP3 = 0;
        for(unsigned int i = 0; i<3; i++){
            if(b_COM[i] != p1 && b_COM[i] != p2){
                indexP3 = i;
                //rw::common::Log::log().info() << "P3: " << indexP3 << "\n";
            }
        }

        vector<Point2d> tracking_Point2ds(3);

        switch(nTrackingPoints){
        case 1:
            tracking_Point2ds[0] = COM_corr;
            tracking_Point2ds[1] = COM_corr;
            tracking_Point2ds[2] = COM_corr;
            break;
        case 2:
            tracking_Point2ds[0] = COM_corr;
            tracking_Point2ds[1] = COM_corr;
            tracking_Point2ds[2] = Point2d(b_COM[indexP3].x-(src.cols/2),b_COM[indexP3].y-(src.rows/2));
            break;
        case 3:
            tracking_Point2ds[0] = COM_corr; //Point2d(b_COM[indexP3].x-(src.cols/2),b_COM[indexP3].y-(src.rows/2));
            tracking_Point2ds[1] = Point2d(p2.x-(src.cols/2),p2.y-(src.rows/2));
            tracking_Point2ds[2] = Point2d(p1.x-(src.cols/2),p1.y-(src.rows/2));
            break;
        default:
            break;
        }
        /*tracking_Point2ds.push_back(Point2d(b_COM[indexP3].x-(src.cols/2),b_COM[indexP3].y-(src.rows/2)));
        tracking_Point2ds.push_back(Point2d(p2.x-(src.cols/2),p2.y-(src.rows/2)));
        tracking_Point2ds.push_back(Point2d(p1.x-(src.cols/2),p1.y-(src.rows/2)));*/
        return tracking_Point2ds;
    }

    vector<Point2d> fail;
    for(int i = 0; i < 3; i++)
        fail.push_back(Point2d(0,0));
    return fail;

}

void featureextraction::initialize_marker1(Mat src){
    //cv::Rect b_roi(445,525,50,50);
    cv::Rect b_roi(50,160,40,40);
    computeMean(src,b_roi, mean_g);
}
