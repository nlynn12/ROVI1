#ifndef VISIONMARKER1_HPP
#define VISIONMARKER1_HPP

#include <iostream>
#include <vector>
#include <string>
#include <numeric>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

#include <rw/common/Log.hpp>

using namespace std;
using namespace cv;

namespace featureextraction{
    enum ColorSegMode{
        NONE,
        ERODE,
        DILATE,
        ERODE_DILATE,
        DILATE_ERODE
    };
    float l2_norm(vector<float> const& u);
    void computeMean(cv::Mat src,cv::Rect roi, vector<float> &training_mean);
    void meanColorSegmentation(cv::Mat src, cv::Mat_<Vec3f> &dst, vector<float> training_mean, float etol, ColorSegMode mode = NONE, int kernelSize = 9);
    void findCircles(vector<vector<Point>> contours, vector<vector<Point>> &circles, float ptol = 0.7, float atol = 2000);
    void getCenterOfMass(cv::Mat src,vector<vector<Point>> contours,vector<Point2d> &COM);
    vector<Point2d> featureextraction_marker1(Mat src,int nTrackingPoints);
    void initialize_marker1(Mat src);
}

#endif
