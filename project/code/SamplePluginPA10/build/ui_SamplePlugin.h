/********************************************************************************
** Form generated from reading UI file 'SamplePlugin.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SAMPLEPLUGIN_H
#define UI_SAMPLEPLUGIN_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDockWidget>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QTabWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SamplePlugin
{
public:
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QPushButton *_btn1;
    QLabel *_label;
    QLabel *_image_res;
    QLabel *label_3;
    QLabel *label_8;
    QLabel *label_9;
    QPushButton *_btn2;
    QWidget *tab_2;
    QLabel *label;
    QLineEdit *_pro_path;
    QComboBox *_marker;
    QLabel *label_4;
    QLabel *label_5;
    QComboBox *_marker_movement;
    QLabel *label_6;
    QComboBox *_background;
    QDoubleSpinBox *_deltat;
    QLabel *label_7;
    QPushButton *_load_scene;
    QLabel *label_2;
    QComboBox *_tracking_points;
    QPushButton *_data_extractor;
    QCheckBox *_data_checkbox;

    void setupUi(QDockWidget *SamplePlugin)
    {
        if (SamplePlugin->objectName().isEmpty())
            SamplePlugin->setObjectName(QString::fromUtf8("SamplePlugin"));
        SamplePlugin->resize(275, 589);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        verticalLayout_2 = new QVBoxLayout(dockWidgetContents);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tabWidget = new QTabWidget(dockWidgetContents);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        _btn1 = new QPushButton(tab);
        _btn1->setObjectName(QString::fromUtf8("_btn1"));
        _btn1->setGeometry(QRect(0, 20, 255, 26));
        _label = new QLabel(tab);
        _label->setObjectName(QString::fromUtf8("_label"));
        _label->setGeometry(QRect(0, 170, 251, 151));
        _label->setMinimumSize(QSize(0, 0));
        _image_res = new QLabel(tab);
        _image_res->setObjectName(QString::fromUtf8("_image_res"));
        _image_res->setGeometry(QRect(0, 360, 251, 151));
        label_3 = new QLabel(tab);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(0, 150, 111, 17));
        label_8 = new QLabel(tab);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(0, 340, 111, 17));
        label_9 = new QLabel(tab);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(50, 0, 151, 20));
        _btn2 = new QPushButton(tab);
        _btn2->setObjectName(QString::fromUtf8("_btn2"));
        _btn2->setGeometry(QRect(0, 50, 255, 26));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        label = new QLabel(tab_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(0, 0, 91, 17));
        _pro_path = new QLineEdit(tab_2);
        _pro_path->setObjectName(QString::fromUtf8("_pro_path"));
        _pro_path->setGeometry(QRect(0, 20, 251, 27));
        _marker = new QComboBox(tab_2);
        _marker->setObjectName(QString::fromUtf8("_marker"));
        _marker->setGeometry(QRect(0, 180, 251, 27));
        label_4 = new QLabel(tab_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(0, 160, 91, 17));
        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(0, 210, 131, 17));
        _marker_movement = new QComboBox(tab_2);
        _marker_movement->setObjectName(QString::fromUtf8("_marker_movement"));
        _marker_movement->setGeometry(QRect(0, 230, 251, 27));
        label_6 = new QLabel(tab_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(0, 260, 91, 17));
        _background = new QComboBox(tab_2);
        _background->setObjectName(QString::fromUtf8("_background"));
        _background->setGeometry(QRect(0, 280, 251, 27));
        _deltat = new QDoubleSpinBox(tab_2);
        _deltat->setObjectName(QString::fromUtf8("_deltat"));
        _deltat->setGeometry(QRect(0, 330, 251, 27));
        _deltat->setDecimals(3);
        _deltat->setMinimum(0.001);
        _deltat->setMaximum(1);
        _deltat->setSingleStep(0.05);
        _deltat->setValue(0.05);
        label_7 = new QLabel(tab_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(0, 310, 91, 17));
        _load_scene = new QPushButton(tab_2);
        _load_scene->setObjectName(QString::fromUtf8("_load_scene"));
        _load_scene->setGeometry(QRect(0, 50, 251, 26));
        label_2 = new QLabel(tab_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(0, 110, 111, 17));
        _tracking_points = new QComboBox(tab_2);
        _tracking_points->setObjectName(QString::fromUtf8("_tracking_points"));
        _tracking_points->setGeometry(QRect(0, 130, 251, 27));
        _data_extractor = new QPushButton(tab_2);
        _data_extractor->setObjectName(QString::fromUtf8("_data_extractor"));
        _data_extractor->setGeometry(QRect(0, 440, 251, 26));
        _data_checkbox = new QCheckBox(tab_2);
        _data_checkbox->setObjectName(QString::fromUtf8("_data_checkbox"));
        _data_checkbox->setGeometry(QRect(0, 370, 141, 22));
        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);


        verticalLayout_2->addLayout(verticalLayout);

        SamplePlugin->setWidget(dockWidgetContents);

        retranslateUi(SamplePlugin);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(SamplePlugin);
    } // setupUi

    void retranslateUi(QDockWidget *SamplePlugin)
    {
        SamplePlugin->setWindowTitle(QApplication::translate("SamplePlugin", "DockWidget", 0, QApplication::UnicodeUTF8));
        _btn1->setText(QApplication::translate("SamplePlugin", "Start/Stop", 0, QApplication::UnicodeUTF8));
        _label->setText(QString());
        _image_res->setText(QString());
        label_3->setText(QApplication::translate("SamplePlugin", "Pre movement", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("SamplePlugin", "Post movement", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("SamplePlugin", "Automatic Movement", 0, QApplication::UnicodeUTF8));
        _btn2->setText(QApplication::translate("SamplePlugin", "Reset", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("SamplePlugin", "Main", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SamplePlugin", "Project Path", 0, QApplication::UnicodeUTF8));
        _pro_path->setText(QApplication::translate("SamplePlugin", "/home/nicolai/Desktop/", 0, QApplication::UnicodeUTF8));
        _marker->clear();
        _marker->insertItems(0, QStringList()
         << QApplication::translate("SamplePlugin", "Marker1", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "Marker2a", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "Marker2b", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "Marker3", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "Simulation", 0, QApplication::UnicodeUTF8)
        );
        label_4->setText(QApplication::translate("SamplePlugin", "Marker", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("SamplePlugin", "Marker Movement", 0, QApplication::UnicodeUTF8));
        _marker_movement->clear();
        _marker_movement->insertItems(0, QStringList()
         << QApplication::translate("SamplePlugin", "Fast", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "Medium", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "Slow", 0, QApplication::UnicodeUTF8)
        );
        label_6->setText(QApplication::translate("SamplePlugin", "Background", 0, QApplication::UnicodeUTF8));
        _background->clear();
        _background->insertItems(0, QStringList()
         << QApplication::translate("SamplePlugin", "color1", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "color2", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "color3", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "lines1", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "texture1", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "texture2", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "texture3", 0, QApplication::UnicodeUTF8)
        );
        label_7->setText(QApplication::translate("SamplePlugin", "Timestep", 0, QApplication::UnicodeUTF8));
        _load_scene->setText(QApplication::translate("SamplePlugin", "Load Workcell", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("SamplePlugin", "Tracking Points", 0, QApplication::UnicodeUTF8));
        _tracking_points->clear();
        _tracking_points->insertItems(0, QStringList()
         << QApplication::translate("SamplePlugin", "1pt", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "2pt", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SamplePlugin", "3pt", 0, QApplication::UnicodeUTF8)
        );
        _data_extractor->setText(QApplication::translate("SamplePlugin", "Robotics Simulation", 0, QApplication::UnicodeUTF8));
        _data_checkbox->setText(QApplication::translate("SamplePlugin", "Data Extraction", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("SamplePlugin", "Settings", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SamplePlugin: public Ui_SamplePlugin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SAMPLEPLUGIN_H
