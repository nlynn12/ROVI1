# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/nicolai/Desktop/rovi1_nlynn12_jeben12/SamplePluginPA10/src/SamplePlugin.cpp" "/home/nicolai/Desktop/rovi1_nlynn12_jeben12/SamplePluginPA10/build/CMakeFiles/SamplePlugin.dir/SamplePlugin.cpp.o"
  "/home/nicolai/Desktop/rovi1_nlynn12_jeben12/SamplePluginPA10/src/VisionMarker1.cpp" "/home/nicolai/Desktop/rovi1_nlynn12_jeben12/SamplePluginPA10/build/CMakeFiles/SamplePlugin.dir/VisionMarker1.cpp.o"
  "/home/nicolai/Desktop/rovi1_nlynn12_jeben12/SamplePluginPA10/build/moc_SamplePlugin.cxx" "/home/nicolai/Desktop/rovi1_nlynn12_jeben12/SamplePluginPA10/build/CMakeFiles/SamplePlugin.dir/moc_SamplePlugin.cxx.o"
  "/home/nicolai/Desktop/rovi1_nlynn12_jeben12/SamplePluginPA10/build/qrc_resources.cxx" "/home/nicolai/Desktop/rovi1_nlynn12_jeben12/SamplePluginPA10/build/CMakeFiles/SamplePlugin.dir/qrc_resources.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "BOOST_DISABLE_ASSERTS"
  "QT_CORE_LIB"
  "QT_DESIGNER_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "QT_OPENGL_LIB"
  "QT_UITOOLS_LIB"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/boostbindings"
  "/usr/include/eigen3"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../src"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/rwyaobi"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/rwpqp"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/lua/src"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/qhull/src"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/unzip"
  "/home/nicolai/Programs/RobWork/RobWork/cmake/../ext/assimp/include"
  "/usr/include/qt4"
  "/usr/include/qt4/QtOpenGL"
  "/usr/include/qt4/QtDesigner"
  "/usr/include/qt4/QtUiTools"
  "/usr/include/qt4/QtGui"
  "/usr/include/qt4/QtCore"
  "/home/nicolai/Programs/RobWork/RobWorkStudio/cmake/../src"
  "/home/nicolai/Programs/RobWork/RobWorkStudio/cmake/../ext/qtpropertybrowser/src"
  "/usr/local/include/opencv"
  "/usr/local/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
