#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>
#define fis 3
#define frad (fis-1)/2
#define Qorder 2

using namespace std;
using namespace cv;


Mat_<Vec3b> intensity_increase(const Mat_<Vec3b> &img, int amount){
    Mat_<Vec3b> intensified_img = img.clone();

    for(size_t x = 0; x < intensified_img.cols; x++){
        for(size_t y = 0; y < intensified_img.rows; y++){
            for(int i = 0; i < 3; i++){
                intensified_img(y,x)[i]=saturate_cast<uchar>(intensified_img(y,x)[i]+amount);
            }
        }
    }

    return intensified_img;
}

cv::MatND getHistogram(const cv::Mat &image){
    int numOfIm = 1;
    int channels[] = {0}; //The channels of the images used in the computation
    cv::Mat mask; //not using mask, so leave empty
    cv::MatND hist;
    int dimensions = 1;//dimensionality of the histogram
    int numOfBins[] = {256}; //number of bins
    float minMaxValue[] = {0.0, 255.0}; //the min and max value of the bins
    const float* ranges[] = {minMaxValue}; //Array of min-max value arrays, so every dimension is assigned min and max
    cv::calcHist(&image, numOfIm, channels, mask, hist, dimensions, numOfBins, ranges);
    return hist; //the returned type is 32FC1
}


cv::Mat getHistogramImage(const cv::Mat &image)
{
    int numOfBins = 256;
    cv::MatND hist= getHistogram(image);

    // Get min and max bin values
    double maxVal=0, minVal=0;
    cv::minMaxLoc(hist, &minVal, &maxVal, 0, 0);
    cv::Mat histImg = cv::Mat::zeros(numOfBins,numOfBins, CV_8UC1);

    int hpt = static_cast<int>(0.9*numOfBins); // set highest point at 90% of numOfBins

    for( int h = 0; h < numOfBins; h++ ) // Draw a vertical line for each bin
    {
        float binVal = hist.at<float>(h);
        int intensity;
        if (binVal != 0){
            intensity = static_cast<int>(hpt* binVal/maxVal) ;
            cv::line(histImg,cv::Point(h,numOfBins), cv::Point(h,numOfBins-intensity), cv::Scalar::all(255));
        }
    }
    return histImg;
}

void contraHarmonicMeanFilter(const cv::Mat_<uchar> src, cv::Mat_<uchar> dst, const int Q, const int filter_size){
    double sum1 = 0;
    double sum2 = 0;

    int filter_index = ((filter_size-1)/2);

    for(size_t x = filter_index; x < src.cols-filter_index; x++){
        for(size_t y = filter_index; y < src.rows-filter_index; y++){
            for(int i = -filter_index; i <= filter_index; i++){
                for(int j = -filter_index; j <= filter_index; j++){
                    sum1 += pow(src(y+i,x+j), (Q+1));
                    sum2 += pow(src(y+i,x+j), Q);
                 }
            }
            if(sum2 == 0) sum2 = 0.001;
            dst(y,x) = sum1 / sum2;
            sum1 = 0;
            sum2 = 0;
        }
    }
}

void maxFilter(const cv::Mat_<uchar> src, cv::Mat_<uchar> dst, const int filter_size){
    int filter_radius = ((filter_size-1)/2);
    int largest = 0;

    for(size_t x = filter_radius; x < src.cols-filter_radius; x++){
        for(size_t y = filter_radius; y < src.rows-filter_radius; y++){
            for(int i = -filter_radius; i <= filter_radius; i++){
                for(int j = -filter_radius; j <= filter_radius; j++){
                    if(largest < src(y+i,x+j))
                        largest = src(y+i,x+j);
                }
            }
            dst(y,x) = largest;
            largest = 0;
        }
    }
}

int main()
{
    // Load image
    Mat src = imread("/home/jjb/workspace/VisionMini/images/Image1.png", CV_LOAD_IMAGE_GRAYSCALE);

    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }


    Mat_<uchar> pad_src;

    copyMakeBorder(src, pad_src, frad, frad, frad, frad, BORDER_CONSTANT, 0);
    //imwrite("/home/jjb/Desktop/padded.png",pad_src);

    Mat_<uchar> filtered;

    src.copyTo(filtered);

    maxFilter(pad_src,filtered,fis);

    copyMakeBorder(filtered, pad_src, frad, frad, frad, frad, BORDER_CONSTANT, 0);

    Mat_<uchar> meaned = src.clone();
    contraHarmonicMeanFilter(pad_src, meaned,Qorder,fis);

    copyMakeBorder(meaned, pad_src, frad, frad, frad, frad, BORDER_CONSTANT, 0);
    contraHarmonicMeanFilter(pad_src, meaned,-Qorder,fis);

    //MatND hist2 = getHistogramImage(crop_filter);

    imwrite("/home/jjb/Desktop/max_filter_meaned.png",meaned);
    imwrite("/home/jjb/Desktop/max_filter_img1.png",filtered);
   // imwrite("/home/jjb/Desktop/max_filter_img1_hist.png",hist2);


    cout << "Program finished" << endl;

    waitKey(0);
    destroyAllWindows();

    return 0;
}

