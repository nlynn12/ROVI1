#ifndef OPENCVFILTERS_H
#define OPENCVFILTERS_H

#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>

cv::Mat loadImage(std::string image, int CV_ARG){
    cv::Mat src = cv::imread(image,CV_ARG);
    if(src.empty()){
        std::cout << "Error: No such file or directory" << std::endl;
    }
    return src;
}

cv::Mat createEmptyCopyOf(cv::Mat src, int CV_ARG=CV_8UC1){
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_ARG);
    return result;
}

cv::Mat createBorder(cv::Mat src, int padding, int border_intensity = 0){
    cv::Mat result;
    cv::copyMakeBorder(src,result,padding,padding,padding,padding, cv::BORDER_CONSTANT, border_intensity);
    return result;
}

cv::Mat maxFilter(cv::Mat src, int filter_size){
    CV_Assert(src.depth() == CV_8U);
    const int r = ((filter_size-1)/2);

    cv::Mat padded = createBorder(src,r);
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_8UC1);

    int largest = 0;

    for(size_t x = 0; x < src.cols; x++){
        for(size_t y = 0; y < src.rows; y++){
            for(int i = -r; i <= r; i++){
                for(int j = -r; j <= r; j++){
                    if(largest < padded.at<uchar>(y+i,x+j))
                        largest = padded.at<uchar>(y+i,x+j);
                }
            }
            result.at<uchar>(y,x) = largest;
            largest = 0;
        }
    }
    return result;
}

cv::Mat contraHarmonicMeanFilter(cv::Mat src, int filter_size, int Q){
    /// Q < -1   eliminates salt noise
    /// Q = -1   harmonic mean
    /// Q = 0    arithmetic mean
    /// Q > 0    eliminates pepper noise

    CV_Assert(src.depth() == CV_8U);
    const int r = ((filter_size-1)/2);

    double sum1 = 0;
    double sum2 = 0;

    cv::Mat padded = createBorder(src,r);
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_8UC1);

    for(size_t x = 0; x < src.cols; x++){
        for(size_t y = 0; y < src.rows; y++){
            for(int i = -r; i <= r; i++){
                for(int j = -r; j <= r; j++){
                    double tmp = (int)padded.at<uchar>(y+i,x+j);
                    sum1 += pow(tmp, (Q+1));
                    sum2 += pow(tmp, Q);
                }
            }
            if(sum2 == 0) sum2 = 0.001;
            result.at<uchar>(y,x) = sum1 / sum2;
            sum1 = 0;
            sum2 = 0;
        }
    }
    return result;
}

cv::Mat midpointFilter(cv::Mat src, int filter_size){
    /// Good for randomly distributed noise

    CV_Assert(src.depth() == CV_8U);
    const int r = ((filter_size-1)/2);

    double min=255,max=0;
    double tmp = 0;

    cv::Mat padded = createBorder(src,r);
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_8UC1);

    for(size_t x = 0; x < src.cols; x++){
        for(size_t y = 0; y < src.rows; y++){
            //cv::Rect roi = cv::Rect(x-r,y-r,filter_size,filter_size);
            //cv::Mat filter = padded(roi);
            //cv::minMaxLoc(filter,&min, &max);
            for(int i = -r; i <= r; i++){
                for(int j = -r; j <= r; j++){
                    tmp = padded.at<uchar>(y+i,x+j);
                    if(max < tmp)
                        max = tmp;
                    if(min > tmp)
                        min = tmp;
                }
            }
            result.at<uchar>(y,x) = (min+max)/2;
            min = 255;
            max = 0;
        }
    }
    return result;
}


cv::Mat calculate_dft(cv::Mat src);

cv::MatND getHistogram(const cv::Mat &image){
    int numOfIm = 1;
    int channels[] = {0}; //The channels of the images used in the computation
    cv::Mat mask; //not using mask, so leave empty
    cv::MatND hist;
    int dimensions = 1;//dimensionality of the histogram
    int numOfBins[] = {256}; //number of bins
    float minMaxValue[] = {0.0, 256.0}; //the min and max value of the bins
    const float* ranges[] = {minMaxValue}; //Array of min-max value arrays, so every dimension is assigned min and max
    cv::calcHist(&image, numOfIm, channels, mask, hist, dimensions, numOfBins, ranges);
    return hist; //the returned type is 32FC1
}


cv::Mat cropImage(cv::Mat src, int x, int y, int width, int heigth){
    cv::Rect roi(x,y,width,heigth);
    cv::Mat tmp = src(roi);
    return tmp;
}

cv::Mat histEqual(const cv::Mat &image){

    cv::Mat result = cv::Mat::zeros(image.rows,image.cols,CV_8UC1);

    cv::MatND hist = getHistogram(image);

    double imageSize = image.cols * image.rows;
    int transFormvalues[256] = {0};
    double tmp = 0;
    for(int i = 0; i < 256; i++){
        float binVal = hist.at<float>(i);
        tmp += 255*(binVal / imageSize);
        transFormvalues[i] = round(tmp);
    }

    for(size_t x = 0; x < image.cols; x++){
        for(size_t y = 0; y < image.rows; y++){
            int pValue = (int)image.at<uchar>(y,x);
            result.at<uchar>(y,x) = transFormvalues[pValue];
        }
    }
    return result;
}

cv::Mat getHistogramImage(const cv::Mat &image){
    int numOfBins = 256;
    cv::MatND hist= getHistogram(image);

    // Get min and max bin values
    double maxVal=0, minVal=0;
    cv::minMaxLoc(hist, &minVal, &maxVal, 0, 0);
    std::cout << "maxVal: " << maxVal << std::endl;
    cv::Mat histImg = cv::Mat::zeros(numOfBins,numOfBins, CV_8UC1);

    int hpt = static_cast<int>(0.9*numOfBins); // set highest point at 90% of numOfBins

    for( int h = 0; h < numOfBins; h++ ) // Draw a vertical line for each bin
    {
        float binVal = hist.at<float>(h);
        int intensity;
        if (binVal != 0){
            intensity = static_cast<int>(hpt* binVal/maxVal) ;
            cv::line(histImg,cv::Point(h,numOfBins), cv::Point(h,numOfBins-intensity), cv::Scalar::all(255));
        }
    }

    std::cout << "Impulse noise estimation:" << std::endl;
    std::cout << "Percentage black pixels: " << (hist.at<float>(0)/(image.cols * image.rows))*100 << std::endl;
    std::cout << "Percentage white pixels: " << (hist.at<float>(255)/(image.cols * image.rows))*100 << std::endl;

    return histImg;
}



void analyzeImage(cv::Mat src, std::string filename){
    cv::Mat croppedImage;
    croppedImage = cropImage(src,1000,1375,500,200);

    // Spatial domain
    cv::MatND hist = getHistogramImage(croppedImage);
    imwrite("../../../f_images/"+filename+".png",hist);

    //Frequency domain
    calculate_dft(src);
}

template<class ImgT>
void dftshift(ImgT& img) {
    const int cx = img.cols/2;
    const int cy = img.rows/2;

    ImgT tmp;
    ImgT topLeft(img, cv::Rect(0, 0, cx, cy));
    ImgT topRight(img, cv::Rect(cx, 0, cx, cy));
    ImgT bottomLeft(img, cv::Rect(0, cy, cx, cy));
    ImgT bottomRight(img, cv::Rect(cx, cy, cx, cy));

    topLeft.copyTo(tmp);
    bottomRight.copyTo(topLeft);
    tmp.copyTo(bottomRight);

    topRight.copyTo(tmp);
    bottomLeft.copyTo(topRight);
    tmp.copyTo(bottomLeft);
}

cv::Mat calculate_dft(cv::Mat src){
    // Get original size
    int wxOrig = src.cols;
    int wyOrig = src.rows;


    int m = cv::getOptimalDFTSize( 2*wyOrig );
    int n = cv::getOptimalDFTSize( 2*wxOrig );

    copyMakeBorder(src, src, 0, m - wyOrig, 0, n - wxOrig, cv::BORDER_CONSTANT, cv::Scalar::all(0));

    // Get padded image size
    const int wx = src.cols, wy = src.rows;
    const int cx = wx/2, cy = wy/2;

    std::cout << wxOrig << " " << wyOrig << std::endl;
    std::cout << wx << " " << wy << std::endl;
    std::cout << cx << " " << cy << std::endl;

    // Compute DFT of image
    cv::Mat_<float> imgs[] = {src.clone(), cv::Mat_<float>::zeros(wy, wx)};
    cv::Mat_<cv::Vec2f> img_dft;
    cv::merge(imgs, 2, img_dft);
    cv::dft(img_dft, img_dft);

    // Shift to center
    dftshift(img_dft);

    // Used for visualization only
    cv::Mat_<float> magnitude, phase;
    cv::split(img_dft, imgs);
    cv::cartToPolar(imgs[0], imgs[1], magnitude, phase);
    magnitude = magnitude + 1.0f;
    cv::log(magnitude, magnitude);
    cv::normalize(magnitude, magnitude, 0, 1, CV_MINMAX);
    cv::imwrite("../../../dft_images/img4_2_dft_after_notch.png", magnitude * 255);
    return img_dft;
}

cv::Mat globalImpactRejection(cv::Mat src, int filter_size, int minThreshold, int maxThreshold){

    CV_Assert(src.depth() == CV_8U);
    const int r = ((filter_size-1)/2);

    double sum = 0;
    double divisor = 0;
    double tmp = 0;
    double tmp1 = 0;

    cv::Mat padded = createBorder(src,r);
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_8UC1);

    for(size_t x = 0; x < src.cols; x++){
        for(size_t y = 0; y < src.rows; y++){
            tmp1 = (int)padded.at<uchar>(y,x);
            if(tmp1 >= maxThreshold || tmp1 <= minThreshold){

             for(int i = -r; i <= r; i++){
                for(int j = -r; j <= r; j++){
                    tmp = (int)padded.at<uchar>(y+i,x+j);
                    if(tmp < maxThreshold && tmp > minThreshold){
                        sum += tmp;
                        divisor++;
                    }
                }
            }
            }
            if(divisor != 0){
            result.at<uchar>(y,x) = sum/divisor;
            }else{
            result.at<uchar>(y,x)  = tmp1;
            }
            sum = 0;
            divisor = 0;



        }
    }
    return result;
}

double DTC(int u, int v, cv::Mat src){
   double M = src.rows;
   double N = src.cols;
   double dist = sqrt( pow(u - M / 2, 2) + pow(v - N / 2, 2) );
   //std::cout<<dist<<std::endl;
   return dist;


}

cv::Mat butterworth(cv::Mat src, int d0, int width){
    int wxOrig = src.cols;
    int wyOrig = src.rows;

    int m = cv::getOptimalDFTSize( 2*wyOrig );
    int n = cv::getOptimalDFTSize( 2*wxOrig );

    cv::Mat_<cv::Vec2f> filter(m,n);
    cv::Mat dft_orign = calculate_dft(src);

    for(size_t x = 0; x < filter.cols; x++){
        for(size_t y = 0; y < filter.rows; y++){
            double D = DTC(y,x,filter);
            double DW = D*width;

            filter(y,x)[0] = 1 / (1 + pow(DW / (D*D - d0*d0) , 2));
        }
    }


    cv::mulSpectrums(filter,dft_orign,dft_orign,cv::DFT_ROWS);
    dftshift(dft_orign);
    cv::Mat_<float> output;
    cv::dft(dft_orign, output, cv::DFT_INVERSE | cv::DFT_REAL_OUTPUT);
    cv::Mat_<float> croppedOutput(output,cv::Rect(0,0,wxOrig,wyOrig));
    cv::normalize(croppedOutput, croppedOutput, 0, 1, CV_MINMAX);

    return croppedOutput;
}

double * DTC2(int u, int v, int uk, int vk, cv::Mat src){
   double M = src.rows;
   double N = src.cols;
   static double dist[2];
   dist[0] = sqrt( pow(u - M/2 - uk, 2) + pow(v - N/2 - vk, 2) );
   dist[1] = sqrt( pow(u - M/2 + uk, 2) + pow(v - N/2 + vk, 2) );
   return dist;


}
cv::Mat butterworthNotch(cv::Mat src, int radius,int NofPairs, int * notchP, int order,bool savefilter ){
    int wxOrig = src.cols;
    int wyOrig = src.rows;

    int m = cv::getOptimalDFTSize( 2*wyOrig );
    int n = cv::getOptimalDFTSize( 2*wxOrig );

    cv::Mat_<cv::Vec2f> filter(m,n);
    cv::Mat dft_orign = calculate_dft(src);


      for(size_t x = 0; x < filter.cols; x++){
        for(size_t y = 0; y < filter.rows; y++){
            double tmp = 1;
            for(int k = 0; k < NofPairs; k++){
            double *D = DTC2(y,x,notchP[k*2],notchP[k*2+1],filter);
            //D =
            double Dk = D[0];
            double Dmk = D[1];
            tmp *= (1 / (1 + pow(radius / Dk , 2*order))) * (1 / (1 + pow(radius / Dmk , 2*order)));
            filter(y,x)[0] = tmp;
          }
       }
    }

    if(savefilter){
    // Used for visualization of filter only
    cv::Mat_<float> imgs[] = {dft_orign.clone(), cv::Mat_<float>::zeros(m, n)};
    cv::Mat_<float> magnitude, phase;
    cv::split(filter, imgs);
    cv::cartToPolar(imgs[0], imgs[1], magnitude, phase);
    magnitude = magnitude + 1.0f;
    cv::log(magnitude, magnitude);
    cv::normalize(magnitude, magnitude, 0, 1, CV_MINMAX);
    cv::imwrite("../../../dft_images/filter.png", magnitude * 255);
    }



    cv::mulSpectrums(filter,dft_orign,dft_orign,cv::DFT_ROWS);
    dftshift(dft_orign);
    cv::Mat_<float> output;
    cv::dft(dft_orign, output, cv::DFT_INVERSE | cv::DFT_REAL_OUTPUT);
    cv::Mat_<float> croppedOutput(output,cv::Rect(0,0,wxOrig,wyOrig));
    cv::normalize(croppedOutput, croppedOutput, 0, 1, CV_MINMAX);

    return croppedOutput;
}


#endif // OPENCVFILTERS_H
