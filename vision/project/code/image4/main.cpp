#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>


#include "opencvfilters.h"

using namespace std;
using namespace cv;

int main(){
    //IMAGE 4 FREQ FILTERING
    Mat src = loadImage("../../../images/Image4_2.png",CV_LOAD_IMAGE_GRAYSCALE);
    imwrite("../../../f_images/img4_2_src.png", src);
    analyzeImage(src,"img4_2_hist");


    //Butterworth
    //Mat Notchfilt = butterworth(src, 807, 27);
    //analyzeImage(Notchfilt*255,"img4_2_hist_notch");
    //cv::imwrite("../../../dft_images/img4_2_butterworthNotchFiltered.png", Notchfilt*255);

    int Numberofpairs = 4;
    int notchPairs[Numberofpairs * 2] = {0,808,586,568,830,0,-586,568};

    Mat Notchfilt = butterworthNotch(src,20,Numberofpairs,notchPairs,4,true);
    analyzeImage(Notchfilt*255,"img4_2_hist_notch");
    cv::imwrite("../../../f_images/img4_2_butterworthNotchFiltered.png", Notchfilt*255);

    cout << "Program finished" << endl;
    return 0;
}
