#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>


#include "opencvfilters.h"

using namespace std;
using namespace cv;


int main(){
    /*
    //IMAGE 2 Spatial Filtering
    Mat src = loadImage("../../../images/Image2.png",CV_LOAD_IMAGE_GRAYSCALE);
    imwrite("../../../f_images/img2_src.png",src);
    analyzeImage(src,"img2_hist");

    Mat filteredImp = globalImpactRejection(src,5,0,255);
    imwrite("../../../f_images/img2_impactfilter.png",filteredImp);
    analyzeImage(filteredImp,"img2_impact_hist");

    Mat filteredImpMidt = midpointFilter(filteredImp,3);
    imwrite("../../../f_images/img2_filteredImpMidt.png",filteredImpMidt);
    analyzeImage(filteredImpMidt,"img2_hist_filtered");

    Mat equalized = histEqual(filteredImpMidt);
    imwrite("../../../f_images/img2_filteredequalized_v2.png",equalized);
    analyzeImage(equalized,"img2_equal_hist");
    */

    //IMAGE 4 FREQ FILTERING
    Mat src2 = loadImage("../../../images/Image4_2.png",CV_LOAD_IMAGE_GRAYSCALE);
    analyzeImage(src2,"img4_hist");

    Mat Notchfilt = butterworthNotch(src2, 807, 27);

    // Used for visualization only
    //cv::Mat_<float> imgs[] = {cv::Mat_<float>::zeros(src2.rows,src2.cols), cv::Mat_<float>::zeros(src2.rows, src2.cols)};
    //cv::Mat_<float> magnitude, phase;
    //cv::split(Notchfilt, imgs);
    //cv::cartToPolar(imgs[0], imgs[1], magnitude, phase);
    //magnitude = magnitude + 1.0f;
    //cv::log(magnitude, magnitude);
    //cv::normalize(magnitude, magnitude, 0, 1, CV_MINMAX);
    cv::imwrite("../../../dft_images/NotchFilterIMG4.png", Notchfilt*255);


    cout << "Program finished" << endl;
    return 0;
}
