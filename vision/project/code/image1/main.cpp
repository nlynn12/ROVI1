#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>


#include "opencvfilters.h"

using namespace std;
using namespace cv;

void calculate_dft(cv::Mat src);

cv::MatND getHistogram(const cv::Mat &image){
    int numOfIm = 1;
    int channels[] = {0}; //The channels of the images used in the computation
    cv::Mat mask; //not using mask, so leave empty
    cv::MatND hist;
    int dimensions = 1;//dimensionality of the histogram
    int numOfBins[] = {256}; //number of bins
    float minMaxValue[] = {0.0, 255.0}; //the min and max value of the bins
    const float* ranges[] = {minMaxValue}; //Array of min-max value arrays, so every dimension is assigned min and max
    cv::calcHist(&image, numOfIm, channels, mask, hist, dimensions, numOfBins, ranges);
    return hist; //the returned type is 32FC1
}


cv::Mat getHistogramImage(const cv::Mat &image){
    int numOfBins = 256;
    cv::MatND hist= getHistogram(image);

    // Get min and max bin values
    double maxVal=0, minVal=0;
    cv::minMaxLoc(hist, &minVal, &maxVal, 0, 0);
    cv::Mat histImg = cv::Mat::zeros(numOfBins,numOfBins, CV_8UC1);

    int hpt = static_cast<int>(0.9*numOfBins); // set highest point at 90% of numOfBins

    for( int h = 0; h < numOfBins; h++ ) // Draw a vertical line for each bin
    {
        float binVal = hist.at<float>(h);
        int intensity;
        if (binVal != 0){
            intensity = static_cast<int>(hpt* binVal/maxVal) ;
            cv::line(histImg,cv::Point(h,numOfBins), cv::Point(h,numOfBins-intensity), cv::Scalar::all(255));
        }
    }

    cout << "Impulse noise estimation:" << endl;
    cout << "Percentage black pixels: " << (hist.at<float>(0)/(image.cols * image.rows))*100 << endl;
    cout << "Percentage white pixels: " << (hist.at<float>(255)/(image.cols * image.rows))*100 << endl;

    return histImg;
}

cv::Mat cropImage(cv::Mat src, int x, int y, int width, int heigth){
    cv::Rect roi(x,y,width,heigth);
    cv::Mat tmp = src(roi);
    return tmp;
}

void analyzeImage(cv::Mat src){
    Mat croppedImage;
    croppedImage = cropImage(src,1000,1375,500,200);

    // Spatial domain
    MatND hist = getHistogramImage(croppedImage);
    imwrite("o_images/img1_hist.png",hist);

    //Frequency domain
    calculate_dft(src);
}

template<class ImgT>
void dftshift(ImgT& img) {
    const int cx = img.cols/2;
    const int cy = img.rows/2;

    ImgT tmp;
    ImgT topLeft(img, cv::Rect(0, 0, cx, cy));
    ImgT topRight(img, cv::Rect(cx, 0, cx, cy));
    ImgT bottomLeft(img, cv::Rect(0, cy, cx, cy));
    ImgT bottomRight(img, cv::Rect(cx, cy, cx, cy));

    topLeft.copyTo(tmp);
    bottomRight.copyTo(topLeft);
    tmp.copyTo(bottomRight);

    topRight.copyTo(tmp);
    bottomLeft.copyTo(topRight);
    tmp.copyTo(bottomLeft);
}

void calculate_dft(cv::Mat src){
    // Get original size
    int wxOrig = src.cols;
    int wyOrig = src.rows;


    int m = cv::getOptimalDFTSize( 2*wyOrig );
    int n = cv::getOptimalDFTSize( 2*wxOrig );

    copyMakeBorder(src, src, 0, m - wyOrig, 0, n - wxOrig, cv::BORDER_CONSTANT, cv::Scalar::all(0));

    // Get padded image size
    const int wx = src.cols, wy = src.rows;
    const int cx = wx/2, cy = wy/2;

    std::cout << wxOrig << " " << wyOrig << std::endl;
    std::cout << wx << " " << wy << std::endl;
    std::cout << cx << " " << cy << std::endl;

    // Compute DFT of image
    cv::Mat_<float> imgs[] = {src.clone(), cv::Mat_<float>::zeros(wy, wx)};
    cv::Mat_<cv::Vec2f> img_dft;
    cv::merge(imgs, 2, img_dft);
    cv::dft(img_dft, img_dft);

    // Shift to center
    dftshift(img_dft);

    // Used for visualization only
    cv::Mat_<float> magnitude, phase;
    cv::split(img_dft, imgs);
    cv::cartToPolar(imgs[0], imgs[1], magnitude, phase);
    magnitude = magnitude + 1.0f;
    cv::log(magnitude, magnitude);
    cv::normalize(magnitude, magnitude, 0, 1, CV_MINMAX);
    cv::imwrite("o_images/img1_dft.png", magnitude * 255);
}

int main(){    
    Mat src = loadImage("../../../images/Image1.png",CV_LOAD_IMAGE_GRAYSCALE);

    /// METHOD 1 (max + mean)
    Mat filtered = maxFilter(src,5);
    imwrite("../../../f_images/img1_maxfilter.png",filtered);
    Mat meaned_max = contraHarmonicMeanFilter(filtered, 5,0);
    imwrite("../../../f_images/img1_meaned_maxfilter.png",meaned_max);

    /// METHOD 2 (CHMF + mean)
    Mat CHMF = contraHarmonicMeanFilter(src,5,3);
    imwrite("../../../f_images/img1_CHMF_Q3.png",CHMF);
    Mat meaned = contraHarmonicMeanFilter(CHMF, 5,0);
    imwrite("../../../f_images/img1_meaned_CHMFQ3.png",meaned);

    /// METHOD 3 (CHMF + geomean)
    //Mat CHMF2 = contraHarmonicMeanFilter(src,5,3);
    //Mat geomeaned = geometricMeanFilter(CHMF2,5);
    //imwrite("../../../f_images/img1_geomeaned_CHMFQ3.png",geomeaned);

    /// METHOD 4 (max + CHMF)
    //Mat max = maxFilter(src,5);
    //Mat max_CHMFQ3 = contraHarmonicMeanFilter(max,5,3);
    //imwrite("../../../f_images/img1_max_CHMFQ3.png",max_CHMFQ3);

    cout << "Program finished" << endl;
    return 0;
}
