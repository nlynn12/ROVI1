#ifndef OPENCVFILTERS_H
#define OPENCVFILTERS_H

#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>

cv::Mat loadImage(std::string image, int CV_ARG){
    cv::Mat src = cv::imread(image,CV_ARG);
    if(src.empty()){
        std::cout << "Error: No such file or directory" << std::endl;
    }
    return src;
}

cv::Mat createEmptyCopyOf(cv::Mat src, int CV_ARG=CV_8UC1){
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_ARG);
    return result;
}

cv::Mat createBorder(cv::Mat src, int padding, int border_intensity = 0){
    cv::Mat result;
    cv::copyMakeBorder(src,result,padding,padding,padding,padding, cv::BORDER_CONSTANT, border_intensity);
    return result;
}

cv::Mat maxFilter(cv::Mat src, int filter_size){
    CV_Assert(src.depth() == CV_8U);
    const int r = ((filter_size-1)/2);

    cv::Mat padded = createBorder(src,r);
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_8UC1);

    int largest = 0;

    for(size_t x = 0; x < src.cols; x++){
        for(size_t y = 0; y < src.rows; y++){
            for(int i = -r; i <= r; i++){
                for(int j = -r; j <= r; j++){
                    if(largest < padded.at<uchar>(y+i,x+j))
                        largest = padded.at<uchar>(y+i,x+j);
                }
            }
            result.at<uchar>(y,x) = largest;
            largest = 0;
        }
    }
    return result;
}

cv::Mat contraHarmonicMeanFilter(cv::Mat src, int filter_size, int Q){
    /// Q < -1   eliminates salt noise
    /// Q = -1   harmonic mean
    /// Q = 0    arithmetic mean
    /// Q > 0    eliminates pepper noise

    CV_Assert(src.depth() == CV_8U);
    const int r = ((filter_size-1)/2);

    double sum1 = 0;
    double sum2 = 0;

    cv::Mat padded = createBorder(src,r);
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_8UC1);

    for(size_t x = 0; x < src.cols; x++){
        for(size_t y = 0; y < src.rows; y++){
            for(int i = -r; i <= r; i++){
                for(int j = -r; j <= r; j++){
                    double tmp = (int)padded.at<uchar>(y+i,x+j);
                    sum1 += pow(tmp, (Q+1));
                    sum2 += pow(tmp, Q);
                }
            }
            if(sum2 == 0) sum2 = 0.001;
            result.at<uchar>(y,x) = sum1 / sum2;
            sum1 = 0;
            sum2 = 0;
        }
    }
    return result;
}

cv::Mat geometricMeanFilter(cv::Mat src, int filter_size){
    /// Achieves smoothing comparable to arithmetic mean
    /// however it looses less information

    CV_Assert(src.depth() == CV_8U);
    const int r = ((filter_size-1)/2);

    double prod = 1;
    double tmp = 0;

    cv::Mat padded = createBorder(src,r);
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_8UC1);

    double nRows = src.rows;
    double nCols = src.cols;

    for(size_t x = 0; x < nCols; x++){
        for(size_t y = 0; y < nRows; y++){
            for(int i = -r; i <= r; i++){
                for(int j = -r; j <= r; j++){
                    tmp = (int)padded.at<uchar>(y+i,x+j);
                    prod *= tmp;
                }
            }
            //std::cout << prod << std::endl;
            prod = pow(prod,(1/(nRows*nCols)));
            //std::cout << prod << std::endl;
            result.at<uchar>(y,x) = prod;
            prod = 1;
        }
    }
    return result;
}

#endif // OPENCVFILTERS_H

