#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>


#include "opencvfilters.h"
#include "maxfilter.h"
#include "contraharmonicmeanfilter.h"
#include "adaptivemedianfilter.h"
#include "midpoint.h"

using namespace std;
using namespace cv;


int main(){
    Mat src = loadImage("../../../images/Image3.png",CV_LOAD_IMAGE_GRAYSCALE);
    imwrite("../../../f_images/img3_src.png",src);

    analyzeImage(src,"img3_hist");

    Mat filtered = midpointFilter(src,3);
    imwrite("../../../f_images/img3_midpoint.png",filtered);

    analyzeImage(filtered,"img3_midpoint_hist");

    cout << "Program finished" << endl;
    return 0;
}
