#ifndef CONTRAHARMONICMEANFILTER_H
#define CONTRAHARMONICMEANFILTER_H

#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>

cv::Mat contraHarmonicMeanFilter(cv::Mat src, int filter_size, int Q){
    /// Q < -1   eliminates salt noise
    /// Q = -1   harmonic mean
    /// Q = 0    arithmetic mean
    /// Q > 0    eliminates pepper noise

    CV_Assert(src.depth() == CV_8U);
    const int r = ((filter_size-1)/2);

    double sum1 = 0;
    double sum2 = 0;

    cv::Mat padded = createBorder(src,r);
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_8UC1);

    for(size_t x = 0; x < src.cols; x++){
        for(size_t y = 0; y < src.rows; y++){
            for(int i = -r; i <= r; i++){
                for(int j = -r; j <= r; j++){
                    double tmp = (int)padded.at<uchar>(y+i,x+j);
                    sum1 += pow(tmp, (Q+1));
                    sum2 += pow(tmp, Q);
                }
            }
            if(sum2 == 0) sum2 = 0.001;
            result.at<uchar>(y,x) = sum1 / sum2;
            sum1 = 0;
            sum2 = 0;
        }
    }
    return result;
}

#endif // CONTRAHARMONICMEANFILTER_H

