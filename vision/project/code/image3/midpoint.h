#ifndef MIDPOINT_H
#define MIDPOINT_H

#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>

cv::Mat midpointFilter(cv::Mat src, int filter_size){
    /// Good for randomly distributed noise

    CV_Assert(src.depth() == CV_8U);
    const int r = ((filter_size-1)/2);

    double min=255,max=0;
    double tmp = 0;

    cv::Mat padded = createBorder(src,r);
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_8UC1);

    for(size_t x = 0; x < src.cols; x++){
        for(size_t y = 0; y < src.rows; y++){
            for(int i = -r; i <= r; i++){
                for(int j = -r; j <= r; j++){
                    tmp = padded.at<uchar>(y+i,x+j);
                    if(max < tmp)
                        max = tmp;
                    if(min > tmp)
                        min = tmp;
                }
            }
            result.at<uchar>(y,x) = (min+max)/2;
            min = 255;
            max = 0;
        }
    }
    return result;
}


#endif // MIDPOINT_H

