#ifndef MAXFILTER_H
#define MAXFILTER_H

#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>

cv::Mat maxFilter(cv::Mat src, int filter_size){
    CV_Assert(src.depth() == CV_8U);
    const int r = ((filter_size-1)/2);

    cv::Mat padded = createBorder(src,r);
    cv::Mat result = cv::Mat::zeros(src.rows,src.cols,CV_8UC1);

    int largest = 0;

    for(size_t x = 0; x < src.cols; x++){
        for(size_t y = 0; y < src.rows; y++){
            for(int i = -r; i <= r; i++){
                for(int j = -r; j <= r; j++){
                    if(largest < padded.at<uchar>(y+i,x+j))
                        largest = padded.at<uchar>(y+i,x+j);
                }
            }
            result.at<uchar>(y,x) = largest;
            largest = 0;
        }
    }
    return result;
}

#endif // MAXFILTER_H

