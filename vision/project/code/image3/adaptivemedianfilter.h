#ifndef ADAPTIVEMEDIANFILTER_H
#define ADAPTIVEMEDIANFILTER_H

#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>

struct AMFValues {
    double zmin, zmax, zmed, z_xy;
    int f_size, f_size_max;
    int OrigCols, OrigRows;
    int XCurPosOrigImg, YCurPosOrigImg;
    cv::Mat OrigImg, PaddedImg, Result;
} AMF;

void stageA();
void stageB();

cv::Mat adaptiveMedianFilter(cv::Mat src, int S_init, int S_max){
    /// Good for impulse noise

    CV_Assert(src.depth() == CV_8U);

    AMF.f_size = S_init;
    AMF.f_size_max = S_max;
    AMF.OrigCols = src.cols;
    AMF.OrigRows = src.rows;
    AMF.OrigImg = src.clone();
    AMF.PaddedImg = createBorder(src,(S_max-1)/2);

    AMF.Result = cv::Mat::zeros(AMF.OrigRows,AMF.OrigCols,CV_8UC1);

    for(AMF.XCurPosOrigImg = 0; AMF.XCurPosOrigImg < AMF.OrigCols; AMF.XCurPosOrigImg++){
        //std::cout << AMF.XCurPosOrigImg << std::endl;
        for(AMF.YCurPosOrigImg = 0; AMF.YCurPosOrigImg < AMF.OrigRows; AMF.YCurPosOrigImg++){
            AMF.z_xy = AMF.OrigImg.at<uchar>(AMF.YCurPosOrigImg,AMF.XCurPosOrigImg);
            stageA();
            AMF.f_size = S_init;
        }
    }
    return AMF.Result;
}

void updateAMFValues(){
    int r = (AMF.f_size-1)/2;
    //std::cout << r << std::endl;
    std::vector<int> filterValues;
    //std::cout << AMF.f_size << std::endl;
    for(int i = -r; i <= r; i++){
        for(int j = -r; j <= r; j++){
            filterValues.push_back(AMF.PaddedImg.at<uchar>(AMF.YCurPosOrigImg+i,AMF.XCurPosOrigImg+j));
        }
    }
    std::sort(filterValues.begin(),filterValues.end());
    //std::cout << "Sorted filter" << std::endl;
    AMF.zmin = filterValues[0];
    AMF.zmed = filterValues[(filterValues.size()-1)/2];
    AMF.zmax = filterValues[filterValues.size()-1];
    //std::cout << filterValues[filterValues.size()-1] << std::endl;
    //std::cout << AMF.zmin << "," << AMF.zmed << "," << AMF.zmax << std::endl;
}

void stageA(){
    updateAMFValues();
    //std::cout << "STAGE A"<< std::endl;
    double A1 = AMF.zmed - AMF.zmin;
    double A2 = AMF.zmed - AMF.zmax;
    //std::cout << AMF.zmin << "," << AMF.zmed << "," << AMF.zmax << std::endl;
    //std::cout << A1 << "," << A2 << std::endl;
    if(A1 > 0 && A2 < 0){
        stageB();
    }else{
        AMF.f_size += 2;
        //std::cout << AMF.f_size << std::endl;
        if(AMF.f_size > AMF.f_size_max){
            stageA();
        }else{
            AMF.Result.at<uchar>(AMF.YCurPosOrigImg,AMF.XCurPosOrigImg) = AMF.zmed;
        }
    }
}

void stageB(){
    //std::cout << "STAGE B" << std::endl;
    double B1 = AMF.z_xy - AMF.zmin;
    double B2 = AMF.z_xy - AMF.zmax;
    //std::cout << AMF.z_xy << std::endl;
    if(B1 > 0 && B2 < 0)
        AMF.Result.at<uchar>(AMF.YCurPosOrigImg,AMF.XCurPosOrigImg) = AMF.z_xy;
    else
        AMF.Result.at<uchar>(AMF.YCurPosOrigImg,AMF.XCurPosOrigImg) = AMF.zmed;
}

#endif // ADAPTIVEMEDIANFILTER_H

