Framework for Exercises
=======================
The [programming exercises](exercises/) is developed in C++ utilizing the [OpenCV](http://opencv.org/) 2.4.9 libraries. For compiling the programs simply use cmake

```
$ cmake CMakeLists.txt
$ make
```
Executables are defined to be placed in the bin folder.

Installing OpenCV 2.4.9 in Ubuntu 15.04
=======================================
**Please proceed at own risk** - the authors cannot be held responsible for any damage done by following the guide below.

- Update system

```
sudo apt-get update
sudo apt-get upgrade
```

- Install dependencies

```
sudo apt-get install build-essential libgtk2.0-dev libjpeg-dev libtiff5-dev libjasper-dev libopenexr-dev cmake python-dev python-numpy python-tk libtbb-dev libeigen3-dev yasm libfaac-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev libqt4-dev libqt4-opengl-dev sphinx-common texlive-latex-extra libv4l-dev libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev default-jdk ant libvtk5-qt4-dev
```

- **Note** When using Ubuntu 15.04 the libav API level should be 11. However OpenCV 2.4.9 uses API 9. This means that you wont be able to compile OpenCV. There's many patches for this however I've chosen to install the FFMPEG as a replacement for the standard Ubuntu libav.

```
cd ~
wget http://ffmpeg.org/releases/ffmpeg-2.7.2.tar.bz2
tar -xvf ffmpeg-2.7.2.tar.bz2 
cd ffmpeg-2.7.2/
./configure --enable-shared --disable-static
make
sudo make install
```

Please note that OpenCV **cannot** be compiled with static ffmpeg libraries.

- Downloading the OpenCV 2.4.9 source

```
cd ~
wget http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.9/opencv-2.4.9.zip
unzip opencv-2.4.9.zip
cd opencv-2.4.9
```

- Generate make file with cmake

```
mkdir build
cd build
cmake -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D WITH_QT=ON -D WITH_OPENGL=ON -D WITH_VTK=ON ..
```

- Compile and install

```
make
sudo make install
```

- Configure OpenCV. (1) Open opencv.conf (might not exist)

```
sudo gedit /etc/ld.so.conf.d/opencv.conf
```

- Configure OpenCV. (2) And insert the line

```
/usr/local/lib
```
- Configuration step

```
sudo ldconfig
```

- Open bash.bashrc

```
sudo gedit /etc/bash.bashrc
```
- Add these two lines at the end of the file and save it:

```
PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig
export PKG_CONFIG_PATH
```

Finally, **restart the computer or logout and then login again**. OpenCV will not work correctly until you do this.

Now you have OpenCV 2.4.9 installed with 3D visualization, Python, Java, TBB, OpenGL, video, and Qt support.

