#include <iostream>
#include <iomanip>
#include <vector>
#include <numeric>
#include <chrono>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

RNG rng(12345);

cv::Mat createBorder(cv::Mat src, int padding, int border_intensity = 0){
    cv::Mat result;
    cv::copyMakeBorder(src,result,padding,padding,padding,padding, cv::BORDER_CONSTANT, border_intensity);
    return result;
}


void find_connected_components_8p(cv::Mat &bin_image, vector<vector<Point>> &list_connected_components){
    /*
     * Connected Components Algorithm (8p connectivity)
     * One component at a time (depth first search)
     *
     * Step 1:
     * - Initiate current label var: currlab
     * - Start algorithm at first pixel in the image
     * Step 2:
     * - If the current pixel is a foreground pixel and is not already labelled then
     *   give this pixel the label "currlab"
     * - Add this pixel to the LIFO queue
     * Step 3:
     * - Pop out last (newest) element from the queue and look at its neighbours
     * - If the neighbouring pixel is a foreground pixel and is not already labelled
     *   then give this pixel the label "currlab" and add pixel to the queue
     * - Repeat until queue is empty
     * Step 4:
     * - Increment "currlab"
     * - Go to Step 2
     */

    bin_image = createBorder(bin_image,1);

    // LIFO queue for connected components algorithm
    vector<Point> list_of_conn_comp;

    // Step 1
    int initial_label = 2;
    int currlab = initial_label;

    for(int y = 0; y < bin_image.rows-1; y++){
        for(int x = 0; x < bin_image.cols-1; x++){
            // Step 2
            if(bin_image.at<uchar>(y,x) == 1){
                bin_image.at<uchar>(y,x) = currlab;
                list_of_conn_comp.push_back(Point(x,y));

                // Save connected components
                list_connected_components.resize(currlab-initial_label+1);
                list_connected_components[currlab-initial_label].push_back(Point(x,y));

                // Step 3
                while(!list_of_conn_comp.empty()){
                    Point n_curr_pixel = list_of_conn_comp.back();
                    list_of_conn_comp.pop_back();

                    for(int nx = n_curr_pixel.x-1; nx <= n_curr_pixel.x+1; nx++){
                        for(int ny = n_curr_pixel.y-1; ny <=n_curr_pixel.y+1; ny++){
                            if(bin_image.at<uchar>(ny,nx) == 1){
                                bin_image.at<uchar>(ny,nx) = currlab;
                                list_of_conn_comp.push_back(Point(nx,ny));

                                // Save connectec components
                                list_connected_components[currlab-initial_label].push_back(Point(nx,ny));
                            }
                        }
                    }
                }
                // Step 4;
                currlab++;
            }
        }
    }
}


void find_connected_components_2pass_8p(Mat &image){
    image = createBorder(image,1);

    //Init max label
    const unsigned int max_label = 255;
    uchar parent[max_label];

    for (unsigned int i = 0; i < max_label; ++i){
        parent[i] = 0;
    }
    uchar label = 0;

    //Pass 1
    for (int y = 0; y < image.rows; ++y){
        for (int x = 0; x < image.cols; ++x){
            if(image.at<uchar>(y, x) == 1){
                if(image.at<uchar>(y - 1, x) == 0 && image.at<uchar>(y, x - 1) == 0){
                    label = label + 1;
                    image.at<uchar>(y, x) = label;
                }else if(image.at<uchar>(y, x - 1) > 0 && image.at<uchar>(y - 1, x) > 0){
                    //Get the smalleste label
                    if(image.at<uchar>(y, x - 1) == image.at<uchar>(y - 1, x)){
                        image.at<uchar>(y, x) = image.at<uchar>(y - 1, x);
                    }else if(image.at<uchar>(y, x - 1) > image.at<uchar>(y - 1, x)){
                        image.at<uchar>(y, x) = image.at<uchar>(y - 1, x);
                        parent[image.at<uchar>(y, x - 1)] = image.at<uchar>(y, x);
                    }else{
                        image.at<uchar>(y, x) = image.at<uchar>(y, x - 1);
                        parent[image.at<uchar>(y - 1, x)] = image.at<uchar>(y, x);
                    }
                }else if(image.at<uchar>(y - 1, x) > 0){
                    image.at<uchar>(y, x) = image.at<uchar>(y - 1, x);
                }else if(image.at<uchar>(y, x - 1) > 0){
                    image.at<uchar>(y, x) = image.at<uchar>(y, x - 1);
                }
            }
        }
    }

    //Pass 2
    for (unsigned int y = 1; y < image.rows - 1; ++y){
        for (unsigned int x = 1; x < image.cols - 1; ++x){
            if(image.at<uchar>(y, x) > 0){
                uchar current_label = image.at<uchar>(y, x);
                while(parent[current_label] > 0){
                    current_label = parent[current_label];
                }
                image.at<uchar>(y, x) = current_label;
            }
        }
    }
}

int main(){
    // Load image
    Mat src = imread("../coins.jpg", CV_LOAD_IMAGE_COLOR);
    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }
    // Convert image to grayscale
    Mat src_gray;
    cv::cvtColor( src, src_gray, cv::COLOR_BGR2GRAY );

    // Blur image
    cv::blur( src_gray, src_gray, cv::Size(9,9) );

    // Convert image to binary image
    cv::Mat threshold_output;
    cv::threshold(src_gray, threshold_output, 105, 255,  cv::THRESH_BINARY_INV);
    cv::normalize(threshold_output, threshold_output, 0, 1, CV_MINMAX);

    static auto endTimePoint = std::chrono::system_clock::now();
    static auto startTimePoint = std::chrono::system_clock::now();
    static auto diffTimePoint = endTimePoint - startTimePoint;
    double timeDifference;


    cout << setw(5) << "It:" << setw(15) << "Queue:" << setw(15) << "2 pass:" << setw(15) << "OpenCV" << endl;
    for(int i = 0; i<50; i++){

        /// Queue Implementation
        vector<vector<Point>> list_connected_components;
        Mat queue = threshold_output.clone();

        startTimePoint = std::chrono::system_clock::now();

        find_connected_components_8p(queue,list_connected_components);

        endTimePoint = std::chrono::system_clock::now();
        diffTimePoint = endTimePoint - startTimePoint;
        timeDifference = std::chrono::duration<double,std::milli>(diffTimePoint).count();
        cout << setw(5) << i+1 << setw(15) << timeDifference;


        /// 2 Pass Implementation
        Mat twopass = threshold_output.clone();

        startTimePoint = std::chrono::system_clock::now();

        find_connected_components_2pass_8p(twopass);

        endTimePoint = std::chrono::system_clock::now();
        diffTimePoint = endTimePoint - startTimePoint;
        timeDifference = std::chrono::duration<double,std::milli>(diffTimePoint).count();
        cout << setw(15) << timeDifference;


        /// Find Contours OpenCV Implementation
        cv::Mat fc_threshold;
        cv::threshold(src_gray, fc_threshold, 105, 255,  cv::THRESH_BINARY_INV);
        vector<vector<Point>> contours;
        std::vector<cv::Vec4i> hierarchy;

        startTimePoint = std::chrono::system_clock::now();

        cv::findContours(fc_threshold,contours,hierarchy,CV_RETR_LIST,CHAIN_APPROX_NONE);

        endTimePoint = std::chrono::system_clock::now();
        diffTimePoint = endTimePoint - startTimePoint;
        timeDifference = std::chrono::duration<double,std::milli>(diffTimePoint).count();
        cout << setw(15) << timeDifference << endl;
    }

    return 0;
}

