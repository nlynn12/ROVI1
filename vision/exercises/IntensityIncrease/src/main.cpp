#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;


Mat_<Vec3b> intensity_increase(const Mat_<Vec3b> &img, int amount){
    Mat_<Vec3b> intensified_img = img.clone();

    for(size_t x = 0; x < intensified_img.cols; x++){
        for(size_t y = 0; y < intensified_img.rows; y++){
            for(int i = 0; i < 3; i++){
                intensified_img(y,x)[i]=saturate_cast<uchar>(intensified_img(y,x)[i]+amount);
            }
        }
    }

    return intensified_img;
}

cv::MatND getHistogram(const cv::Mat &image){
    int numOfIm = 1;
    int channels[] = {0}; //The channels of the images used in the computation
    cv::Mat mask; //not using mask, so leave empty
    cv::MatND hist;
    int dimensions = 1;//dimensionality of the histogram
    int numOfBins[] = {256}; //number of bins
    float minMaxValue[] = {0.0, 255.0}; //the min and max value of the bins
    const float* ranges[] = {minMaxValue}; //Array of min-max value arrays, so every dimension is assigned min and max
    cv::calcHist(&image, numOfIm, channels, mask, hist, dimensions, numOfBins, ranges);
    return hist; //the returned type is 32FC1
}


cv::Mat getHistogramImage(const cv::Mat &image)
{
    int numOfBins = 256;
    cv::MatND hist= getHistogram(image);

    // Get min and max bin values
    double maxVal=0, minVal=0;
    cv::minMaxLoc(hist, &minVal, &maxVal, 0, 0);
    cv::Mat histImg = cv::Mat::zeros(numOfBins,numOfBins, CV_8UC1);

    int hpt = static_cast<int>(0.9*numOfBins); // set highest point at 90% of numOfBins

    for( int h = 0; h < numOfBins; h++ ) // Draw a vertical line for each bin
    {
        float binVal = hist.at<float>(h);
        int intensity;
        if (binVal != 0){
            intensity = static_cast<int>(hpt* binVal/maxVal) ;
            cv::line(histImg,cv::Point(h,numOfBins), cv::Point(h,numOfBins-intensity), cv::Scalar::all(255));
        }
    }
    return histImg;
}



int main()
{
    // Load image
    Mat src = imread("../lena.bmp", CV_LOAD_IMAGE_COLOR);

    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }
    namedWindow("Original Image", CV_WINDOW_AUTOSIZE);
    imshow("Original Image", src);
    cout << "Color image type: " << src.type() << endl;

    /// Exercise 2.1
    Mat_<Vec3b> intensityT = intensity_increase(src,50);
    namedWindow("Intensity Transformation", CV_WINDOW_AUTOSIZE);
    imshow("Intensity Transformation",intensityT);

    /// Exercise 2.2
    Mat_<uchar> greyLena;
    cvtColor(src, greyLena, CV_BGR2GRAY);
    namedWindow("Grey image", CV_WINDOW_AUTOSIZE);
    imshow("Grey image",greyLena);

    MatND hist = getHistogramImage(greyLena);
    namedWindow("Histogram");
    imshow("Histogram", hist);
    waitKey(0);



    destroyAllWindows();

    return 0;
}

