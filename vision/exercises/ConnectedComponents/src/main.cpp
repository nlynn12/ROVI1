#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

struct pixel{
    pixel(int x, int y) : x(x), y(y) {}
    int x,y;
};

void find_connected_components_8p(cv::Mat &bin_image){
    /*
     * Connected Components Algorithm (8p connectivity)
     * One component at a time (depth first search)
     *
     * Step 1:
     * - Initiate current label var: currlab
     * - Start algorithm at first pixel in the image
     * Step 2:
     * - If the current pixel is a foreground pixel and is not already labelled then
     *   give this pixel the label "currlab"
     * - Add this pixel to the LIFO queue
     * Step 3:
     * - Pop out last (newest) element from the queue and look at its neighbours
     * - If the neighbouring pixel is a foreground pixel and is not already labelled
     *   then give this pixel the label "currlab" and add pixel to the queue
     * - Repeat until queue is empty
     * Step 4:
     * - Increment "currlab"
     * - Go to Step 2
     */

    vector<pixel> list_of_conn_comp;
    // Step 1
    int currlab = 2;

    for(int y = 0; y < bin_image.rows-1; y++){
        for(int x = 0; x < bin_image.cols-1; x++){
            // Step 2
            if(bin_image.at<uchar>(y,x) == 1){
                bin_image.at<uchar>(y,x) = currlab;
                list_of_conn_comp.push_back(pixel(x,y));
                // Step 3
                while(!list_of_conn_comp.empty()){
                    pixel n_curr_pixel = list_of_conn_comp.back();
                    list_of_conn_comp.pop_back();

                    for(int nx = n_curr_pixel.x-1; nx <= n_curr_pixel.x+1; nx++){
                        for(int ny = n_curr_pixel.y-1; ny <=n_curr_pixel.y+1; ny++){
                            if(bin_image.at<uchar>(ny,nx) == 1){
                                bin_image.at<uchar>(ny,nx) = currlab;
                                list_of_conn_comp.push_back(pixel(nx,ny));
                            }
                        }
                    }
                }
                // Step 4;
                currlab++;
            }
        }
    }

    // Print
    for(int y = 0; y < bin_image.rows; y++){
        for(int x = 0; x < bin_image.cols; x++){
            cout << (int)bin_image.at<uchar>(y,x);
        }
        cout << endl;
    }


}


int main(){
    // Load image
    Mat image = imread("../connected_components.png", CV_LOAD_IMAGE_GRAYSCALE);

    if (image.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }

    // Convert image to binary image
    normalize(image, image, 0, 1, CV_MINMAX);

    // Find Connected Components
    find_connected_components_8p(image);

    return 0;
}

