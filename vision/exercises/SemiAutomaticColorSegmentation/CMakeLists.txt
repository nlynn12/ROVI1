cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)

## Init the project
project(SemiAutomaticColorSegmentation LANGUAGES C CXX)
add_definitions(-std=c++11)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

find_package(OpenCV REQUIRED)

#include_directories(include/)

add_executable(${PROJECT_NAME}  src/main.cpp )

target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS})

set(CMAKE_BUILD_TYPE Release)
