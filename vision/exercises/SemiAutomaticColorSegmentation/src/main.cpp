#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;



int main(){
    // Load image
    cv::Mat src = imread("../lego.jpg", CV_LOAD_IMAGE_COLOR);
    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }
    // Create ROI and compute mean of it
    cv::Rect roi(70, 145, 90, 40);
    cv::Mat roi_src = src(roi);
    cv::Scalar mean_s, stddev_s;
    cv::meanStdDev(roi_src,mean_s,stddev_s);

    cv::Mat_<float> mean(3,1);
    for(int i = 0; i < 3; ++i) {
        mean(i,0) = mean_s[i];
    }

    //imshow("Rectangle", roi_src);

    float euclideanDistTol = 0.2*255;
    Mat_<Vec3f> threshold_src = src.clone();

    for(int y = 0; y < src.rows; y++){
        for(int x = 0; x < src.cols; x++){
            Vec3f &bgr = threshold_src(y,x);
            Mat_<float> bgrm(bgr,false);
            if(cv::norm(bgrm-mean) <= euclideanDistTol)
                bgr = Vec3f(1,1,1);
            else
                bgr = Vec3f(0,0,0);

        }
    }
    imshow("Thresholded Src",threshold_src);
    cv::waitKey(0);

    Mat kernel = Mat::ones(9,9,CV_8U);
    dilate(threshold_src,threshold_src,kernel);

    erode(threshold_src,threshold_src,kernel);
    imshow("Thresholded image", threshold_src);
    waitKey(0);

    waitKey(0);
    destroyAllWindows();

    return 0;
}

