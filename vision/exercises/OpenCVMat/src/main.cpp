#include <iostream>
#include "opencv2/highgui/highgui.hpp"

using namespace std;
using namespace cv;

int main()
{
    // Load image
    Mat src = imread("../color.png", CV_LOAD_IMAGE_COLOR);
    cout << src.type() << endl;

    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }
    // Display image
    namedWindow("Original Image", CV_WINDOW_AUTOSIZE);
    imshow("Original Image",src);

    /* at method */
    Mat at_src(src);

    for(size_t x = 350; x < 440; x++){
        for(size_t y = 100; y < 220; y++){
            at_src.at<Vec3b>(y,x)[0] = 0;
            at_src.at<Vec3b>(y,x)[1] = 0;
            at_src.at<Vec3b>(y,x)[2] = 0;
        }
    }

    // Display image
    namedWindow("Manipulated Image - at method", CV_WINDOW_AUTOSIZE);
    imshow("Manipulated Image - at method",at_src);

    Mat ptr_src(src);
    for(size_t y = 100; y < 220; y++){
        Vec3b* data_src = ptr_src.ptr<Vec3b>(y);
        for(size_t x =350; x < 440; x++){
            data_src[x][0] = 0;
            data_src[x][1] = 0;
            data_src[x][2] = 0;
        }
    }
    // Display image
    namedWindow("Manipulated Image - ptr method", CV_WINDOW_AUTOSIZE);
    imshow("Manipulated Image - ptr method",ptr_src);


    Mat_<Vec3b> brk_src = src.clone();
    for(size_t x = 350; x < 440; x++){
        for(size_t y = 100; y < 220; y++){
            brk_src(y,x)[0]=0;
            brk_src(y,x)[1]=0;
            brk_src(y,x)[2]=0;
        }
    }
    // Display image
    namedWindow("Manipulated Image - bracket method", CV_WINDOW_AUTOSIZE);
    imshow("Manipulated Image - bracket method",brk_src);


    waitKey(0);

    destroyAllWindows();
    return 0;
}

