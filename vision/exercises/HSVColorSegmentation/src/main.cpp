#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

const int hSlider = 180;
int hSliderMin;
int hSliderMax;

const int sSlider = 255;
int sSliderMin;
int sSliderMax;

const int vSlider = 255;
int vSliderMin;
int vSliderMax;

double hue;
double beta;

cv::Mat dst;
cv::Mat imghsv;

void separateChannels (cv::Mat img) {
    //initialize a matrix for each channel in img
    cv::Mat ch1(img.rows, img.cols, CV_8UC1);
    cv::Mat ch2(img.rows, img.cols, CV_8UC1);
    cv::Mat ch3(img.rows, img.cols, CV_8UC1);

    //initialize an int array of size 2
    int from_to[2];

    //copy channel 2 of img to channel 0 of ch1
    from_to[0] = 2;
    from_to[1] = 0;
    cv::mixChannels( &img, 1, &ch1, 1, from_to, 1);


    //copy channel 1 of img to channel 0 of ch2
    from_to[0] = 1;
    from_to[1] = 0;
    cv::mixChannels( &img, 1, &ch2, 1, from_to, 1);


    //copy channel 0 of img to channel 0 of ch3
    from_to[0] = 0;
    from_to[1] = 0;
    cv::mixChannels( &img, 1, &ch3, 1, from_to, 1);
}

void on_trackbar( int, void* ){
    cv::inRange(imghsv, cv::Scalar(hSliderMin, sSliderMin, vSliderMin), cv::Scalar(hSliderMax, sSliderMax, vSliderMax), dst);
    imshow( "Colour Segmentation", dst );
}

int main(){
    // Load image
    cv::Mat src = imread("../../../FinalProject/Vision/Sequence1/images/MarkerColor/marker_color_01.png", CV_LOAD_IMAGE_COLOR);
    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }
    imshow("src",src);
    waitKey(0);

    // Split image in 3 channels (BGR)
    cv::Mat imgBGR[3];
    cv::split(src,imgBGR);


    // Convert BGR to HSV
    cv::cvtColor(src, imghsv, CV_BGR2HSV);
    separateChannels(imghsv); //alternatively use cv::split() as above

    //Part 3
    hSliderMin = 0;
    hSliderMax = 180;
    sSliderMin = 0;
    sSliderMax = 255;
    vSliderMin = 0;
    vSliderMax = 255;

    namedWindow("Colour Segmentation", cv::WINDOW_NORMAL);

    cv::createTrackbar( "H min", "Colour Segmentation", &hSliderMin, hSlider, on_trackbar );
    cv::createTrackbar( "H max", "Colour Segmentation", &hSliderMax, hSlider, on_trackbar );
    cv::createTrackbar( "S min", "Colour Segmentation", &sSliderMin, sSlider, on_trackbar );
    cv::createTrackbar( "S max", "Colour Segmentation", &sSliderMax, sSlider, on_trackbar );
    cv::createTrackbar( "V min", "Colour Segmentation", &vSliderMin, vSlider, on_trackbar );
    cv::createTrackbar( "V max", "Colour Segmentation", &vSliderMax, vSlider, on_trackbar );

    cv::waitKey(0);


    waitKey(0);
    destroyAllWindows();

    return 0;
}

