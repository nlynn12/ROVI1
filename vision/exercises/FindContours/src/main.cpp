#include <iostream>
#include <iomanip>
#include <vector>
#include <numeric>
#include <chrono>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

RNG rng(12345);

int main(){
    // Load image
    Mat src = imread("../coins.jpg", CV_LOAD_IMAGE_COLOR);
    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }
    // Convert image to grayscale
    Mat src_gray;
    cv::cvtColor( src, src_gray, cv::COLOR_BGR2GRAY );

    // Blur image
    cv::blur( src_gray, src_gray, cv::Size(9,9) );

    // Convert image to binary image
    cv::Mat threshold_output;
    cv::threshold(src_gray, threshold_output, 105, 255,  cv::THRESH_BINARY_INV);

    vector<vector<Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(threshold_output,contours,hierarchy,CV_RETR_LIST,CHAIN_APPROX_NONE);


    // Draw Connected Components
    Mat drawing = Mat::zeros(src.size(), CV_8UC3);
    for(int i = 0; i < contours.size(); i++){
        Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        drawContours(drawing,contours,i,color,1,8);
    }
    namedWindow( "Connected Components", CV_WINDOW_NORMAL );
    imshow( "Connected Components", drawing );

    waitKey(0);
    destroyAllWindows();

    return 0;
}

