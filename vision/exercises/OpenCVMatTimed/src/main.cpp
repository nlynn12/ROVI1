#include <iostream>
#include <iomanip>
#include <chrono>
#include "opencv2/highgui/highgui.hpp"

using namespace std;
using namespace cv;

int main()
{

    static auto endTimePoint = std::chrono::system_clock::now();
    static auto startTimePoint = std::chrono::system_clock::now();
    static auto diffTimePoint = endTimePoint - startTimePoint;
    double timeDifference;

    // Load image
    Mat src = imread("../color.png", CV_LOAD_IMAGE_COLOR);
    cout << src.type() << endl;

    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }
    // Display image
    namedWindow("Original Image", CV_WINDOW_AUTOSIZE);
    imshow("Original Image",src);

    cout << endl;
    cout << "--------------------------------------------------------" << endl;
    cout << "Prints out average access time pr. pixel in nano seconds" << endl;
    cout << "--------------------------------------------------------" << endl;
    cout << endl;

    cout << setw(13) << "At" << setw(13) << "Ptr" << setw(13) << "Brk" << endl;

    for(int i = 0; i < 30; i++){
        /* at method */
        Mat at_src(src);

        startTimePoint = std::chrono::system_clock::now();

        for(size_t x = 350; x < 440; x++){
            for(size_t y = 100; y < 220; y++){
                at_src.at<Vec3b>(y,x)[0] = 0;
                at_src.at<Vec3b>(y,x)[1] = 0;
                at_src.at<Vec3b>(y,x)[2] = 0;
            }
        }

        endTimePoint = std::chrono::system_clock::now();
        diffTimePoint = endTimePoint - startTimePoint;
        timeDifference = std::chrono::duration<double,std::nano>(diffTimePoint).count();
        cout << setw(13) << timeDifference/10800;


        /* ptr method */

        Mat ptr_src(src);

        startTimePoint = std::chrono::system_clock::now();
        for(size_t y = 100; y < 220; y++){
            Vec3b* data_src = ptr_src.ptr<Vec3b>(y);
            for(size_t x =350; x < 440; x++){
                data_src[x][0] = 0;
                data_src[x][1] = 0;
                data_src[x][2] = 0;
            }
        }
        endTimePoint = std::chrono::system_clock::now();
        diffTimePoint = endTimePoint - startTimePoint;
        timeDifference = std::chrono::duration<double,std::nano>(diffTimePoint).count();
        cout << setw(13) << timeDifference/10800;


        /*Bracket method*/

        Mat_<Vec3b> brk_src = src.clone();

        startTimePoint = std::chrono::system_clock::now();
        for(size_t x = 350; x < 440; x++){
            for(size_t y = 100; y < 220; y++){
                brk_src(y,x)[0]=0;
                brk_src(y,x)[1]=0;
                brk_src(y,x)[2]=0;
            }
        }
        endTimePoint = std::chrono::system_clock::now();
        diffTimePoint = endTimePoint - startTimePoint;
        timeDifference = std::chrono::duration<double,std::nano>(diffTimePoint).count();
        cout << setw(13) << timeDifference/10800 << endl;
    }

    waitKey(0);

    destroyAllWindows();
    return 0;
}

