#include <iostream>
#include <vector>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

RNG rng(12345);
Mat src,src_gray;
int threshold_ = 105;
int thres_slider_max = 255;


cv::Mat createBorder(cv::Mat src, int padding, int border_intensity = 0){
    cv::Mat result;
    cv::copyMakeBorder(src,result,padding,padding,padding,padding, cv::BORDER_CONSTANT, border_intensity);
    return result;
}


void find_connected_components_8p(cv::Mat &bin_image, vector<vector<Point>> &list_connected_components){
    /*
     * Connected Components Algorithm (8p connectivity)
     * One component at a time (depth first search)
     *
     * Step 1:
     * - Initiate current label var: currlab
     * - Start algorithm at first pixel in the image
     * Step 2:
     * - If the current pixel is a foreground pixel and is not already labelled then
     *   give this pixel the label "currlab"
     * - Add this pixel to the LIFO queue
     * Step 3:
     * - Pop out last (newest) element from the queue and look at its neighbours
     * - If the neighbouring pixel is a foreground pixel and is not already labelled
     *   then give this pixel the label "currlab" and add pixel to the queue
     * - Repeat until queue is empty
     * Step 4:
     * - Increment "currlab"
     * - Go to Step 2
     */

    bin_image = createBorder(bin_image,1);

    // LIFO queue for connected components algorithm
    vector<Point> list_of_conn_comp;

    // Step 1
    int initial_label = 2;
    int currlab = initial_label;

    for(int y = 0; y < bin_image.rows-1; y++){
        for(int x = 0; x < bin_image.cols-1; x++){
            // Step 2
            if(bin_image.at<uchar>(y,x) == 1){
                bin_image.at<uchar>(y,x) = currlab;
                list_of_conn_comp.push_back(Point(x,y));

                // Save connected components
                list_connected_components.resize(currlab-initial_label+1);
                list_connected_components[currlab-initial_label].push_back(Point(x,y));

                // Step 3
                while(!list_of_conn_comp.empty()){
                    Point n_curr_pixel = list_of_conn_comp.back();
                    list_of_conn_comp.pop_back();

                    for(int nx = n_curr_pixel.x-1; nx <= n_curr_pixel.x+1; nx++){
                        for(int ny = n_curr_pixel.y-1; ny <=n_curr_pixel.y+1; ny++){
                            if(bin_image.at<uchar>(ny,nx) == 1){
                                bin_image.at<uchar>(ny,nx) = currlab;
                                list_of_conn_comp.push_back(Point(nx,ny));

                                // Save connectec components
                                list_connected_components[currlab-initial_label].push_back(Point(nx,ny));
                            }
                        }
                    }
                }
                // Step 4;
                currlab++;
            }
        }
    }
}

void count_coins(vector<vector<Point>> list_connected_components){
    int ten     = 0,
        fifteen = 0,
        twenty  = 0,
        five    = 0,
        fifty   = 0,
        area    = 0;
    for(int i = 0; i<list_connected_components.size();i++){
        area = list_connected_components[i].size();
        if (area > 20000 && area < 25000) ten++;
        if (area > 25000 && area < 30000) fifteen++;
        if (area > 30000 && area < 40000) twenty++;
        if (area > 40000 && area < 47500) five++;
        if (area > 47500 && area < 50000) fifty++;
    }
    std::cout << "Five: " << five << std::endl;
    std::cout << "Ten: " << ten << std::endl;
    std::cout << "Fifteen: " << fifteen << std::endl;
    std::cout << "Twenty: " << twenty << std::endl;
    std::cout << "Fifty: " << fifty << std::endl;
}

void conn_comp_track(){
    // Convert image to binary image
    cv::Mat threshold_output;
    cv::threshold(src_gray, threshold_output, threshold_, 255,  cv::THRESH_BINARY_INV);

    cv::namedWindow( "Threshold.png", cv::WINDOW_NORMAL );
    cv::imshow( "Threshold.png", threshold_output );

    cv::normalize(threshold_output, threshold_output, 0, 1, CV_MINMAX);

    // Find Connected Components
    vector<vector<Point>> list_connected_components;
    find_connected_components_8p(threshold_output,list_connected_components);


    /// DEBUG: Print large areas of connected components
    for(auto component : list_connected_components){
        if(component.size()>10000)
            cout << component.size() << endl;
    }

    // Count coins depending on their value (area)
    count_coins(list_connected_components);


    // Draw Connected Components
    Mat drawing = Mat::zeros(src.size(), CV_8UC3);
    for(int i = 0; i < list_connected_components.size(); i++){
        Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        drawContours(drawing,list_connected_components,i,color,1,8);
    }
    namedWindow( "Connected Components", CV_WINDOW_NORMAL );
    imshow( "Connected Components", drawing );
}

void on_trackbar(int, void*){
    conn_comp_track();
}

int main(){
    // Load image
    src = imread("../coins.jpg", CV_LOAD_IMAGE_COLOR);
    if (src.empty()){
        cout << "No such file or directory" << endl;
        return -1;
    }
    // Convert image to grayscale
    cv::cvtColor( src, src_gray, cv::COLOR_BGR2GRAY );

    // Blur image
    cv::blur( src_gray, src_gray, cv::Size(9,9) );

    // Advanced main
    std::string sourceWindow = "source";
    cv::namedWindow(sourceWindow,cv::WINDOW_NORMAL);
    imshow(sourceWindow,src);
    cv::createTrackbar( "Binary image threshold:", sourceWindow, &threshold_, thres_slider_max, on_trackbar );


    waitKey(0);
    destroyAllWindows();


    return 0;
}

